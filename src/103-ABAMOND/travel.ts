/* eslint-disable @typescript-eslint/naming-convention */
namespace App {
	registerPassageDisplayNames({
		AB_BlackSwamp: "The Black Swamp",
		AB_ForestClearing: "Forest Clearing",
		AB_ForestTrail: "Forest Trail",
		AB_GlutezonGate: () => visited("AB_GlutezonGate") ? "Glutezon Village Front Gate" : "Valley Below",
		AB_GlutezonQueen: () => visited("AB_GlutezonQueen") ? "Glutezon Queen's Chamber" : "Long Hall",
		AB_GlutezonShop: "Village Trader",
		AB_GlutezonVillage: "Village Yard",
		AB_HillsideTrail: "Hillside Trail",
		AB_MamazonCroplands: "Back Gate",
		AB_MamazonGate: () => visited("AB_MamazonGate") ? "Mamazon Village Front Gate" : "Small Village",
		AB_MamazonQueen: () => visited("AB_MamazonQueen") ? "Mamazon Queen's Chamber" : "Stone Hall",
		AB_MamazonShop: "Village Trader",
		AB_MamazonVillage: "Village Green",
		AB_MysteriousLanding: "Mysterious Landing",
		AB_OvergrownCairn: "Overgrown Cairn",
		AB_Plateau: "Hillside Plateau",
		AB_Shanty: "Small Shanty",
		AB_SwampClearing: "Bobola Clearing",
		AB_SwampRiver: "Swamp River",
		Abamond: "Lonely Pier",
		FarBeach: "Far Beach",
		HiddenCove: "Hidden Cove",
		Jungle: "Into the Jungle",
		OceanCave: "Explore the caves",
		Ruins: "Into the Ruins",
		TreasureRoom: "Treasure Room",
	});

	registerTravelDestination({
		Abamond: [
			{
				caption: "Back to The Salty Mermaid",
				destination: "Deck",
			},
			"FarBeach", "Jungle",
		],
		FarBeach: ["Abamond", "OceanCave"],
		Jungle: [
			"Abamond", "AB_BlackSwamp",
			// TODO replace tests in the the Jungle passage with a travel scene, hide it upon uncovering all the destinations
			{
				available: [{type: 'var:b', scope: {type: "job", id: "JUNGLE"}, name: "COVE_FOUND", value: true}],
				destination: "HiddenCove",
			},
			{
				available: [{type: 'var:b', scope: {type: "job", id: "JUNGLE"}, name: "RUINS_FOUND", value: true}],
				destination: "Ruins",
			},
		],
		AB_BlackSwamp: ["Jungle", "AB_SwampRiver"],
		AB_SwampRiver: [
			"AB_BlackSwamp", "AB_MysteriousLanding",
			{
				available: [
					{type: 'quest', name: "BOBOLA_SAP_2", property: "status", value: QuestStatus.Completed},
					{type: 'quest', name: "BOBOLA_SAP_1", property: "status", value: QuestStatus.Completed, condition: '!='},
					{type: 'item', name: "quest/bucket of bobola sap", value: 0, condition: '=='},
				],
				destination: "AB_SwampClearing",
			},
		],
		AB_SwampClearing: ["AB_SwampRiver"],
		AB_MysteriousLanding: ["AB_SwampRiver", "AB_Shanty"],
		AB_Shanty: ["AB_MysteriousLanding"],
		OceanCave: [
			"FarBeach",
			{
				available: [{type: 'dayPhase', max: DayPhase.Afternoon}],
				caption: "Explore the Caverns below Abamond",
				destination: "CaveRogueLikeGUI",
				action: () => {
					Rogue.Engine.instance.loadScene("#passage-caveroguelikegui", "OceanCave", 1)
				},
			},
			{
				caption: player => `Continue Exploring (Level: ${player.flags["ABAMOND_CAVE_LEVEL"]})`,
				available: [
					{type: 'dayPhase', max: DayPhase.Afternoon},
					{type: 'var:n', scope: {type: "player"}, name: "ABAMOND_CAVE_LEVEL", value: 1, condition: '>'},
				],
				enabled: [{type: 'dayPhase', max: DayPhase.Afternoon}],
				destination: "CaveRogueLikeGUI",
				action: (player) => {
					Rogue.Engine.instance.loadScene("#passage-caveroguelikegui", "OceanCave", player.flags["ABAMOND_CAVE_LEVEL"] as number)
				},
			},
		],
		HiddenCove: ["Jungle", "AB_Plateau"],
		Ruins: ["Jungle", "TreasureRoom"],
		TreasureRoom: ["Ruins"],
		AB_Plateau: ["HiddenCove", "AB_HillsideTrail", "AB_ForestTrail"],
		AB_HillsideTrail: ["AB_Plateau", "AB_OvergrownCairn"],
		AB_ForestTrail: ["AB_Plateau", "AB_ForestClearing"],
		AB_OvergrownCairn: ["AB_HillsideTrail", "AB_GlutezonGate"],
		AB_ForestClearing: ["AB_ForestTrail", "AB_MamazonGate"],
		AB_GlutezonGate: [
			"AB_OvergrownCairn",
			{
				available: [{type: 'quest', name: 'MAMAZON_CHAMP', property: "status", value: QuestStatus.Completed}],
				caption: "Enter Village",
				scene: "gluetezonVillageLockedForMamazons",
			},
			{
				available: [
					{type: 'quest', name: 'MAMAZON_CHAMP', property: "status", value: QuestStatus.Completed, condition: '!='},
					{type: Stat.Body, name: BodyPart.Ass, value: 40, condition: '<'},
				],
				caption: "Enter Village",
				scene: "gluetezonVillageLockedForSkinnyButs",
			},
			{
				available: [
					{type: 'quest', name: 'MAMAZON_CHAMP', property: "status", value: QuestStatus.Completed, condition: '!='},
					{type: Stat.Body, name: BodyPart.Ass, value: 40, condition: '>='},
				],
				caption: "Enter Village",
				destination: "AB_GlutezonVillage",
			},
		],
		AB_MamazonGate: [
			"AB_ForestClearing",
			{
				available: [{type: 'quest', name: 'GLUTEZON_CHAMP', property: "status", value: QuestStatus.Completed}],
				caption: "Enter Village",
				scene: "mamazonVillageLockedForGluetezons",
			},
			{
				available: [
					{type: 'quest', name: 'GLUTEZON_CHAMP', property: "status", value: QuestStatus.Completed, condition: '!='},
					{type: Stat.Body, name: BodyPart.Bust, value: 40, condition: '<'},
				],
				caption: "Enter Village",
				scene: "mamazonVillageLockedForFlatChested",
			},
			{
				available: [
					{type: 'quest', name: 'GLUTEZON_CHAMP', property: "status", value: QuestStatus.Completed, condition: '!='},
					{type: Stat.Body, name: BodyPart.Bust, value: 40, condition: '>='},
				],
				caption: "Enter Village",
				destination: "AB_MamazonVillage",
			},
		],
		AB_GlutezonVillage: ["AB_GlutezonGate", "AB_GlutezonQueen", "AB_GlutezonShop"],
		AB_GlutezonQueen: ["AB_GlutezonVillage"],
		AB_GlutezonShop: ["AB_GlutezonVillage"],
		AB_MamazonVillage: ["AB_MamazonGate", "AB_MamazonQueen", "AB_MamazonShop", "AB_MamazonCroplands"],
		AB_MamazonQueen: ["AB_MamazonVillage"],
		AB_MamazonShop: ["AB_MamazonVillage"],
		AB_MamazonCroplands: ["AB_MamazonVillage"],
		CombatAbamondGenericWin: [{caption: "Continue", destination: "CaveRogueLikeGUI"}],
		CombatAbamondGenericLose: [{caption: "Continue", destination: "CaveRogueLikeGUI"}],
		CombatAbamondGenericLoseNonFatal: [{caption: "Continue", destination: "CaveRogueLikeGUI"}],
		CombatAbamondGenericFlee: [{caption: "Continue", destination: "CaveRogueLikeGUI"}],
	});
}

/* eslint-enable @typescript-eslint/naming-convention */
