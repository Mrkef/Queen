Object.append(App.Data.Travel.scenes, {
	gluetezonVillageLockedForMamazons: {
		entry: "attempt",
		actors: {},
		fragments: {
			attempt: {
				type: "text",
				text: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

				<span class='npc-text'>"Halt! We won't allow any allies of the <span class="npc'>Mamazons</span> \
				to enter our peaceful village! Begone thot!"</span>

				It seems like you've burned your bridges with the @@.npc;Mamazons@@.`,
			},
		},
	},
	gluetezonVillageLockedForSkinnyButs: {
		entry: "attempt",
		actors: {},
		fragments: {
			attempt: {
				type: "text",
				text: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

				<span class='npc-text'>"Halt! Begone from <span class="npc">Glutezon</span> lands you pancake arsed whore, \
				before we kill you for being a <span class="npc">Mamazon</span> spy!"</span>

				It seems like you've encountered the fabled @@.npc;Glutezons@@, a tribe of jungle women with immense \
				asses. Perhaps if you were equally as endowed they might allow you to enter their village…`,
			},
		},
	},
	mamazonVillageLockedForGluetezons: {
		entry: "attempt",
		actors: {},
		fragments: {
			attempt: {
				type: "text",
				text: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

				<span class='npc-text'>"Halt! We won't allow any allies of the <span class="npc'>Glutezons</span> \
				to enter our peaceful village! Begone thot!"</span>

				It seems like you've burned your bridges with the @@.npc;Mamazons@@.`,
			},
		},
	},
	mamazonVillageLockedForFlatChested: {
		entry: "attempt",
		actors: {},
		fragments: {
			attempt: {
				type: "text",
				text: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

				<span class='npc-text'>"Halt! Begone from <span class="npc">Mamazon</span> lands you flat chested harlot, \
				before we kill you for being a <span class="npc">Glutezon</span> spy!"</span>

				It seems like you've encountered the fabled @@.npc;Mamazons@@, a tribe of jungle women with immense \
				breasts. Perhaps if you were equally as endowed they might allow you to enter their village…`,
			},
		},
	},
});
