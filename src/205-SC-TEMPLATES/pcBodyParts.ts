for (const descFunction of Object.keys(App.PR.pCharacterRating)) {
	const templateKey = _.snakeCase(descFunction).toUpperCase();
	Template.add(`n${templateKey}`, () => App.PR.humanSelfRating(setup.world.pc, descFunction, {colorize: true}));
	Template.add(`nb${templateKey}`, () => App.PR.humanSelfRating(setup.world.pc, descFunction, {brief: true}));
}
