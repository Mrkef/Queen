namespace App.UI {
	export const link = (function () {
		let counter = 0;

		// reset all handlers for each passage
		$(document).on(':passageinit', () => {
			State.temporary.linkHandlers = {};
			counter = 0;
		});

		return makeLink;

		type AnyHandler = (...args: any[]) => void;
		/**
		 * Creates a markup for a SugarCube link which executes given function with given arguments
		 *
		 * @param linkText link text
		 * @param handler callable object
		 * @param args arguments
		 * @param passage the passage name to link to
		 * @returns link in SC markup
		 */
		function makeLink<THandler extends AnyHandler>(linkText: string, handler: THandler, args: Parameters<THandler>, passage = '', tooltip = '') {
			// pack handler and data
			State.temporary.linkHandlers[counter] = {
				f: handler,
				args: args,
			};

			// can't say _linkHandlers here because SC does not recognize its own notation in "..._varName"
			const scHandlerText =
				`State.temporary.linkHandlers[${counter}].f(...State.temporary.linkHandlers[${counter}].args);`;
			++counter;

			if (passage) {
				return UI.pPassageLink(linkText, passage, scHandlerText, tooltip);
			} else {
				if (tooltip) {
					throw "Tooltips are not supported by the <<link>> markup.";
				}
				// data-passage scheme does not work with empty passage name
				return `<<link "${linkText}">><<run ${scHandlerText}>><</link>>`;
			}
		}
	})();

	/**
	 * Creates a HTML element with custom SugarCube attributes which works as a passage link
	 *
	 * The result works in the same way as the wiki markup in the SugarCube
	 * @see https://www.motoslave.net/sugarcube/2/docs/#markup-html-attribute
	 * @param linkText link text
	 * @param passage the passage name to link to
	 * @param setter setter text
	 * @param tooltip tooltip text
	 * @param elementType element type, default is 'a'. Could be any of 'a', 'audio', img', 'source', 'video'
	 * @returns element text
	 *
	 * @example
	 * // equal to [[Go to town|Town]]
	 * App.UI.passageLink("Go to town", "Town")
	 */
	export function pPassageLink(linkText: string, passage: string, setter = '', tooltip = '', elementType = 'a'): string {
		let res = `<${elementType} data-passage="${passage}"`;
		if (setter) {
			res += ` data-setter="${Utils.escapeHtml(setter)}"`;
		}
		if (tooltip) {
			res += ` title="${tooltip}"`;
		}
		res += `>${linkText}</${elementType}>`;
		return res;
	}

	/**
	 * Replaces contents of the element, identified by the given selector, with wiki'ed new content
	 *
	 * The function is an analogue to the SugarCube `<<replace>>` macro (and is a simplified version of it)
	 */
	export function replace(element: string | HTMLElement, ...newContent: (string | Node)[]): void {
		const target = typeof element === "string" ? document.querySelector(element) as HTMLElement : element;
		target.textContent = '';
		for (const nc of newContent) {
			if (typeof nc === "string") {
				const ins = jQuery(document.createDocumentFragment());
				ins.wiki(nc);
				ins.appendTo(target);
			} else {
				target.append(nc);
			}
		}
	}

	export function pWithTooltip(text: string, tooltipText: string | string[], classNames?: string[], styles?: string[]): string {
		const makeTooltip = (tooltipContent: string | string[]): string => {
			if (Array.isArray(tooltipContent)) {
				if (tooltipContent.length === 1) {
					return makeTooltip(tooltipContent[0]);
				}
				return `<div class="tooltip"><ul>${tooltipContent.map(element => `<li>${element}</li>`).join('')}</ul></div>`;
			} else {
				return `<span class="tooltip">${tooltipContent}</span>`;
			}
		};
		const tooltip = makeTooltip(tooltipText);
		let classes = ["text-with-tooltip"];
		if (classNames) {
			classes = classes.concat(classNames);
		}

		let styleString = "";
		if (styles && styles.length > 0) {
			styleString = ` style="${styles.join(';')}"`;
		}
		return `<span class="${classes.join(' ')}"${styleString}>${text}${tooltip}</span>`;
	}

	export function makeElement<T extends keyof HTMLElementTagNameMap>(tag: T, content?: string, classNames?: string[]): HTMLElementTagNameMap[T] {
		const element = document.createElement(tag);
		if (classNames !== undefined) {
			if (Array.isArray(classNames)) {
				element.classList.add(...classNames);
			} else {
				element.classList.add(classNames);
			}
		}
		if (content) {
			element.append(content);
		}
		return element;
	}

	export function appendNewElement<T extends keyof HTMLElementTagNameMap>(
		tag: T, parent: ParentNode, content?: string, classNames?: string[]): HTMLElementTagNameMap[T] {
		const element = makeElement(tag, content, classNames);
		parent.append(element);
		return element;
	}

	export function prependNewElement<T extends keyof HTMLElementTagNameMap>(
		tag: T, parent: ParentNode, content?: string, classNames?: string[]): HTMLElementTagNameMap[T] {
		const element = makeElement(tag, content, classNames);
		parent.prepend(element);
		return element;
	}

	export function appendTextNode(parent: ParentNode, text: string): void {
		parent.append(document.createTextNode(text));
	}

	export interface FormattedFragment {
		text: string;
		style: string | string[];
	}

	export type MaybeFormattedFragment = string | FormattedFragment;

	export function appendFormattedFragment(parent: ParentNode, text: FormattedFragment): HTMLSpanElement {
		return appendNewElement('span', parent, text.text, Array.isArray(text.style) ? text.style : [text.style]);
	}

	export function appendFormattedText(parent: ParentNode, ...text: readonly MaybeFormattedFragment[]): void {
		for (const t of text) {
			if (typeof t === "string") {
				$(parent).wiki(t);
			} else {
				appendNewElement('span', parent, t.text, Array.isArray(t.style) ? t.style : [t.style]);
			}
		}
	}

	type PassageLinkMap = Pick<HTMLElementTagNameMap, 'a' | 'audio' | 'img' | 'source' | 'video'>;
	type PassageLinkHandler = (passageName: string) => void;
	/**
	 * Creates a HTML element with custom SugarCube attributes which works as a passage link
	 *
	 * The result works in the same way as the wiki markup in the SugarCube
	 * @see https://www.motoslave.net/sugarcube/2/docs/#markup-html-attribute
	 * @param linkText link text
	 * @param passage the passage name to link to
	 * @param handler setter text (optional)
	 * @param tooltip text (optional)
	 * @param element type (optional) default is 'a'.
	 * Could be any of 'a', 'audio', img', 'source', 'video'
	 * @returns element text
	 *
	 * @example
	 * // equal to [[Go to town|Town]]
	 * passageLink("Go to town", "Town")
	 */
	export function passageLink(linkText: string, passage: string, handler?: PassageLinkHandler, tooltip?: string): HTMLAnchorElement;
	export function passageLink<L extends keyof PassageLinkMap>(linkText: string, passage: string, handler: PassageLinkHandler,
		tooltip: string, elementType: L): PassageLinkMap[L];
	export function passageLink<L extends keyof PassageLinkMap>(linkText: string, passage: string, handler?: PassageLinkHandler,
		tooltip = '', elementType?: L): PassageLinkMap[L] {
		const res = document.createElement(elementType ?? 'a');
		res.dataset.passage = passage;
		res.addEventListener('click', (ev) => {
			ev.preventDefault();
			if (handler) {
				handler(passage);
			}
			Engine.play(passage);
		});

		if (tooltip) {
			res.title = tooltip;
		}
		if (linkText.includes('@@') || linkText.includes('<')) {
			$(res).wiki(linkText);
		} else {
			res.textContent = linkText;
		}
		return res as PassageLinkMap[L];
	}

	/**
	 * Show a list of links (or disabled links) as a delimited strip
	 * @param {Array<Node|string>} links
	 * @returns {HTMLUListElement}
	 */
	export function linksStrip(...links: (Node | string)[]): HTMLUListElement {
		const strip = document.createElement('ul');
		strip.className = "choices-strip";

		for (const link of links) {
			const li = document.createElement("li");
			if (typeof link === "string") {
				$(li).wiki(link); // must be SC markup
			} else {
				li.append(link);
			}
			strip.append(li);
		}
		return strip;
	}

	export function toggleCollapsible(this: HTMLElement, styleName: string) {
		this.classList.toggle(styleName);
		const content = this.nextElementSibling as HTMLElement;
		content.style.maxHeight = content.style.maxHeight ? "" : `${content.scrollHeight}px`;
	}

	const colorScaleBaseColors = ['red', 'yellow', 'lime', 'cyan', 'blue', '#bf00ff'];
	const meterColors = ["red", "brown", "yellow", "cyan", "lime"];

	let colorScaler = chroma.scale(colorScaleBaseColors).mode('lab');
	let meterColorScaler = chroma.scale(meterColors).mode('lab');

	function resetColorScale() {
		const isLightTheme = chroma(getComputedStyle(document.body).backgroundColor).luminance() > 0.5;
		let baseColors: chroma.Color[] = [];
		let baseMeterColors: chroma.Color[] = [];
		if (isLightTheme) {
			baseColors = colorScaleBaseColors.map(c => chroma(c).darken(2));
			baseMeterColors = meterColors.map(c => chroma(c).darken(1.2));
		} else {
			baseColors = colorScaleBaseColors.map(c => chroma(c));
			baseMeterColors = meterColors.map(c => chroma(c));
		}
		colorScaler = chroma.scale(baseColors).mode('lab'); // uniform scale over the given colors
		meterColorScaler = chroma.scale(baseMeterColors).mode('lab');
	}

	/**
     * Returns a color that defines the percentage type of the "value" passed.
     * @param value
     * @param max - defaults to 100, Percent = Value / Opt
     */
	export function colorScale(value: number, max = 100): string {
		return colorScaler(Math.clamp(value / max, 0, 1)).hex();
	}

	/**
     * Returns a color that defines the percentage type of the "value" passed.
     * @param value
     * @param max - defaults to 100, Percent = Value / Opt
     */
	export function meterColor(value: number, max = 100): string {
		return meterColorScaler(Math.clamp(value / max, 0, 1)).hex();
	}

	export function scaleColorStrip(width: string, height: string): string {
		const res: string[] = [];
		res.push(`<div style="width:${width}">`);
		const stripElementStype = `display:inline-block;height:${height};width:1%;`
		const colors = colorScaler.colors(100);
		for (let i = 0; i < 100; ++i) {
			res.push(`<span style="${stripElementStype}background-color:${colors[i]}"></span>`);
		}
		res.push('</div>');
		return res.join('');
	}

	/**
	 * Renders out a 10 star meter surrounded with brackets.
	 * @param score - Current stat/score
	 * @param range - range of valid values (the meter is inverted when min > max)
	*/
	export function rMeter(score: number, range: {min: number, max: number},
		{inverseColors = false, showScore = false, showMax = false}
	): HTMLSpanElement {
		const clampedScore = range.max > range.min ? Math.clamp(score, range.min, range.max) : Math.clamp(score, range.max, range.min);
		const normalizedScore = Math.clamp((score - range.min) / (range.max - range.min), 0, 1);
		let sMeter = "";
		const stars = Math.floor((normalizedScore * 10));
		let i = 0;

		const res = document.createElement("span");
		res.append(document.createTextNode("["));
		const colorStars = document.createElement("span");
		for (i = 0; i < stars; i++) {
			sMeter += "★";
		}
		colorStars.textContent = sMeter;
		colorStars.style.color = meterColor(inverseColors ? 1 - normalizedScore : normalizedScore, 1);
		res.append(colorStars);

		if ((10 - stars) !== 0) {
			sMeter = "";
			for (i = 0; i < (10 - stars); i++) {
				sMeter += "★";
			}
			const greyStars = document.createElement("span");
			greyStars.textContent = sMeter;
			greyStars.style.color = "grey";
			res.append(greyStars);
		}
		res.appendTextNode(showScore || showMax ? "] ": "]");

		if (showScore && showMax) {
			res.appendTextNode(`${Math.floor(clampedScore).toFixed(0)}/${range.max.toFixed(0)}`)
		} else if (showScore) {
			res.appendTextNode(Math.floor(clampedScore).toFixed(0));
		} else if (showMax) {
			res.appendTextNode(range.max.toFixed(0));
		}

		return res;
	}

	export function rCoreStatMeter(statName: CoreStat, player: Entity.Player): HTMLSpanElement {
		const statValue = player.stat(Stat.Core, statName);
		const maxStat = player.maxStat(Stat.Core, statName);
		switch (statName) {
			case CoreStat.Hormones:
				const halfMax = maxStat / 2;
				// female/male version of the meter
				return statValue > halfMax
					? rMeter((statValue - halfMax), {min: 0, max: halfMax}, {showScore: settings.displayMeterNumber})
					: rMeter((halfMax - statValue), {min: halfMax, max: 0}, {showScore: settings.displayMeterNumber});
			case CoreStat.Toxicity:
				return rMeter(statValue, {min: 0, max: maxStat}, {inverseColors: true, showScore: settings.displayMeterNumber});
			default:
				return rMeter(statValue, {min: 0, max: maxStat}, {showScore: settings.displayMeterNumber});
		}
	}

	 /**
	 * Print out a 10 star colorized stat meter for a body part.
	 */
	export function rBodyMeter(statName: BodyStat, player: Entity.Player): HTMLSpanElement {
		const statConfig = PR.statConfig(Stat.Body)[statName];
		return rMeter(player.stat(Stat.Body, statName), statConfig,
			{inverseColors: statName === BodyPart.Waist, showScore: settings.displayMeterNumber});
	}

	export function rSkillMeter(skill: Skills.Any, player: Entity.Player): HTMLSpanElement {
		return rMeter(player.stat(Stat.Skill, skill), {min: 0, max: 100}, {showScore: settings.displayMeterNumber});
	}

	const putNPC = (npcElement: HTMLElement) => {
		const npcName = npcElement.dataset.npcName;
		const customArguments = npcElement.dataset.npcArgs;
		const npcArgument: PassageReference[] = [];
		if (customArguments?.length) {
			for (const arg of customArguments.split(';')) {
				if (arg.includes(',')) {
					const temporary = arg.split(',');
					npcArgument.push([temporary[0], temporary[1]]);
				} else {
					npcArgument.push(arg);
				}
			}
		}
		if (npcName?.length) {
			UI.replace(npcElement, wNPC(setup.world.pc, npcName, npcArgument)); // TODO custom params
		}
	};

	const putLocationJobs = (lj: HTMLElement) => {
		const jobLocation = lj.dataset.locationJob;
		if (jobLocation?.length) {
			const newContent = wLocationJobs(setup.world.pc, jobLocation);
			if (newContent) {
				UI.replace(lj, newContent);
			} else {
				lj.remove();
			}
		}
	};

	const putTravels = (content: HTMLElement, destinations: Data.Travel.Destinations) => {
		const travelDiv = content.querySelector<HTMLElement>("#travel");
		const travels = wTravels(setup.world, destinations);
		if (travels) {
			if (travelDiv) {
				UI.replace(travelDiv, travels);
				return null;
			} else {
				travels.style.marginTop = "2ex";
			}
		}
		return travels;
	};

	export function rPostProcess(passageName: string, content: HTMLElement): HTMLElement | null {
		// find all npcs
		const npcElements = content.querySelectorAll<HTMLElement>("[data-npc-name]");
		for (const npcElement of npcElements) {
			putNPC(npcElement);
		}

		// find all joblink
		const locationJobs = content.querySelectorAll<HTMLElement>("[data-location-job]");
		for (const lj of locationJobs) {
			putLocationJobs(lj);
		}

		const destinations = Data.Travel.destinations[passageName];
		if (destinations) {
			return putTravels(content, destinations);
		}
		return null;
	}

	export function loadTheme(name: string): void {
		const themeFileName = `./themes/theme-${name}.css`;
		importStyles(themeFileName)
			.then(() => {
				resetColorScale();
			},
			() => {
				console.error(`Could not load theme '${name} CSS file '${themeFileName}!`)
			});
	}
}
