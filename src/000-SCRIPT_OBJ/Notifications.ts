namespace App.Notifications {
	// Class for handling player notifications. Currently only affects the overnight sleeping, but could naturally
	// be extended to handle messages EOF messages of jobs (scenes)

	export enum MessageCategory {
		Dreams,
		StatChange,
		StatusChange,
		Knowledge
	}

	export class Engine {
		/** Array of message objects. */
		private readonly _messages: Message[] = [];

		private _getMessages(category: MessageCategory, day: number) {
			return this._messages.filter( o => o.category === category && o.day === day);
		}

		/**
		 * @returns Array of messages
		 */
		get messages(): Message[] {
			return this._messages;
		}

		clear(): void {
			this._messages.length = 0;
		}

		/**
		 *
		 * @param category Type of message.
		 * @param day Typically you want the player.Day + 1 to display on the sleep screen.
		 * @param message Message to display. Can have tokens added to it.
		 */
		addMessage(category: MessageCategory, day: number, message: string): void {
			if (category === MessageCategory.StatChange) {
				this._messages.push(new StatMessage(category, day, message));
			} else {
				this._messages.push(new Message(category, day, message));
			}
		}

		/**
		 * Print a plain string list of messages separated by optional character.
		 */
		strPrint(category: MessageCategory, day: number, header: string, color: string, bgColor: string,
			character = '<br>'): string {
			if (setup.world.debugMode) {
				console.debug(`Notification:StrPrint(${category},${day},${character}`);
			}
			let output = this._getMessages(category, day).map(o => o.print).join(character);
			if (output !== "") {
				output = Engine.header(header, color, bgColor) + output;
			}

			return output;
		}

		/**
		 * Format a header for a message group
		 * @param header Text to display
		 * @param color Text color
		 * @param bgColor Background color
		 */
		static header(header: string, color: string, bgColor: string): string {
			return "<div style='width:100%;margin-bottom:10px;padding-left:20px;font-weight:bold;color:" +
				color + ";background-color:" + bgColor + "'>" + header + "</div>";
		}
	}

	class Message {
		private readonly _category: MessageCategory;
		private readonly _day: number;
		private readonly _message: string;
		/**
		 *
		 * @param category Category or Section head of message.
		 * @param day Day message shall render on.
		 * @param message The text of the message.
		 */
		constructor(category: MessageCategory, day: number, message: string) {
			this._category = category;
			this._day = day;
			this._message = message;
		}

		get category() {return this._category;}
		get day() {return this._day;}
		get message() {return this._message;}

		get print() {
			return PR.tokenizeString(setup.world.pc, undefined, this._message);
		}
	}

	/**
	 * Reserved for future use.
	 */
	class StatMessage extends Message {
		constructor(category: MessageCategory, day: number, message: string) {
			super(category, day, message);
		}

		override get print() {
			return super.print;
		}
	}
}
