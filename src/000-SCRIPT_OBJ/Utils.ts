namespace App.Utils {
	/**
	 * replaces special HTML characters with their '&xxx' forms
	 */
	export function escapeHtml(text: string): string {
		/* eslint-disable @typescript-eslint/naming-convention */
		const map: Record<string, string> = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#039;',
		};
		/* eslint-enable @typescript-eslint/naming-convention */
		return text.replaceAll(/["&'<>]/g, m => map[m]);
	}

	export function gaussianPair(mu: number, sigma: number): [number, number] {
		let u = 2 * State.random() - 1;
		let v = 2 * State.random() - 1;
		let s = u * u + v * v;
		while (s === 0 || s > 1) {
			u = 2 * State.random() - 1;
			v = 2 * State.random() - 1;
			s = u * u + v * v;
		}

		const f = Math.sqrt(-2 * Math.log(s) / s) * sigma;
		return [u * f + mu, v * f + mu];
	}
}

namespace App {
	export function isDefined<T>(v: T): v is NonNullable <T> {
		return v !== undefined && v !== null;
	}

	export function assertIsDefined<T>(value: T): asserts value is NonNullable<T> {
		if (!isDefined(value)) {
			throw `Expected 'val' to be defined, but received ${value}`; // eslint-disable-line @typescript-eslint/restrict-template-expressions
		}
	}

	export function ensureIsDefined<T>(value: T): NonNullable<T> {
		if (isDefined(value)) return value;
		throw new Error('ensureIsDefined() encountered nullish value');
	}

	export function assertIsFinalRating(value: Data.RatingValue): asserts value is Data.RatingFinalValue {
		if (!(typeof value === "string" || Array.isArray(value))) {
			throw new TypeError("rating value is not final");
		}
	}

	export function valueOrDefault<T>(v: T, defaultValue: NonNullable<T>): NonNullable<T> {
		return isDefined(v) ? v : defaultValue;
	}

	export function assertUnreachable(x: never): never {
		// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
		throw new Error(`Reached supposed to be unreachable code with argument value of ${x}`)
	}

	export function registerTravelDestination(destinations: Record<string, Data.Travel.Destinations>): Record<string, Data.Travel.Destinations> {
		return Object.append(App.Data.Travel.destinations, destinations);
	}

	export function registerPassageDisplayNames(names: Record<string, DynamicText>): Record<string, DynamicText> {
		return Object.append(App.Data.Travel.passageDisplayNames, names);
	}

	interface PassageLink {
		link: string;
		text?: string;
		setter?: string;
	}

	const sc2LinkSimple = /\[\[([^\|^(?:\->)]+)(?:\||(?:\->))?(?:([^\]]+))?\](?:\[(.+)\])?\]/;

	export function parseSCPassageLink(s: string): PassageLink {
		const m = sc2LinkSimple.exec(s);
		if (!m) {
			throw `Link parsing error: '${s} does not look like a passage link to me`;
		}
		return {link: m[1], text: m[2], setter: m[3]};
	}

	/**
	 * Returns random element from a weighted table
	 */
	export function randomProperty<T extends PropertyKey>(choices: Partial<Record<T, number>>): EnumerablePropertyKey<T> {
		const totalWeight = _.sum(Object.values(choices));
		let rnd = State.random() * totalWeight;
		const res = Object.entries(choices).find((entry) => {
			rnd -= entry[1];
			return rnd <= 0;
		})
		if (!res) {
			throw new Error("Could not find anything in the choices object! Perhaps it's empty?")
		}
		return res[0];
	}

	export function splitSlashed<T1 extends string, T2 extends string>(id: `${T1}/${T2}`): [T1, T2] {
		const parts = id.split('/');
		return [parts[0] as T1, parts[1] as T2];
	}

	export function splitUnderscored<T1 extends string, T2 extends string>(id: `${T1}_${T2}`): [T1, T2] {
		const parts = id.split('_');
		return [parts[0] as T1, parts[1] as T2];
	}

	export function makeCompoundName<C extends string, T extends string>(c: C, t: T): `${C}/${T}` {
		return `${c}/${t}`;
	}

	export function splitCompoundName<C extends string, T extends string>(n: `${C}/${T}`): [C, T]  {
		return n.split('/') as [C, T];
	}

	export namespace Effects {

		function statEffectFunStat<T extends Stat>(human: Entity.Human, statType: T, effect: Partial<Record<StatTypeMap[T], Data.StatChangeEffect>>) {
			for (const [stat, value] of Object.entries(effect)) {
				if (value.xp) {
					human.adjustXP(statType, stat as StatTypeMap[T], value.xp, value.limiter);
				}
				if (value.value) {
					human.adjustStat(statType, stat as StatTypeMap[T], value.value);
				}
			}
		}

		function statEffectFun(statChanges: Data.EffectStatChanges, human: Entity.Human) {
			if (statChanges.core) {
				statEffectFunStat(human, Stat.Core, statChanges.core)
			}
			if (statChanges.body) {
				statEffectFunStat(human, Stat.Body, statChanges.body);
			}
			if (statChanges.skill) {
				statEffectFunStat(human, Stat.Skill, statChanges.skill);
			}
		}

		function makeStatEffectKnowledge(_statChanges: Data.EffectStatChanges): string[] {
			return [];
		}

		export function makeStatEffect(statChanges: Data.EffectStatChanges): Data.EffectDesc {
			return {
				value: statChanges.value,
				fun: h => statEffectFun(statChanges, h),
				knowledge: makeStatEffectKnowledge(statChanges),
			};
		}

		export type TemplateAction = (human: Entity.Human, amount: number) => void;
		export type TemplateData = Record<string, {points: number, value?: number, suffixCount: number}>;
		export interface ActionSet {
			knowledge: string;
			knowledgeSuffix: string;
			pointsFactor?: number;
			valueFactor?: number;
			data: TemplateData;
		}
		export function makeEffects(action: TemplateAction, data: Record<string, ActionSet>): Record<string, Data.EffectDesc> {
			const res: Record<string, Data.EffectDesc> = {};
			for (const baseName in data) {
				const set = data[baseName];
				for (const [act, d] of Object.entries(set.data)) {
					res[_.snakeCase(`${baseName}_${act}`).toUpperCase()] = {
						fun: p => action(p, d.points * (set.pointsFactor ?? 1)),
						value: (d.value ?? d.points) * (set.valueFactor ?? 1),
						knowledge: [`${set.knowledge}${(set.knowledgeSuffix ?? "").repeat(d.suffixCount)}`],
					};
				}
			}
			return res;
		}
	}

	export function compareDateTime(left: DateTime, right: DateTime): number {
		if (left.day !== right.day) {return left.day - right.day;}
		return left.phase - right.phase;
	}

	export namespace GameVariables {
		export function add(a: GameVariable, b?: GameVariable): GameVariable {
			if (!isDefined(b)) {
				return a;
			}
			if (typeof a === typeof b) {
				switch (typeof a) {
					case 'number':
					case 'string':
						// @ts-expect-error TS can't see that types of a and b are equal
						// eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/restrict-plus-operands
						return a + b;
					default:
						throw new Error(`Could not add values of type ${typeof a}`);
				}
			} else {
				throw new TypeError(`Could not add ${typeof a} to ${typeof b}`);
			}
		}

		export function toString<T extends GameVariable>(v: T | readonly T[], converter?: (v: T) => string): string {
			if (Array.isArray(v)) { // TODO readonly T[]
				return v.map(av => toString(av, converter)).join(',');
			}
			return converter ? converter(v) : `${v}`;
		}
	}
}

Object.defineProperty(Array.prototype, 'randomElement', {
	configurable: true,
	writable: true,

	value(this: unknown[]) {
		if (this.length === 0) {
			throw "randomElement: Array is empty";
		}

		return this.random();
	},
});

Object.defineProperty(Array.prototype, 'retrieve', {
	configurable: true,
	writable: false,

	value(this: unknown[], predicate: (value: unknown, index: number, obj: unknown[]) => unknown) {
		const res = this.find(predicate);
		if (res === undefined) {
			throw new Error("Could not find satisfying value in the array")
		}
		return res;
	},
});

Object.defineProperty(Object.prototype, 'append', {
	value(target: Record<PropertyKey, unknown>, source: Record<PropertyKey, unknown>): Record<PropertyKey, unknown> {
		for (const p in source) {
			if (target.hasOwnProperty(p)) {
				throw new Error(`Property ${p} already defined`);
			}
			target[p] = source[p];
		}
		return target;
	},
});

Object.defineProperty(Object.prototype, 'hasKey', {
	value(k: PropertyKey): boolean {
		return k in this;
	},
});

Object.defineProperty(Math, 'mean', {
	configurable: true,
	writable: true,

	value(v0: number, ...vn: number[]): number {
		let sum = v0;
		for (const v of vn) {
			sum += v;
		}
		return sum / (vn.length + 1);
	},
});

Object.defineProperty(_, 'pascalCase', {
	value(this: typeof _, s: string): string {
		return s.length > 0 ? s[0].toUpperCase() + this.camelCase(s.slice(1)) : s;
	},
});
