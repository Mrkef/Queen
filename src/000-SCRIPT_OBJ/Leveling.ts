namespace App.Leveling {
	export function levelingRecord<T>(rating: Record<number, T>, value: number): T {
		const res = Object.entries(rating).find(r => Number.parseInt(r[0]) >= value);
		if (res === undefined) {
			throw new Error(`Could not match value of ${value} to the rating table`);
		}
		return res[1];
	}

	/**
	* @summary Fetch a leveled value from the given rating for given values.
	*
	* This function descends into the passed ratings object using supplied index
	* value array. The rating object is indexed as using the first element of the
	* index array. The result of this operation is passed to this function again with
	* sliced index array. This continues until index array is exhausted or indexing
	* returns a simple string or array of strings (a random element of which is returned then).
	*
	* @param ratings object with ratings or the final string
	* @param value array of rating index values or a single value
	*/
	function multiIndexLevelingProperty(ratings: Data.RatingValue, value: number | number[]): string {
		if (typeof (ratings) === "string") return ratings; // value does not change

		let v = 0;
		let nextValues: number[] = [];
		if (Array.isArray(value)) {
			v = value[0];
			if (value.length > 1) nextValues = value.slice(1);
		} else {
			v = value;
		}
		const res = levelingRecord(ratings, v);
		if (nextValues.length === 0) {
			assertIsFinalRating(res);
			return Array.isArray(res) ? res.randomElement() : res;
		}
		return multiIndexLevelingProperty(res, nextValues);
	}

	export function isFixedLeveling(ld: Data.LevelingDataAny): ld is Data.FixedStatLevelingData {
		return ld.hasOwnProperty("fixed");
	}

	export function isNoneLeveling(ld: Data.LevelingDataAny): ld is Data.NoneStatLevelingData {
		return ld.hasOwnProperty("none");
	}

	export function isExplicitLeveling(ld: Data.LevelingDataAny): ld is Data.ExplicitStatLevelingData {
		return !isFixedLeveling(ld) && !isNoneLeveling(ld) && Object.entries(ld)[0][1].hasOwnProperty("cost");
	}

	function isLevelingRecordWithAdjective(lr: Data.StatLevelingRecordAny): lr is Data.StatLevelingRecordAdjective {
		return lr.hasOwnProperty("adjective");
	}

	/**
	 * Helper function. Checks relevant statistic config and returns the leveling record if one exists.
	 */
	function levelingRecordImpl(ratings: Data.LevelingDataAny, value: number): Data.StatLevelingRecordAny {
		if (isNoneLeveling(ratings)) {
			return ratings.none;
		}
		if (isFixedLeveling(ratings)) {
			return ratings.fixed;
		}
		return levelingRecord(ratings, value);
	}

	/**
	 * Helper function. Checks relevant statistic config and returns a colorized Property value for use if one exists.
	 */
	export function levelingProperty<T extends Stat>(type: T, stat: StatTypeMap[T], property: "adjective", value: number, colorize?: boolean): string;
	export function levelingProperty<T extends Stat>(type: T, stat: StatTypeStrMap[T], property: "adjective", value: number, colorize?: boolean): string;
	export function levelingProperty<T extends Stat>(
		type: T, stat: StatTypeMap[T], property: "adjective", value: number, colorize = false
	): string {
		const cfg = PR.statConfig(type)[stat];
		const propertyValue = levelingRecordImpl(cfg.leveling, value);
		if (!isLevelingRecordWithAdjective(propertyValue)) {
			return "";
		}

		const str = propertyValue[property];
		if (str === "" || !colorize) return str;

		return `<span style='color:${UI.colorScale(value - cfg.min, cfg.max - cfg.min)}'>${str}</span>`;
	}

	/**
	 * Returns config object which describe naming of the given type
	 * @param type Config type (StatType.Body)
	 */
	function namingConfig<T extends Stat>(type: T): Record<StatTypeMap[T], Data.RatedNounConfig> {
		type R = Record<StatTypeMap[T], Data.RatedNounConfig>;
		switch (type) {
			case Stat.Body: return Data.naming.bodyConfig as R;
			default: throw "Unexpected type";
		}
	}

	const ratedNounString = (cfg: Data.RatedNounConfig, v: number | number[]): string => {
		const nounRec = cfg.noun;
		if (typeof nounRec === "string") {
			return nounRec;
		}
		if (Array.isArray(nounRec)) {
			return nounRec.randomElement();
		}
		return multiIndexLevelingProperty(nounRec.leveling, v);
	};

	/**
	 * Helper function. Checks relevant statistic config and returns a NOUN (colorized) for use if one exists.
	 */
	export function noun<T extends Stat>(type: T, stat: StatTypeMap[T], value: number | number[], colorize = false): string {
		const nCfg = namingConfig(type);
		if (nCfg === undefined || !nCfg.hasOwnProperty(stat)) return "NO_NOUN_FOR_" + type + ":" + stat;

		const statCfg = nCfg[stat];
		const str = ratedNounString(statCfg, value);

		if (colorize) {
			const ratingValues = Array.isArray(value) ? clone(value) : [value];
			const ratingProperties = statCfg.adjective.rating.map(r => splitCompoundName(r));
			console.assert(ratingValues.length <= ratingProperties.length,
				"Values array is longer than the properties number (v: %d, p:%d) for %s/%s", ratingValues.length, ratingProperties.length, type, stat);
			for (let i = ratingValues.length; i < ratingProperties.length; ++i) {
				const missingStatCgf = PR.statConfig(ratingProperties[i][0])[ratingProperties[i][1]];
				ratingValues.push(Math.mean(missingStatCgf.min, missingStatCgf.max));
			}
			// we should compute the median value now, but all our indices are at most of length 2
			// thus it is the same as the mean
			const weightedValue: number[] = []
			for (let i = 0; i < ratingValues.length; ++i) {
				const cfg = PR.statConfig(ratingProperties[i][0])[ratingProperties[i][1]];
				weightedValue.push((ratingValues[i] - cfg.min) / (cfg.max - cfg.min));
			}

			return `<span style='color:${UI.colorScale(_.mean(weightedValue), 1)}'>${str}</span>`;
		}
		return str;
	}

	/**
	 * Returns array of applicable adjective values for a leveling noun
	 */
	export function nounAdjectives<T extends Stat>(type: T, stat: StatTypeMap[T], human: Entity.Human, colorize?: boolean): string[];
	export function nounAdjectives<T extends Stat>(type: T, stat: StatTypeStrMap[T], human: Entity.Human, colorize?: boolean): string[];
	export function nounAdjectives<T extends Stat>(type: T, stat: StatTypeMap[T], human: Entity.Human, colorize = false): string[] {
		const adjCfg = namingConfig(type)?.[stat]?.adjective;
		if (adjCfg === undefined) return [];

		const adjectiveRatings = adjCfg.rating || [type + '/' + stat];
		const stats = adjCfg.index ?? adjectiveRatings;
		const adjectiveApplicableLevels = adjCfg.applicableLevel ??
			adjectiveRatings.map(() => {return {min: 0, max: 100};});

		const adjectives: string[] = [];
		const statPercent = human.statPercent(type, stat);
		for (let i = 0; i < adjectiveRatings.length; ++i) {
			if (statPercent >= adjectiveApplicableLevels[i].min && statPercent <= adjectiveApplicableLevels[i].max) {
				const v = splitSlashed(stats[i]);
				const adjectiveStatPercent = human.statPercent(v[0], v[1]);
				const r = splitSlashed(adjectiveRatings[i]);
				adjectives.push(adjective(r[0] as StatTypeStr, r[1], adjectiveStatPercent, colorize));
			}
		}

		return adjectives;
	}

	function isFullRatedNoun(r: Data.RatingFinalValue | Data.SimpleRated | Data.FullRated): r is Data.FullRated {
		return r.hasOwnProperty("index");
	}

	/**
	 * Fetch index names required to find leveled value for a leveling property
	 * @param stat Stat type (SKILL, BODY, STAT)
	 * @param property Property name ("Bust", "Lips", etc.)
	 * @param aspect "noun", "adjective"
	 */
	function namingIndices<T extends Stat>(stat: T, property: StatTypeMap[T], aspect: Data.NamingAspect): Data.FullBodyCoreStatStr[] {
		const st = namingConfig(stat);
		switch (aspect) {
			case "adjective":
				const adj = st[property].adjective;
				return adj.index ??  [`${stat}/${property}` as Data.FullBodyCoreStatStr];
			case "noun":
				const noun = st[property].noun;
				return isFullRatedNoun(noun) ? noun.index :  [`${stat}/${property}` as Data.FullBodyCoreStatStr];
		}
	}

	/**
	 * Returns leveling noun, optionally with applicable adjectives prepended
	 * @param type
	 * @param stat
	 * @param human
	 * @param adjectives Whether to prepend adjectives
	 * @param colorize colorize output
	 */
	export function humanNoun<T extends Stat>(type: T, stat: StatTypeMap[T], human: Entity.Human,
		adjectives = false, colorize = false): string {
		const indexNames = namingIndices(type, stat, "noun");
		if (indexNames === undefined) return "NO_NOUN_FOR_" + type + ":" + stat;
		const indices = indexNames.map(s => splitCompoundName(s));
		const indexValues = indices.map(x => human.stat(x[0], x[1]));
		let str = noun(type, stat, indexValues, colorize);

		if (adjectives) {
			str = nounAdjectives(type, stat, human, colorize).join(' ') + ' ' + str;
		}
		return str;
	}

	/**
	 * Helper function. Get total amount of XP points needed to raise from a to b
	 */
	export function totalXPPoints<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], valueA: number, valueB: number): number {
		const statCfg = PR.statConfig(type)[stat];
		if (statCfg.levelingCost) {
			return statCfg.levelingCost(valueB) - statCfg.levelingCost(valueA);
		}

		const levelingCost = statCfg.leveling;

		if (isFixedLeveling(levelingCost)) {
			return levelingCost.fixed.cost * (valueB - valueA);
		}

		if (isNoneLeveling(levelingCost) || !isExplicitLeveling(levelingCost)) {
			return 0;
		}

		let lastLeveledTo = valueA;
		let res = 0;

		for (const entry of Object.entries(levelingCost)) {
			const propertyValue = Number.parseInt(entry[0]);
			if (propertyValue <= valueA) continue;
			if (propertyValue > valueB) break;

			res += (propertyValue - lastLeveledTo) * entry[1].cost;
			lastLeveledTo = propertyValue;
		}
		res += (valueB - lastLeveledTo) * levelingCost[lastLeveledTo].cost;
		return res;
	}

	/**
	 * Helper function. Checks relevant statistic config and returns an ADJECTIVE (colorized) for use if one exists.
	 */
	export function adjective<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], value: number, colorize?: boolean): string;
	export function adjective<T extends keyof StatTypeStrMap>(type: T, stat: StatTypeStrMap[T], value: number, colorize?: boolean): string;
	export function adjective<T extends keyof Data.StatConfigMap>(type: T, stat: StatTypeMap[T], value: number, colorize = false): string {
		return levelingProperty(type, stat, "adjective", value, colorize);
	}
}
