namespace App.Entity.DADNPC {

	export interface SliderItemTypeMap {
		'base': KeysMatching<Data.PresetData, number>;
		'basedim': DaBaseDim;
		'Mods': DaMod;
	}
	export interface ListItemTypeMap {
		'obj': "npclist";
		'arr': keyof typeof Data.Lists;
		'clothes': ClothingSlot;
		'weapon': ClothingSlot;
	}

	type SliderItemArgType = keyof SliderItemTypeMap;
	type SliderItemArgNestedType = Exclude<SliderItemArgType, 'base'>;
	type ListItemArgType = keyof ListItemTypeMap;

	interface SliderDataBase {
		label: string;
		min: number;
		max: number;
		step: number;
	}

	interface SliderData<T extends SliderItemArgType> extends SliderDataBase {
		type: T;
		attrib: SliderItemTypeMap[T];
	}

	type SliderDataAny = SliderData<SliderItemArgType>;

	interface PresetAttributeBase {
		type: 'base';
		attr: KeysMatching<Data.PresetData, number>;
	}

	interface PresetAttributeOther<T extends KeysMatching<Data.PresetData, object>>  {
		type: T;
		attr: KeysMatching<Data.PresetData[T], number>;
	}

	type PresetAttribute = PresetAttributeBase | PresetAttributeOther<KeysMatching<Data.PresetData, object>>;

	type NPCNumericAttribType<T extends keyof Data.PresetData | 'base'> = T extends keyof Data.PresetData ? KeysMatching<Data.PresetData[T], number> : KeysMatching<Data.PresetData, number>;

	type SliderIDStringTemplate<T extends keyof SliderItemTypeMap> = `${T}_${SliderItemTypeMap[T]}`;
	type ListIDStringTemplate<T extends keyof ListItemTypeMap> = `${T}_${ListItemTypeMap[T]}`;

	// export type IDString = IDStringTemplate<keyof GetItemsArgsTypeMap>;
	export type IDStringSlider = SliderIDStringTemplate<keyof SliderItemTypeMap>;
	export type IDStringList = ListIDStringTemplate<keyof ListItemTypeMap>;

	type IDString = IDStringList | IDStringSlider;

	interface ListElement {
		type: ListItemArgType;
		attrib: string;
		label: string;
		items: any[];
	}

	export class Avatar {
		private _canvasHeight = 800;
		private _canvasWidth = 360;
		private _canvasElement = "npcRender";
		private _portraitHeight = 240;
		private _portraitWidth = 180;
		private _portraitElement = "npcRenderPortrait";

		private _equip!: Data.NPCEquipData;

		private _npcData: Data.PresetData | null = null;

		private _sliders: Partial<Record<IDStringSlider, SliderDataAny>> = {};
		private _faceData: DaBodyPreset;

		private _lists: Partial<Record<IDStringList, ListElement>> = {};
		private _pc: QoSDaPlayer | null = null;
		private _loadID: string | null = null;
		private _init = false;
		private _root!: JQuery<HTMLElement>;

		constructor() {
			this._faceData = $.extend(true, {}, Data.DAD.faceStruct);
		}

		get npc() {return this._npcData;}
		get face() {return this._faceData;}
		get equip() {return this._equip;}
		get pc(): QoSDaPlayer| null {return this._pc;}
		get sliders() {return this._sliders;}
		get lists() {return this._lists;}
		get loadID() {return this._loadID;}

		init(id: string) {
			if (this._init) return;
			this._loadData(id);

			this._init = true;
		}

		reLoad(id: string) {
			this._loadData(id);

			Engine.play("NPCTester");
		}

		addList<T extends "obj" | "clothes">(type: T, attrib: ListItemTypeMap[T], label: string): string {
			const id: IDStringList = `${type}_${attrib}`;
			if (this._lists.hasOwnProperty(id) == false) {
				this._lists[id] = {
					type: type,
					attrib: attrib,
					label: typeof label === 'undefined' ? attrib : label,
					items: Avatar._getItems(type, attrib)!,
				};
			}

			return id;
		}

		addSlider(id: IDStringSlider, name: string, min: number, max: number, step: number) {
			const parts = splitUnderscored(id);

			this._sliders[id] = {
				type: parts[0],
				attrib: parts[1],
				label: name,
				min: min,
				max: max,
				step: step,
			}

			return id;
		}

		drawUI(rootID: string) {
			this._root = $("#" + rootID);
			$(document).one(":passagedisplay", this._drawUI.bind(this));
		}

		private _loadData(id: string) {
			this._npcData = _.merge({}, Data.dadNpc[id].data);
			this._equip = _.merge({}, Data.dadNpc[id].equip);

			// Copy face data structure and populate it from loaded npc data.
			this._faceData = _.merge({}, Data.DAD.faceStruct);

			for (const p of Object.keys(this._faceData.basedim)) {
				if (this._npcData.basedim.hasOwnProperty(p)) this._faceData.basedim[p] = this._npcData.basedim[p];
			}

			for (const p of Object.keys(this._faceData.Mods)) {
				if (this._npcData.Mods.hasOwnProperty(p)) this._faceData.Mods[p] = this._npcData.Mods[p];
			}
			this._loadID = id;
		}

		private _drawUI() {
			for (const sliderId of Object.keys(this.sliders)) {
				this._drawLabel(sliderId);
				this._drawSlider(sliderId);
				this._drawValue(sliderId);
			}

			for (const listId of Object.keys(this.lists)) {
				this._drawLabel(listId);
				this._drawDropDown(listId);
			}
		}

		drawCanvas(element: string, height: number, width: number) {
			this._canvasHeight = height;
			this._canvasWidth = width;
			this._canvasElement = element;
			$(document).one(":passageend", this._drawCanvas.bind(this));
		}

		private _drawCanvas() {
			let canvasGroup = null;
			if (typeof canvasGroup === 'undefined' || canvasGroup == null) {
				canvasGroup = da.getCanvasGroup(this._canvasElement,
					{
						border: "1px solid goldenrod",
						width: this._canvasWidth,
						height: this._canvasHeight,
					});
			}

			let pc = new QoSDaPlayer(this.npc);
			pc = this._attachParts(pc);
			void da.draw(canvasGroup, pc, {
				printHeight: false,
				printAdditionalInfo: false,
				renderShoeSideView: false,
				offsetX: 10,
				offsetY: 0,
			});
			this._pc = pc;
		}

		drawPortrait(element: string, height: number, width: number) {
			this._portraitElement = element;
			this._portraitHeight = height;
			this._portraitWidth = width;

			$(document).one(":passageend", this._drawPortrait.bind(this));
		}

		private _drawPortrait() {
			const canvasGroup = da.getCanvasGroup("hiddenPortraitCanvas", {
				border: "none",
				width: 2100,
				height: 3600,
			});

			let pc = new QoSDaPlayer(this.npc!);
			pc = this._attachParts(pc);

			void da.draw(canvasGroup, pc, {printHeight: false, printAdditionalInfo: false, renderShoeSideView: false})
				.then((exports) => {
				// draw just the head in a separate canvas
				// first retrieve/create the canvas if it's the first time we're getting it
					const portraitCanvas = da.getCanvas("portraitFace",
						{
							width: this._portraitWidth,
							height: this._portraitHeight,
							// can add any CSS style here like border
							border: "solid 1px goldenrod",
							// you can also position it absolutely
							// position: "absolute",
							// top     : "10px",
							// left    : "10px",
							// or relative to a parent
							position: "relative",
							parent: document.getElementById(this._portraitElement),
						});

					const eyeCanvas = da.getCanvas("portraitEye",
						{
							width: this._portraitWidth,
							height: this._portraitHeight,
							border: "solid 1px goldenrod",
							position: "relative",
							parent: document.getElementById(this._portraitElement + "Eye"),
						});

					// you can call this multiple times to draw different parts (with different canvases)
					da.drawFocusedWindow(portraitCanvas,
						exports,
						{
							center: exports[da.Part.RIGHT].neck.nape,
							width: 50,
							height: 50,
						});

					da.drawFocusedWindow(eyeCanvas,
						exports,
						{
							center: exports[da.Part.RIGHT].eyes.center,
							width: 15,
							height: 15,
						});
				});

			// da.hideCanvasGroup("hiddenCanvas");
		}

		private _drawLabel(id: IDString) {
			$(Avatar._getAttrib(id)).append(
				$("<div>").addClass("npcWorkshopLabel").text(this._getObj(id).label));
		}

		private _getSlider(id: IDStringSlider) {
			const res = this._sliders[id];
			assertIsDefined(res);
			return res;
		}

		private _getList(id: IDStringList) {
			const res = this._lists[id];
			assertIsDefined(res);
			return res;
		}

		private _drawDropDown(id: IDString) {
			const root = $("<select>").attr("id", id + "_DropDown").addClass("NPCDropDown");
			$(Avatar._getAttrib(id)).append(root);

			const items = (this._getObj(id) as ListElement).items;
			const selected = this._getSelectedItem(id);
			for (let i = 0; i < items.length; i++) {
				const opt = $('<option>').attr('value', items[i].v).text(items[i].t);
				if (items[i].v == selected) opt.attr('selected', 'selected');
				root.append(opt);
			}

			root.on("change", {id: id}, this._dropDownChanged.bind(this));
		}

		private _drawSlider(id: IDStringSlider) {
			const root = document.getElementById(id);
			assertIsDefined(root);

			const element = UI.appendNewElement('div', root, undefined, ["npcSliderClass"]);
			element.id = id + "_Slider";
			root.append(element);
			let statStart = this._getStat(id);

			if (statStart === undefined) {
				console.log("Unable to map property:" + id);
				statStart = this._getMin(id);
			}
			const slider = noUiSlider.create(element, {
				start: statStart,
				step: this._getStep(id),
				range: {
					min: [this._getMin(id)],
					max: [this._getMax(id)],
				},
			});
			const sliderData = this._getSlider(id);
			slider.on("change." + id, (v) => {this.doSliderUpdate(sliderData.type, sliderData.attrib, v as string[] /* , ID */);});
			slider.on("update." + id, (v) => {this.doSliderValUpdate(v[0] as string, id);});
		}

		private _drawValue(id: IDStringSlider) {
			const valStr = `${this._getStat(id)}`;
			$(Avatar._getAttrib(id)).append(
				$("<div>").addClass("npcWorkshopValue").attr("id", id + "_Value").html(this.doSliderValUpdate(valStr, id)));
		}

		private static _getAttrib(id: string) {
			return "#" + id;
		}

		private _getObj(id: IDString) {
			const parts = App.splitUnderscored(id);
			const type = parts[0];
			if (type === 'obj' || type === 'arr' || type === 'clothes' || type === 'weapon') {
				return this._getList(id as IDStringList);
			} else {
				return this._getSlider(id as IDStringSlider);
			}
		}

		private _getStep(id: IDStringSlider) {
			return this._getSlider(id).step;
		}

		private _getMin(id: IDStringSlider) {
			return this._getSlider(id).min;
		}

		private _getMax(id: IDStringSlider) {
			return this._getSlider(id).max;
		}

		/**
		 *
		 * @param id base_prop | basedim_prop | Mods_prop
		 */
		private _getStat(id: IDStringSlider): number {
			const npc = this.npc;
			assertIsDefined(npc);
			try {
				const parts = App.splitUnderscored(id);
				if (parts[0] === 'base') {
					return npc[parts[1] as SliderItemTypeMap['base']];
				}
				return _.get(npc, [parts[0], parts[1]]) as number;
			} catch (error) {
				console.log("Error:_getStat(" + id + ")");
				return Number.NaN;
			}
		}

		private _attachParts(pc: QoSDaPlayer) {
			if (pc.gender == 1 || pc.gender == 2) {
				const penis = da.Part.create(da.PenisHuman, {side: "right"});
				pc.attachPart(penis);
				const balls = da.Part.create(da.TesticlesHuman, {side: "right"});
				pc.attachPart(balls);
			}

			if (pc.gender === 1) {
				pc.removeSpecificPart(da.OversizedChest);
				pc.removeSpecificPart(da.ChestHuman);
			}

			if (pc.gender == 0 || pc.gender == 2) {
				const bust = da.Part.create(da.OversizedChest, {side: null});
				pc.attachPart(bust);
				pc.removeSpecificPart(da.NipplesHuman);
				const nips = da.Part.create(da.OversizedChestNipples, {side: null});
				pc.attachPart(nips);
			}

			if (pc.gender === 0) {
				pc.removeSpecificPart(da.PenisHuman);
				pc.removeSpecificPart(da.TesticlesHuman);
			}
			return this._clothesHandler(pc);
		}

		private _clothesHandler(pc: QoSDaPlayer) {
			for (const e of Object.entries(this.equip)) {
				const id = e[1];
				if (id === null) {continue;}
				const items = Data.AvatarMapping.clothes[id];
				if (items) {
					AvatarEngine.handleClothingItems(pc, items);
				}
			}

			return pc;
		}

		private static _getArrayItem(key: keyof typeof Data.Lists) {
			switch (key) {
				case 'hairStyles':
				case 'makeupStyles':
					return  Object.values(Data.Lists[key]).map(o => o.short);
				default: {
					const items = Data.Lists[key];
					if (Array.isArray(items) && items.length > 0 && typeof items[0] === 'object') {
						// return items.map(o => o.name);
						throw "Probably unused";
					}
				}
			}
			// return Data.Lists[key];
			throw "Unexpected array item key"
		}

		private static _getItems<T extends keyof ListItemTypeMap>(type: T, attrib: ListItemTypeMap[T]) {
			switch (type) {
				case 'obj':
					if (attrib === "npclist") {
						return Object.entries(Data.dadNpc).map((v, _i, _a) => new Object({t: v[1].data.name, v: v[0]}));
					} else {
						// return Object.keys([attrib]);
						throw `Invalid item obj attribute "${attrib}"`;
					}
				case 'arr':
					return this._getArrayItem(attrib as ListItemTypeMap['arr']);
				case 'weapon':
				case 'clothes':
					const items = [{t: "nothing", v: "nothing"}];
					Array.prototype.push.apply(items, Object.entries(App.Data.clothes)
						.filter(o => o[1].slot == attrib)
						.map((v, _i, _a) => new Object({t: v[0], v: v[0]})));
					return items;
			}
			throw 'Unexpected type';
		}

		private _getSelectedItem(id: IDString) {
			const o = this._getObj(id);

			switch (o.type) {
				case 'clothes':
				case 'weapon':
					const e = this.equip[o.label as keyof Data.NPCEquipData];
					if (e != null) return e;
					break;
			}

			switch (o.attrib) {
				case 'npclist':
					return this.loadID;
				case 'hairStyles':
					const key = this.npc;
					// @ts-expect-error TODO
					const res = Data.Lists[o.attrib].filter(o => o.short == key);
					if (res.length > 0) return res[0].short;
					break;
				case 'hairColors':
					return this.npc!.hairFill;
				case 'makeupStyles':
					return setup.world.pc.makeupStyle;
			}

			return undefined;
		}

		doSliderUpdate<T extends SliderItemArgType>(type: T, attr: SliderItemTypeMap[T], valAr: string[]) {
			if (!this.npc) {
				throw 'NPC is not set';
			}

			const val = parseFloat(valAr[0]);

			switch (attr) {
				case "fem":
					this.npc.basedim.legFem = (50 * (val / 11));
					break;
				case "upperMuscle":
					this.npc.basedim.lowerMuscle = val;
					break;
			}

			if (type === 'base') {
				this.npc[attr as NPCNumericAttribType<'base'>] = val;
			} else {
				_.set(this.npc, [type, attr], val);
				const ft = this.face[type as SliderItemArgNestedType];
				if (ft.hasOwnProperty(attr)) {
					ft[attr as (DaBaseDim)] = val; // TODO Can Mods slip in here? That would be an error?
				}
			}

			this._drawCanvas();
			this._drawPortrait();
			this.doSliderValUpdate(valAr[0], `${type}_${attr}`);
			this.exportData();
		}

		doSliderValUpdate(val: string, id: string) {
			$("#" + id + "_Value").html(val);
			return val;
		}

		private _dropDownChanged(e: JQuery.ChangeEvent<any, {id: IDString}>) {
			const o = this._getObj(e.data.id);
			const list = $("#" + e.data.id + "_DropDown");
			const val = list.val() as string;

			switch (o.type) {
				case 'clothes':
				case 'weapon':
					if (val == 'nothing') {
						this.equip[o.label as Data.NPCEquipSlot] = null;
					} else {
						this.equip[o.label as Data.NPCEquipSlot] = val;
					}
					this._drawCanvas();
					this._drawPortrait();
					break;
			}

			switch (o.attrib) {
				case 'npclist':
					this.reLoad(val);
					break;
				case 'hairStyles':
					// setup.world.pc.HairStyle = val;
					this._drawCanvas();
					// Avatar._DrawPortrait();
					break;
				case 'hairColors':
					// setup.world.pc.HairColor = val;
					this._drawCanvas();
					// Avatar._DrawPortrait();
					break;
			}

			this.exportData();
		}

		exportData() {
			const data = {
				data: this.npc,
				equip: this.equip,
			};

			$('#dataOutput').val(JSON.stringify(data));
			$('#faceDataOutput').val(JSON.stringify(this.face));
		}
	}
}
