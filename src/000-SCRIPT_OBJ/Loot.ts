namespace App {
	/**
	 * For generating random treasure scenarios
	 */
	export class Loot {
		readonly #prices: Record<Data.ItemNameTemplateAny, number> = {};
		static addPrices(prices: Record<Data.ItemNameTemplateAny, number>, type: Items.Category, list: Record<string, unknown>): void {
			for (const tag of Object.keys(list)) {
				prices[Items.makeId(type, tag)] = Items.calculateBasePrice(type, tag);
			}
		}

		constructor() {
			// Init vars
			// Populate
			Loot.addPrices(this.#prices, Items.Category.Clothes, Data.clothes);
			Loot.addPrices(this.#prices, Items.Category.Drugs, Data.drugs);
			Loot.addPrices(this.#prices, Items.Category.Food, Data.food);
			Loot.addPrices(this.#prices, Items.Category.Cosmetics, Data.cosmetics);
			Loot.addPrices(this.#prices, Items.Category.MiscConsumable, Data.misc);
			Loot.addPrices(this.#prices, Items.Category.Reel, Data.slots);
		}

		get prices(): Record<Data.ItemNameTemplateAny, number> {return this.#prices;}

		/**
		 *
		 * @param lists ['clothes', 'drugs', 'food', 'cosmetics', 'miscConsumable', 'reel' ]
		 * @param poolMin Minimum loot pool
		 * @param poolMax Maximum loot pool
		 * @param filter Function to apply to find item
		 */
		generateStash(lists: Items.Category[], poolMin: number, poolMax: number, filter: Data.ItemFilter<Items.Category>): Record<string, number> {
			let pool = Math.max(poolMin, Math.ceil(poolMax * State.random()));
			const loot: Record<string, number> = {};

			let i = 0;
			for (let item = this._findItem(lists.randomElement(), pool, filter); item !== null && i < 100;
				item = this._findItem(lists.randomElement(), pool, filter), ++i) { // safety
				if (item !== null) {
					// Add item to loot dictionary, or increment.
					if (loot.hasOwnProperty(item.record)) {
						loot[item.record] += 1;
					} else {
						loot[item.record] = 1;
					}
					pool -= item.price;
				}
				i++;
			}

			return loot;
		}

		/**
		 *
		 * @param table array of objects defining loot drop chances
		 * @param poolMin minimum amount of loot
		 * @param poolMax maximum amount of loot
		 */
		generateLootBox(table: Data.LootTableItem[], poolMin: number, poolMax: number): string {
			let buffer = "";
			const player = setup.world.pc;

			let pool = Math.ceil(poolMin + (poolMax - poolMin) * State.random());

			// Multiply loot value.
			pool = Math.ceil(pool * (player.hexStrength(GameState.VoodooEffect.TreasureFinder, 1)));
			player.removeHex(GameState.VoodooEffect.TreasureFinder);

			const counter: number[] = [];
			const loot: Record<string, PoolItem> = {};
			let coins = 0; // cash converts

			let i = 0;
			let breakcount = 0;

			while (pool > 0 && breakcount < 100) { // Break out after 100 rolls no matter what.
				const obj = table[i];

				if (Math.ceil(100 * State.random()) <= obj.chance) {
					// Handle money.
					if (obj.type === 'coins') {
						const temporaryCoins = Math.max(obj.min as number, Math.min(Math.ceil(obj.max as number * State.random()), pool));
						coins += temporaryCoins;
						pool -= temporaryCoins;
						if (pool <= 0) {
							break;
						} else {
							breakcount++;
							i++;
							i = i >= table.length ? 0 : i;
							continue;
						}
					}

					if (counter[i] >= (obj.maxCount ?? Number.MAX_SAFE_INTEGER)) {
						breakcount++;
						i++;
						i = i >= table.length ? 0 : i;
						continue;
					}

					// Fetch an item instead.
					const temporaryPool = (obj.hasOwnProperty("max") ?
						Math.max(obj.min as number, Math.min(Math.ceil(obj.max as number * State.random()), pool)) : pool);
					const item = this._findItem(obj.type, temporaryPool, obj.filter);

					if (item != null) {
						// Add item to loot dictionary, or increment.
						if (loot.hasOwnProperty(item.record)) {
							loot[item.record].count += 1;
							counter[i]++;
						} else {
							loot[item.record] = {...item, count: 1};
							loot[item.record].count = 1;
							counter[i] = 1;
						}

						pool -= (obj.hasOwnProperty('free') && obj.free) ? 0 : item.price;
					}

					// increment counters
					breakcount++;
					i++;
					i = i >= table.length ? 0 : i;
				}
			}

			const k = Object.keys(loot).sort((a, b) => loot[a].title.localeCompare(loot[b].title));
			if (coins === 0 && k.length === 0) return "<span style='color:red'>Poof! It's empty?!?!?</span>";

			for (i = 0; i < k.length; i++) {
				const obj = loot[k[i]];
				player.addItem(obj.category, obj.tag, obj.count);
			}

			if (coins) buffer += `You found <span class='item-money'>${coins} coins</span>!\n`;
			const out = k.map(o => loot[o].title + (loot[o].count > 1 ?
				`<span class='item-money'> x${loot[o].count}</span>` : ""));
			buffer += out.join("\n");

			if (coins) player.earnMoney(coins, GameState.IncomeSource.Loot);

			return buffer;
		}

		/**
		 *
		 * @param category Category to search.
		 * @param pool amount of funds available
		 * @param filter function to apply to result
		 */
		private _findItem(category: Items.Category, pool: number, filter?: Data.ItemFilter<Items.Category>): FoundItem | null {
			let ds = Items.tryGetItemsDictionary(category); // Create a data source.
			if (typeof filter === 'function') ds = filter(ds, category, pool); // optional filter.
			const tags = Object.keys(ds)
				.filter(k => ds[k].inMarket !== false &&
					(!Items.isEquipmentCategory(category) || !setup.world.pc.ownsWardrobeItem(k)) && // no duplicate clothing
					this.prices[Items.makeId(category, k)] <= pool);

			if (tags.length === 0) return null;
			const t = tags.randomElement();
			const record = Items.makeId(category, t);

			const destination = ds[t];
			const desc = Items.isEquipmentDesc(destination)
				? destination.shortDesc.replaceAll('{COLOR}', destination.color)
				: (Items.isInventoryDesc(destination) ? destination.shortDesc : destination.name);
			return {
				record: record, // CLOTHES/cheap wig
				price: this.prices[record], // number
				tag: t, // cheap wig
				category: category, // CLOTHES
				title: desc, // blah blah a cheap wig
			};
		}

		static get instance(): Loot {
			return setup.loot;
		}

		/**
		 * Filter creation shortcut
		 * @param test the test to apply to the object to
		 */
		private static _getFilter<C extends Items.Category>(test: (obj: Data.ItemTypeDescMap[C]) => boolean) {
			return (a: Items.ItemDescDictionary<Items.Category>, _b: unknown, _c: number) => {
				const o: Items.ItemDescDictionary<Items.Category> = {};
				for (const p in a)
					// @ts-expect-error FIXME this needs propagating the C type upstream
					if (test(a[p]))
						o[p] = a[p];
				return o;
			};
		}

		/**
		 * Simple comparison on a property.
		 * @param a Object property to test
		 * @param b value to test == or includes for arrays
		 */
		// static cmp(a: string, b: string | number | boolean) {
		// 	const t = (o: object) => {
		// 		const v = o[a];
		// 		return (v == b) || (Array.isArray(v) && v.includes(b));
		// 	};
		// 	return Loot._getFilter(t);
		// }

		/**
		 *
		 * @param a Object property to test
		 * @param values array of values to check
		 */
		static any<C extends Items.Category, K extends keyof Data.ItemTypeDescMap[C]>(a: K, values: AlwaysArray<Data.ItemTypeDescMap[C][K]>) {
			const t = (o: Data.ItemTypeDescMap[C]) => {
				const ov = o[a];
				return Array.isArray(ov) ? ov.includesAny(...values) : values.includes(ov);
			};

			return Loot._getFilter(t);
		}

		static makeLootTableItem<TCat extends Items.Category, K extends keyof Data.ItemTypeDescMap[TCat]>(
			cat: TCat, chance: number, maxCount: number, key?: K, values?: AlwaysArray<Data.ItemTypeDescMap[TCat][K]>,
			free?: boolean): Data.LootTableItem {
			const res: Data.LootTableItem = {
				type: cat,
				chance,
				maxCount,
			};
			if (key && values) {
				res.filter = Loot.any<TCat, K>(key, values)
			}
			if (free) {
				res.free = free;
			}
			return res;
		}

		static makeLootTableItemNoFilter(type:  Items.Category | "coins", chance: number, min: number, max: number): Data.LootTableItem {
			return {type, chance, min, max};
		}

		testLoot() {
			const table: Data.LootTableItem[] = [
				Loot.makeLootTableItem(Items.Category.Food, 50, 500, "effects", [
					"BUST_XP_COMMON", "BUST_XP_UNCOMMON",
					"BUST_XP_RARE", "BUST_XP_LEGENDARY",
				]),
			];

			return this.generateLootBox(table, 100, 1000);
		}
	}

	interface FoundItem {
		record: string; // CLOTHES/cheap wig
		price: number; // number
		tag: string; // cheap wig
		category: Items.Category; // CLOTHES
		title: string; // blah blah a cheap wig
	}

	interface PoolItem extends FoundItem {
		count: number;
	}
}
