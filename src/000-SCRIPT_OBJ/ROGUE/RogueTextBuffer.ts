
namespace App.Rogue {

	interface TextBufferOptions {
		display: ROT.Display;
		position: XY;
		size: XY;
		lines: number;
	}
	export class TextBuffer {
		#data: string[] = [];
		readonly #options: TextBufferOptions;

		constructor(options: TextBufferOptions) {
			this.#options = Object.assign({}, options);
		}

		configure(options: TextBufferOptions): void {
			Object.assign(this.#options, options);
		}

		clear(): void {
			this.#data = [];
		}

		private static _chunkString(str: string, length: number) {
			return str.match(new RegExp(`.{1,${length}}`, 'g')); // eslint-disable-line @typescript-eslint/prefer-regexp-exec
		}

		write(text: string): void {
			// Chunk long messages into buffer sized segments.
			const s = TextBuffer._chunkString(text, this.#options.size.x);
			console.log(s);
			this.#data = this.#data.concat(s?.toString() ?? "");
			// Only keep 'lines' length
			if (this.#data.length >= this.#options.lines) {
				this.#data.splice(0, this.#data.length - this.#options.lines);
			}
			// this._data.push(text);
		}

		flush(): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			/* clear */
			for (let i = 0; i < size.x; i++) {
				for (let j = 0; j < size.y; j++) {
					d.draw(pos.x + i, pos.y + j, null, null, null);
				}
			}

			const text = this.#data.join("\n");
			d.drawText(pos.x, pos.y, text, size.x);
		}
	}
}
