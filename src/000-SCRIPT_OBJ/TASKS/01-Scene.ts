namespace App {
	interface RewardItem {
		name: Data.ItemNameTemplateAny;
		value: number;
		opt?: "random" | "wear";
	}

	interface ItemChoiceElement {
		name: Data.ItemNameTemplateAny;
		value: number;
	}
	export class SceneRewards {
		pay: number;
		tokens: number;
		// ScaledValues: number[];
		items: (RewardItem|number)[];
		itemChoices: ItemChoiceElement[];
		chosenItem: number;
		slotUnlockCount: number;
		constructor() {
			this.pay = 0;
			this.tokens = 0;
			// this.ScaledValues = [];
			this.items = [];
			this.itemChoices = [];
			this.chosenItem = -1;
			this.slotUnlockCount = 0;
		}
	}

	export interface CalculatedScene {
		text: DocumentFragment;
		actions: Actions.Base[];
	}

	/**
	 * Stores and plays a "scene" from a job.
	*/
	export class Scene {
		private static _debug = false;
		private readonly _task: Task;
		protected player: Entity.Player;
		private readonly _sceneData: Data.Tasks.Scene;

		constructor(task: Task, player: Entity.Player, sceneData: Data.Tasks.Scene) {
			this._task = task;
			this.player = player;
			this._sceneData = sceneData;
		}

		get id(): string {return this._sceneData.id;}

		protected get task(): Task {
			return this._task;
		}

		/**
		 * Tokenize a string
		 */
		protected tokenize(str: string): string {
			return PR.tokenizeString(this.player, this.task.giver, str);
		}

		calculate(ctx: Actions.EvaluationContext, withText = true): CalculatedScene | null{
			const thisFunctionName = ":Scene.calculate";
			Scene.debug(this.id + thisFunctionName, "started");

			const triggersData = this._sceneData.triggers;
			let triggered = false;
			if (triggersData) {
				const triggers = new Conditions.Expression(triggersData);
				const computed = triggers.compute(ctx.conditionContext);
				triggered = computed.root.status(ctx.conditionContext);
			} else {
				triggered = true;
			}
			if (!triggered) {
				return null;
			}

			Scene.debug(this.id + thisFunctionName, 'scene triggered');

			if (this._sceneData.checks) {
				const checksExpr = new Conditions.Expression(this._sceneData.checks);
				checksExpr.compute(ctx.conditionContext);
			}

			const actions = Actions.parseActions(this._sceneData.post ?? []);
			for (const a of actions) {
				a.preCompute(ctx);
			}
			const text = new DocumentFragment();
			if (withText) {
				$(text).wiki(this.tokenize(this._sceneText(ctx.conditionContext)));
			}
			Scene.debug(this.id + thisFunctionName, "ended");
			return {text, actions};
		}

		/**
		 * Iterate through all of the text fragments, filter them basing on check results for a scene,
		 * and then combine them into a string.
		 */
		private _sceneText(checks: Conditions.EvaluationContext): string {
			const text = this._sceneData.text ?? "";
			if (typeof text === "string") {
				return text;
			}

			let results: string[] = [];
			const usedTags: Set<string> = new Set();
			let percent = 0;
			const colorize =  (a: string) => (a === undefined) ? "" : PR.colorizeString(percent, a.slice(2, -2), 100);

			for (const textFragment of text) {
				if (typeof textFragment === "string") {
					results.push(textFragment);
					usedTags.clear(); // allows to process the next set of choices
				} else {
					const tags = Object.keys(textFragment).filter(k => k !== "text");
					const combinedTag = tags.join('&');
					if (!usedTags.has(combinedTag)) {
						let pass = true;
						for (const t of tags) {
							console.assert(checks.hasTag(t) && typeof (textFragment[t]) === "number",
								`Wrong tag reference in the 'text' property of scene ${this._sceneData.id} for tag ${t}`);
							const checkValue = checks.tagged(t).checkValue;
							if (checkValue > textFragment[t]) {
								pass = false;
								break;
							} else {
								if (checks.tagged(t).target > 1) {
									percent = checkValue;
								}
							}
						}
						if (pass) {
							usedTags.add(combinedTag);
							 // use the last computed percent for the colorizing
							results.push(textFragment.text.replaceAll(/@@((?!color:).)+?@@/g, colorize));
						}
					}
				}
			}

			results = results.filter(r => r !== "");
			return results.length > 0 ? " " + results.join(" ") : "";
		}

		static get debugPrintEnabled(): boolean {
			return Scene._debug;
		}

		static set debugPrintEnabled(v: boolean) {
			Scene._debug = v;
		}

		static debug(fun: string, str: string): void {
			if (Scene.debugPrintEnabled)
				console.debug(fun + ":" + str + "\n");
		}
	}
}
