/* eslint-disable @typescript-eslint/no-unused-vars */

// https://stackoverflow.com/questions/40510611
type RequireAtLeastOne<T, Keys extends keyof T = keyof T> =
	Pick<T, Exclude<keyof T, Keys>>
	& {
		[K in Keys]-?: Required<Pick<T, K>> & Partial<Pick<T, Exclude<Keys, K>>>
	}[Keys];

type RequiredPick<T, K extends keyof T> = T & {[P in K]-?: T[P]};

type NonEmptyArray<T> = [T, ...T[]];

// https://stackoverflow.com/questions/62158066/typescript-type-where-an-object-consists-of-exactly-a-single-property-of-a-set-o
type Explode<T> = keyof T extends infer K
	? K extends unknown
		? {[I in keyof T]: I extends K ? T[I] : never}
		: never
	: never;

type AtMostOne<T> = Explode<Partial<T>>;
type AtLeastOne<T, U = {[K in keyof T]: Pick<T, K>}> = Partial<T> & U[keyof U]
type ExactlyOne<T> = AtMostOne<T> & AtLeastOne<T>

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AlwaysArray<T> = T extends any[] ? T : T[];

/** Convert union type to intersection #darkmagic. from ts-essentials */
type UnionToIntersection<U> = (U extends unknown ? (k: U) => void : never) extends (k: infer I) => void ? I : never;

type Primitive = string | number | boolean | bigint | symbol | undefined | null;
// eslint-disable-next-line @typescript-eslint/ban-types
type Builtin = Primitive | Function | Date | Error | RegExp;

type DeepRequired<T> = T extends Error
	? Required<T>
	: T extends Builtin
	? T
	: T extends Map<infer K, infer V>
	? Map<DeepRequired<K>, DeepRequired<V>>
	: T extends ReadonlyMap<infer K, infer V>
	? ReadonlyMap<DeepRequired<K>, DeepRequired<V>>
	: T extends WeakMap<infer K, infer V>
	? WeakMap<DeepRequired<K>, DeepRequired<V>>
	: T extends Set<infer U>
	? Set<DeepRequired<U>>
	: T extends ReadonlySet<infer U>
	? ReadonlySet<DeepRequired<U>>
	: T extends WeakSet<infer U>
	? WeakSet<DeepRequired<U>>
	: T extends Promise<infer U>
	? Promise<DeepRequired<U>>
	// eslint-disable-next-line @typescript-eslint/ban-types
	: T extends {}
	? {
		[K in keyof T]-?: DeepRequired<T[K]>;
	}
	: Required<T>;

// https://stackoverflow.com/questions/54520676/
type KeysMatching<T, V> = {[K in keyof T]-?: T[K] extends V ? K : never}[keyof T];

type DiscriminateUnion<T, K extends keyof T, V extends T[K]> = Extract<T, Record<K, V>>
type MapDiscriminatedUnion<T extends Record<K, string>, K extends keyof T> =
	{[V in T[K]]: DiscriminateUnion<T, K, V>};

type DiscriminateUnionWithoutKey<T, K extends keyof T, V extends T[K]> = Omit<Extract<T, Record<K, V>>, K>
type MapDiscriminatedUnionWithoutKey<T extends Record<K, string>, K extends keyof T> =
	{[V in T[K]]: DiscriminateUnionWithoutKey<T, K, V>};

type MapDiscriminatedUnionToOptions<T extends Record<K, string>, K extends keyof T> =
{[V in T[K]]: Partial<DiscriminateUnionWithoutKey<T, K, V>>};

/* eslint-enable @typescript-eslint/no-unused-vars */
