namespace App {

	export enum CoreStat {
		Health = "health",
		Nutrition = "nutrition",
		Willpower = "willpower",
		Perversion = "perversion",
		Femininity = "femininity",
		Fitness = "fitness",
		Toxicity = "toxicity",
		Hormones = "hormones",
		Energy = "energy",
		Futa = "futa",
	}

	export type CoreStatStr = Uncapitalize<keyof typeof CoreStat>;

	export enum BodyPart {
		Face = "face",
		Lips = "lips",
		Bust = "bust",
		Ass = "ass",
		Waist = "waist",
		Hips = "hips",
		Penis = "penis",
		Hair = "hair",
		Balls = "balls",
	}

	export type BodyPartStr = Uncapitalize<keyof typeof BodyPart>;

	export enum BodyProperty {
		BustFirmness = "bustFirmness",
		Height = "height",
		Lactation = "lactation"
	}

	export type BodyPropertyStr = `${BodyProperty}`;

	export type BodyStat = BodyPart | BodyProperty;
	export type BodyStatStr = BodyPartStr | BodyPropertyStr;

	export enum DerivedBodyStat {
		AssFirmness = "assFirmness",
	}

	export type DerivedBodyStatStr = `${DerivedBodyStat}`;

	export namespace Skills {
		export enum Charisma {
			Seduction = "seduction",
			Singing = "singing",
			Dancing = "dancing",
			Styling = "styling",
			Courtesan = "courtesan",
		}

		export type CharismaStr = `${Charisma}`;

		export enum Domestic {
			Cleaning = "cleaning",
			Cooking = "cooking",
			Serving = "serving",
		}

		export type DomesticStr = `${Domestic}`;

		export enum Piracy {
			Swashbuckling = "swashbuckling",
			Sailing = "sailing",
			Navigating = "navigating",
		}

		export type PiracyStr = `${Piracy}`;

		export enum Sexual {
			HandJobs = "handJobs",
			TitFucking = "titFucking",
			BlowJobs = "blowJobs",
			AssFucking = "assFucking",
			BoobJitsu = "boobJitsu",
			AssFu = "assFu"
		}

		export type SexualStr = `${Sexual}`;

		export type Any = Charisma | Domestic | Piracy | Sexual;
		export type AnyStr = CharismaStr | DomesticStr | PiracyStr | SexualStr;
	}

	export enum Stat {
		Core = "core",
		Skill = "skill",
		Body = "body"
	}

	export type StatTypeStr = `${Stat}`;

	export interface StatTypeMap {
		[Stat.Core]: CoreStat;
		[Stat.Skill]: Skills.Any;
		[Stat.Body]: BodyStat | DerivedBodyStat;
	}

	export interface StatTypeStrMap {
		"core": CoreStatStr;
		"skill": Skills.AnyStr;
		"body": BodyStatStr | DerivedBodyStatStr;
	}

	export enum NpcStat {
		Mood = "mood",
		Lust = "lust"
	}

	export type NPCStats = Record<NpcStat, number>;

	export const enum QuestStage {
		Intro = "intro",
		Middle = "middle",
		Finish = "finish",
		JournalEntry = "journalEntry",
		JournalComplete = "journalComplete"
	}

	export const enum QuestStatus {
		Unavailable,
		Available,
		Active,
		CanComplete,
		Completed,
	}

	// export type DayPhase = 0 | 1 | 2 | 3 | 4; // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night

	export enum DayPhase {
		Morning = 0,
		Afternoon = 1,
		Evening = 2,
		Night = 3,
		LateNight = 4
	}

	export interface DateTime {
		day: number;
		phase: DayPhase;
	}

	export enum ClothingSlot {
		Neck = "neck",
		Bra = "bra",
		Nipples = "nipples",
		Corset = "corset",
		Panty = "panty",
		Wig = "wig",
		Hat = "hat",
		Stockings = "stockings",
		Shirt = "shirt",
		Pants = "pants",
		Penis = "penis",
		Dress = "dress",
		Costume = "costume",
		Shoes = "shoes",
		Butt = "butt",
		Weapon = "weapon",
		Mascara = "mascara"
	}

	export namespace Items {
		export const enum Rarity {
			Common = "common",
			Uncommon = "uncommon",
			Rare = "rare",
			Legendary = "legendary"
		}

		export enum Category {
			Drugs = "drugs",
			Food = "food",
			Cosmetics = "cosmetics",
			MiscConsumable = "miscConsumable",
			MiscLoot = "miscLoot",
			Clothes = "clothes",
			Weapon = "weapon",
			Quest = "quest",
			LootBox = "lootBox",
			Reel = "reel"
		}
		export interface ItemTypeInventoryTypeMap {
			[Category.Drugs]: Entity.InventoryManager,
			[Category.Food]: Entity.InventoryManager,
			[Category.Cosmetics]: Entity.InventoryManager,
			[Category.MiscConsumable]: Entity.InventoryManager,
			[Category.MiscLoot]: Entity.InventoryManager,
			[Category.Clothes]: Entity.ClothingManager,
			[Category.Weapon]: Entity.ClothingManager,
			[Category.Quest]: Entity.InventoryManager,
			[Category.LootBox]: Entity.InventoryManager,
			[Category.Reel]: Entity.InventoryManager,
		}
	}

	export enum TaskType {
		Job = 'job',
		Quest = 'quest'
	}

	export namespace Combat {
		export enum Style {
			Unarmed,
			Swashbuckling,
			BoobJitsu,
			AssFu,
			Kraken,
			Siren,
			Boobpire,
		}

		export namespace Moves {
			export enum Unarmed {
				Punch = 'Punch',
				Kick = 'Kick',
				Haymaker = 'Haymaker',
				Knee = 'Knee',

			}

			export enum Swashbuckling {
				Slash = 'Slash',
				Stab = 'Stab',
				Parry = 'Parry',
				Riposte = 'Riposte',
				Cleave = 'Cleave',
				Behead = 'Behead',
			}

			export enum BoobJitsu {
				Jiggle = 'Jiggle',
				Wobble = 'Wobble',
				BustOut = 'Bust Out',
				Twirl = 'Twirl',
				BoobQuake = 'Boob Quake',
				TittyTwister = 'Titty Twister',
			}

			export enum AssFu {
				BootySlam = 'Booty Slam',
				ShakeIt = 'Shake It',
				Twerk = 'Twerk',
				AssQuake = 'Ass Quake',
				ThunderBuns = 'Thunder Buns',
				BunsOfSteel = 'Buns of Steel',
			}

			export enum Kraken {
				Grab = 'Grab',
				Strangle = 'Strangle',
				Mouth = 'Mouth',
				Ejaculate1 = 'Ejaculate1',
				Ass = 'Ass',
				Ejaculate2 = 'Ejaculate2',
			}

			export enum Siren {
				Touch = 'Touch',
				Toss = 'Toss',
				Scream = 'Scream',
				Drown = 'Drown',
			}

			export enum Boobpire {
				Touch = 'Touch',
				Toss = 'Toss',
				Bite = 'Bite',
				Claw = 'Claw',
			}
		}

		export interface StyleMovesMap {
			[Style.Unarmed]: Moves.Unarmed;
			[Style.Swashbuckling]: Moves.Swashbuckling;
			[Style.BoobJitsu]: Moves.BoobJitsu;
			[Style.AssFu]: Moves.AssFu;
			[Style.Kraken]: Moves.Kraken;
			[Style.Siren]: Moves.Siren;
			[Style.Boobpire]: Moves.Boobpire;
		}

		export enum Effect {
			Stunned,
			Blinded,
			Guarded,
			Dodging,
			Bloodthirst,
			Seeking,
			LifeLeech,
			Parry,
		}
	}
}
