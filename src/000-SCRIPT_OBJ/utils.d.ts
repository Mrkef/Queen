// implementations are in Utils.ts

declare global {
	interface Math {
		/** Returns mean value of the arguments */
		mean(v0: number, ...vn: number[]): number;
	}

	interface Object {
		// hasOwnProperty<P, X extends Partial<Record<string, P>>, Y extends PropertyKey>(this: X, prop: Y): this is X & Record<Y, P>;
		// hasOwnProperty<T>(this: T, k: PropertyKey): k is keyof T;
		hasKey<T>(this: T, k: PropertyKey): k is keyof T;
	}

	type EnumerablePropertyKey<T extends PropertyKey> = T extends symbol ? never : (T extends number ? string : T);

	interface ObjectConstructor {
		keys<K extends PropertyKey, V>(o: Partial<Record<K, V>>): EnumerablePropertyKey<K>[];
		entries<K extends PropertyKey, V>(o: {[P in K]?: V} | ArrayLike<V>): [EnumerablePropertyKey<K>, V][];
		/**
		 * Like @ref Object.assign(), but throws if any property in @see props attempts to
		 * overwrite a property with the same name in @see target
		 * @param target
		 * @param props
		 */
		append<T>(target: Record<PropertyKey, T>, properties: Record<PropertyKey, T>): Record<PropertyKey, T>;
	}

	/*
	type IfUnknownOrAny<T, Y, N> = unknown extends T ? Y : N;

	type ArrayType<T> = IfUnknownOrAny<
		T,
		T[] extends T ? T[] : any[] & T,
		Extract<T, readonly any[]>
	>;
	*/

	interface ArrayConstructor {
		// isArray<T>(arg: T): arg is ArrayType<T>;
		isArray(argument: unknown): argument is ReadonlyArray<unknown>;
	}

	interface Array<T> {
		randomElement(): T;
		retrieve(predicate: (value: T, index: number, obj: T[]) => unknown, thisArgument?: unknown): T;
	}
}

declare module "lodash" {
	interface LoDashStatic {
		/**
		 * Converts string to pascal case.
		 *
		 * @param string The string to convert.
		 * @return Returns the pascal cased string.
		 */
		pascalCase(string?: string): string;
	}
}
export {}
