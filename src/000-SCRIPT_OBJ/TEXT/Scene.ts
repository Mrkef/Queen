namespace App.Text {
	export class SceneContext implements Data.Text.Scenes.SceneContext {
		constructor(world: Entity.World, data?: Data.Text.Scenes.Scene) {
			this._world = world;
			this._actors = new Map<string, Actor>();
			this.addActor('pc', new ActorPc(world.pc));
			if (data) {
				for (const [nick, id] of Object.entries(data.actors)) {
					if (id.startsWith('npc:')) {
						this.addActor(nick, new ActorNpc(world.npc(id.slice(4))));
					} else {
						this.addActor(nick, new ActorHuman(world.character(id)));
					}
				}
			}
		}

		get world(): Entity.World {
			return this._world;
		}

		get pc(): Entity.Player {
			return this._world.pc;
		}

		actors(id: string): Actor {
			const res = this._actors.get(id);
			if (!res) {
				throw new Error(`Could nto find actor with id '${id}'`);
			}
			return res;
		}

		get actorPc(): ActorPc {
			return this._actors.get('pc') as ActorPc;
		}

		addActor(id: string, actor: Actor): void {
			if (this._actors.has(id)) {
				throw new Error(`Could not add actor to scene: actor with id '${id}' already exists`);
			}
			this._actors.set(id, actor);
		}

		actor(id: string): Actor {
			const res = this._actors.get(id);
			if (!res) {
				throw new Error(`Could not find actor with id '${id}' in the scene context`);
			}
			return res;
		}

		private readonly _world: Entity.World;
		private readonly _actors: Map<string, Actor>;
	}

	function actorProperty(actor: Actor, propertyChain: string): string {
		// eslint-disable-next-line you-dont-need-lodash-underscore/get
		const v = _.get(actor, propertyChain) as unknown;
		if (v === undefined || v === null) {
			console.error(`Could not find ${propertyChain} path while expanding string`);
			return 'NOT FOUND';
		}
		return v.toString();
	}

	function expandFlavouredText(actor: Actor, text: string): string {
		const replacer = (_match: string, styleList: string | null, text: string): string => {
			const cssClasses: string[] = clone(actor.cssClasses());
			const style: string[] = [];
			let id = "";
			if (styleList) {
				for (const styleListEntry of styleList.split(';')) {
					if (styleListEntry.length === 0) continue;
					switch (styleListEntry[0]) {
						case '#':
							id = styleListEntry.slice(1);
							break;
						case '.':
							cssClasses.push(styleListEntry.slice(1));
							break;
						default:
							style.push(styleListEntry);
					}
				}
			}
			let res = '<span ';
			if (id) {
				res += `id="${id}" `;
			}
			if (cssClasses.length > 0) {
				res += `class="${cssClasses.join(' ')}" `;
			}
			if (style.length > 0) {
				res += `style=${style.join(';')} `;
			}
			res += `>${text}</span>`;
			return res;
		};
		return text.replaceAll(/@@(.+;)?(.+?)@@/g, replacer);
	}

	function actorTextReplacer(match: string, actorId: string, arg: string | null, ctx: SceneContext): string {
		const actor = ctx.actor(actorId);
		if (arg === null || arg.length === 0) {
			return actor.nameFor(ctx.actorPc);
		}

		let expanded = "";

		const parts = arg.split('|');
		for (let part of parts) {
			if (part.startsWith('&')) {
				part = part.slice(1);
			} else {
				expanded += ' ';
			}
			if (part.length === 0) {
				expanded += actor.nameFor(ctx.actorPc);
				continue;
			}
			switch (part[0]) {
				case '.':
					expanded += actorProperty(actor, part.slice(1));
					break;
				case '(':
					if (part.last() !== ')') {
						throw new Error(`Invalid actor reference in ${match}: can't find closing bracket`);
					}
					expanded += actor.verb(part.slice(1, -1));
					break;
				default:
					expanded += expandFlavouredText(actor, expandSceneText(part, ctx));
					break;
			}
		}
		return expanded.trimStart();
	}

	export function expandSceneText(text: string, ctx: SceneContext): string {
		// we process actor references from back to start to handle them in the innermost to outermost order
		for (let start = text.lastIndexOf('@{'); start !== -1; start = text.lastIndexOf('@{')) {
			const end = text.indexOf('}', start + 1);
			if (end < 0) {
				console.error("Unbalanced actor reference pattern in:", text);
				throw new Error("Unbalanced actor reference pattern");
			}
			text = text.slice(0, start)
				+ text.slice(start + 2, end).replaceAll(
					/(\w+)(.*)/g, (match, actorId, arg) => actorTextReplacer(match, actorId as string, arg as string, ctx))
				+ text.slice(end + 1);
		}
		return text;
	}

	function choiceIsFragment(choice: Data.Text.Scenes.Choices.Any): choice is Data.Text.Scenes.Choices.NextFragment {
		return choice.hasOwnProperty('link');
	}

	abstract class Scene {
		private readonly _data: Data.Text.Scenes.Scene;
		private readonly _ctx: SceneContext;
		private readonly _container: HTMLDivElement;
		private readonly _currentContent: HTMLDivElement;
		private readonly _defaultCaption: Data.Text.Scenes.DynamicText;

		protected constructor(data: Data.Text.Scenes.Scene, ctx: SceneContext, parent: ParentNode) {
			this._data = data;
			this._ctx = ctx;

			this._container = UI.appendNewElement('div', parent, undefined, ['scene-container']);
			this._currentContent = UI.appendNewElement('div', this._container, undefined, ['scene']);
			this._defaultCaption = this._data.defaultCaption ?? "Continue…";
		}

		protected get container(): HTMLDivElement {return this._container;}
		protected get currentContent(): HTMLDivElement {return this._currentContent;}

		public playFragmentChain(fragmentId: Data.Text.Scenes.FragmentId): Promise<void> {
			return new Promise<void>((resolve) => {
				this._playFragment(fragmentId, resolve);
			});
		}

		private _playFragment(fragmentId: Data.Text.Scenes.FragmentId, resolver: () => void): void {
			const fragment = this._data.fragments[fragmentId];
			if (!fragment) {
				throw `Could not find fragment with Id '${fragmentId}' in the conversation`;
			}
			if (!fragment.enabled || fragment.enabled(this._ctx)) {
				// this.numberOfFragmentsPlayed++;
				if (fragment.action) {
					fragment.action(this._ctx);
				}
				this.appendFragment(this._getFragmentText(fragment.text));
			}
			if (!fragment.next) {
				resolver(); // end of the scene reached
				return;
			}
			if (typeof fragment.next === 'string' || typeof fragment.next === 'number') {
				return this._playFragment(fragment.next, resolver);
			}
			if (typeof fragment.next === 'function') { // DynamicText
				return this._playFragment(fragment.next(this._ctx), resolver);
			}
			const choices = fragment.next;
			this._renderChoices(choices, resolver);
		}

		protected expandDynamic<T extends string|number>(value: Data.Text.Scenes.DynamicSceneValue<T>): T {
			return typeof value === 'function' ? value(this._ctx) : value;
		}

		private _getFragmentText(text: Data.Text.Scenes.DynamicText): string {
			return expandSceneText(this.expandDynamic(text), this._ctx);
		}

		private _renderChoices(choices: Data.Text.Scenes.Choices.Any[], resolver: () => void) {
			const choicesDiv = UI.appendNewElement("div", this._currentContent);
			let isFirst = true;
			const choicesElements = choices
				.filter(c => Choices.isAvailable(c, this._ctx.world))
				.map((c) => {
					const link = document.createElement("a");
					let cap = this.expandDynamic(c.caption ?? this._defaultCaption);
					if (isFirst) {
						cap = _.capitalize(cap);
						isFirst = false;
					}
					UI.appendFormattedText(link, cap);
					if (c.hint) {
						link.classList.add('has-tooltip');
						tippy(link, {allowHTML: true, content: this.expandDynamic(c.hint)});
					}
					link.addEventListener('click', () => {
						if (choiceIsFragment(c)) {
							choicesDiv.remove(); // TODO maybe replace with the chosen option, if not the default one was taken
							this._playFragment(this.expandDynamic(c.link), resolver);
						} else {
							resolver();
							Engine.play(this.expandDynamic(c.passage));
						}
					});
					return link;
				});
			let first = true;
			for (const ce of choicesElements) {
				if (!first) {
					choicesDiv.append(document.createTextNode(" or "));
				}
				first = false;
				choicesDiv.append(ce);
			}
			if (choicesElements.length > 1) {
				choicesDiv.append(document.createTextNode('?'));
			}
		}

		protected abstract appendFragment(text: string): void;
	}

	class SinglePieceScene extends Scene {
		private _text = '';
		public constructor(data: Data.Text.Scenes.Scene, ctx: SceneContext, parent: ParentNode) {
			super(data, ctx, parent);
		}

		public override playFragmentChain(fragmentId: Data.Text.Scenes.FragmentId): Promise<void> {
			return super.playFragmentChain(fragmentId).then((value) => {
				UI.appendFormattedText(this.currentContent, this._text);
				return value;
			});
		}

		protected override appendFragment(text: string): void {
			this._text += text;
		}
	}

	class MultiPieceScene extends Scene {
		private readonly _coverButton: HTMLButtonElement;
		private readonly _pastContent: HTMLDivElement;

		public constructor(data: Data.Text.Scenes.Scene, ctx: SceneContext, parent: ParentNode) {
			super(data, ctx, parent);

			this._pastContent = UI.prependNewElement('div', this.container, undefined, ['scene', 'scene-past-fragments']);
			this._coverButton = UI.prependNewElement('button', this.container, "Previous…", ['scene-past-cover']);
			this._coverButton.addEventListener('click', UI.toggleCollapsible.bind(this._coverButton, 'scene-past-cover-active'));
		}

		protected override appendFragment(text: string): void {
			if (this.currentContent.childElementCount) {
				// move them to the past elements
				this._pastContent.append(...this.currentContent.children);
				this._coverButton.style.visibility = "visible";
			}
			const content = UI.appendNewElement('div', this.currentContent);
			UI.appendFormattedText(content, text);
		}
	}

	function choicesCount(data: Data.Text.Scenes.Scene): number {
		return Object.values(data.fragments).reduce(
			(total, fragment) => Array.isArray(fragment.next) ? total + 1 : total, 0);
	}

	export function playScene(data: Data.Text.Scenes.Scene, world: Entity.World, parent: ParentNode): Promise<void> {
		const sceneContext = new SceneContext(world, data);
		const epId = typeof (data.entry) === "string" || typeof data.entry === "number" ?
			data.entry : data.entry(sceneContext);
		const scene = Object.keys(data.fragments).length <= 1 || choicesCount(data) <= 1
			? new SinglePieceScene(data, sceneContext, parent)
			: new MultiPieceScene(data, sceneContext, parent);
		return scene.playFragmentChain(epId);
	}
}
