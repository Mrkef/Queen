namespace App.Text.Human {

	export interface                                  AspectDescriptionOptions {
		brief?: boolean;
		colorize?: boolean;
	}

	/**
	 * Provides text descriptions for nouns.
	 * There are four bits for each property:
	 *
	 * * full: colorful, full description
	 * * noun: only noun
	 * * cNoun: only noun, colorless
	 * * adj: colorful, brief description (adjectives)
	 * * cFull: colorless, full
	 * * cAdj: colorless, brief
	 * and a function pXxx(brief = false, color = true)
	 */
	export interface AspectDesc {
		/**
		 * colorful, full description
		 */
		get full(): string;
		/**
		 * colorless, full
		 */
		get cFull(): string;
		/**
		 * colorful, brief description (adjectives)
		 */
		get adj(): string;
		/**
		 * colorless, brief
		 */
		get cAdj(): string;
		/**
		 * only noun, colourfull
		 */
		get noun(): string;
		/**
		 * only noun, colorless
		 */
		get cNoun(): string;
		/**
		 * Expanded colourful description
		 */
		get desc(): string;

		pNoun(adjectives: boolean, colorize?: boolean): string;

		describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string;
	}

	export interface StatAspectDesc extends AspectDesc {
		/** raw stat value */
		raw: number;
	}

	interface SizebaleAspect {
		size: string;
		bSize: string;
		cSize: string;
		cbSize: string;
		sizeString(options?: AspectDescriptionOptions): string;
		/** raw stat value */
		raw: number;
	}

	interface BodyPartDesc extends AspectDesc, SizebaleAspect {
	}

	function isBodyPartDesc(aspect: AspectDesc): aspect is BodyPartDesc {
		return "cSize" in aspect;
	}

	/**
	 * Expands simple tokens in the rating string
	 *
	 * The tokens are:
	 * * ADJECTIVE adjective
	 * * NOUN noun full noun
	 * * LENGTH size
	 * * LENGTH_C compact size
	 * @param aspect
	 * @param str
	 * @returns
	 */
	function expandAspectRatingTokens(aspect: AspectDesc, str: string, colorize: boolean): string {
		function nounReplacer(_match: string, delimiter: string) {
			return (colorize ? aspect.noun : aspect.cNoun) + delimiter;
		}
		function adjReplace(_match: string, delimiter: string) {
			return (colorize ?  aspect.adj : aspect.cAdj) + delimiter;
		}
		if (isBodyPartDesc(aspect)) {
			str = str.replaceAll('LENGTH_C', aspect.sizeString({brief: true, colorize: true}));
			str = str.replaceAll('LENGTH', aspect.size);
		}
		str = str.replaceAll(/NOUN([^A-Za-z_|$])/g, nounReplacer);
		str = str.replaceAll(/ADJECTIVE([^A-Za-z_|$])/g, adjReplace);
		return str;
	}

	function expandRatingText(
		actor: Text.ActorHuman, aspect: AspectDesc, text: Data.DynamicRatingText, colorize: boolean, viewer?: Text.ActorHuman
	): string {
		const ctx = new Text.SceneContext(setup.world);
		ctx.addActor('o', actor);
		ctx.addActor('v', viewer ?? actor);

		const sceneText  = (typeof text === 'string') ? text : text(actor, viewer ?? actor);
		return Text.expandSceneText(expandAspectRatingTokens(aspect, sceneText, colorize), ctx);
	}

	type RatingKey = Exclude<BodyStatStr, "hair" | "height"> |
		Exclude<CoreStatStr, "energy" | "femininity" | "futa" | "health" | "hormones" | "nutrition"
			| "perversion" | "toxicity" | "willpower"> |
		"beauty" | "clothing" | "fetish" | "style";

	/**
	 * Fetch a rating for a statistic/value
	 */
	function rating(type: RatingKey, value: number, actor: Text.ActorHuman, aspect: AspectDesc, colorize = false, viewer?: Text.ActorHuman): string {
		const rating = Leveling.levelingRecord(Data.ratings[type], value);

		return expandRatingText(actor, aspect, rating, colorize, viewer);
	}

	abstract class AspectDescBase implements AspectDesc {
		readonly #parent: ActorHuman;
		constructor(parent: ActorHuman) {
			this.#parent = parent;
		}

		abstract get full(): string;
		abstract get cFull(): string;
		abstract get adj(): string;
		abstract get cAdj(): string;
		abstract get noun(): string;
		abstract get cNoun(): string;

		get desc(): string {
			return this.describe({colorize: true});
		}

		toString(): string {
			return this.full;
		}

		abstract pNoun(adjectives: boolean, colorize?: boolean): string;

		abstract describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string;

		protected get actor(): ActorHuman {
			return this.#parent;
		}

		protected get human(): Entity.Human {
			return this.#parent.entity;
		}
	}

	abstract class AspectDescImpl extends AspectDescBase {
		override get full(): string {
			return this.pNoun(true, true);
		}

		override get cFull(): string {
			return this.pNoun(true, false);
		}

		override get noun(): string {
			return this.pNoun(false, true);
		}

		override get cNoun(): string {
			return this.pNoun(true, false);
		}

		override get adj(): string {
			return this.describe({brief: true, colorize: true});
		}

		override get cAdj(): string {
			return this.describe({brief: true, colorize: false});
		}
	}

	abstract class RatedNounDesc<T extends Stat> extends AspectDescImpl implements StatAspectDesc {
		constructor(parent: ActorHuman, type: T, stat: StatTypeMap[T]) {
			super(parent);
			this._type = type;
			this._stat = stat;
		}

		override get adj(): string {
			return this._nounAdjectives(true).join(' ');
		}

		override get cAdj(): string {
			return this._nounAdjectives(false).join(' ');
		}

		get raw(): number {
			return this.human.stat(this._type, this._stat);
		}

		/**
		 * Returns leveling noun, optionally with applicable adjectives prepended
		 * @param adjectives Whether to prepend adjectives
		 * @param colorize colorize output
		 */
		pNoun(adjectives = false, colorize = false): string {
			return Leveling.humanNoun(this._type, this._stat, this.human, adjectives, colorize);
		}

		protected get stat(): StatTypeMap[T] {
			return this._stat;
		}

		private _nounAdjectives(colorize?: boolean): string[] {
			return Leveling.nounAdjectives(this._type, this._stat, this.human, colorize);
		}

		private readonly _type: T;
		private readonly _stat: StatTypeMap[T];
	}

	abstract class RatedBodyPart extends RatedNounDesc<Stat.Body> implements BodyPartDesc{
		get size(): string {
			return this.sizeString({brief: false, colorize: true});
		}

		get bSize(): string {
			return this.sizeString({brief: true, colorize: true});
		}

		get cSize(): string {
			return this.sizeString({brief: false, colorize: false});
		}

		get cbSize(): string {
			return this.sizeString({brief: true, colorize: false});
		}

		sizeString(options?: AspectDescriptionOptions): string {
			return unitSystem.lengthString(PR.statToCm(this.human, this.stat as BodyStat), options?.brief).toString()
		}
	}

	class StdRatedBodyPart extends RatedBodyPart {
		readonly #part: Exclude<BodyPart, BodyPart.Hair>;
		constructor(parent: ActorHuman, part: Exclude<BodyPart, BodyPart.Hair>) {
			super(parent, Stat.Body, part);
			this.#part = part;
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			const statValue = this.human.stat(Stat.Body, this.#part);
			return options?.brief
				? Leveling.adjective(Stat.Body, this.#part, statValue, options.colorize)
				: rating(this.#part, statValue, this.actor, this, options?.colorize, viewer ?? this.actor);
		}
	}

	class AssBodyPart extends RatedBodyPart {
		constructor(parent: ActorHuman) {
			super(parent, Stat.Body, BodyPart.Ass)
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			const assValue = this.human.stat(Stat.Body, BodyPart.Ass);
			const fitnessValue = this.human.stat(Stat.Core, CoreStat.Fitness);

			if (options?.brief) {
				return Leveling.adjective(Stat.Body, BodyPart.Ass, assValue, options.colorize) + ' '
					+ Leveling.adjective(Stat.Body, DerivedBodyStat.AssFirmness, fitnessValue, options.colorize);
			}

			let output = rating(BodyPart.Ass, assValue, this.actor, this, options?.colorize, viewer ?? this.actor);

			const hipsPercent = this.human.statPercent(Stat.Body, BodyPart.Hips);
			const assPercent = this.human.statPercent(Stat.Body, BodyPart.Ass);

			if ((assPercent > 30) || (hipsPercent > 30)) {
				if (assPercent < (hipsPercent - 15)) {
					output += ` It is ${PR.colorizeString(0.2, "disproportionately small", 1.0)} for ${this.actor.pr.his} `;
				} else if (assPercent > (hipsPercent + 15)) {
					output += ` It is ${PR.colorizeString(0.2, "disproportionately big", 1.0)} for ${this.actor.pr.his} `;
				} else {
					output += ` It is ${PR.colorizeString(0.4, "flattered", 1)} by ${this.actor.pr.his} `;
				}
				output += Leveling.adjective(Stat.Body, BodyPart.Hips, this.human.stat(Stat.Body, BodyPart.Hips), options?.colorize) + " hips.";
			}

			return output;
		}
	}

	class BustBodyPart extends RatedBodyPart {
		constructor(parent: ActorHuman) {
			super(parent, Stat.Body, BodyPart.Bust)
		}

		override sizeString(options?: AspectDescriptionOptions): string {
			return this._cup(options);
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			const bStat = this.human.stat(Stat.Body, BodyPart.Bust);
			if (options?.brief) {
				const fStat = this.human.stat(Stat.Body, BodyProperty.BustFirmness);
				return Leveling.adjective(Stat.Body, BodyPart.Bust, bStat, options.colorize) + ' '
					+ Leveling.adjective(Stat.Body, BodyProperty.BustFirmness, fStat, options.colorize);
			}
			return rating(BodyPart.Bust, bStat, this.actor, this, options?.colorize, viewer ?? this.actor);
		}

		private _cup(options?: AspectDescriptionOptions): string {
			const bustStatValue = this.human.stat(Stat.Body, BodyPart.Bust);
			const bustStatXP = this.human.statXP(Stat.Body, BodyPart.Bust);
			const cc = (Leveling.totalXPPoints(Stat.Body, BodyPart.Bust, 0, bustStatValue) + bustStatXP) / 3.23;
			let cupString = unitSystem.cupString(bustStatValue) + " cup";
			if (options?.colorize) {
				cupString = PR.colorizeString(bustStatXP, cupString, this.human.getLeveling(Stat.Body, BodyPart.Bust, 1).cost);
			}
			if (options?.brief) {
				return cupString;
			}
			cupString += " (" + unitSystem.massString(cc, true, 1000) + " each)";
			return options?.colorize
				? PR.colorizeString(bustStatValue, cupString, this.human.maxStat(Stat.Body, BodyPart.Bust))
				: cupString;
		}
	}

	class FaceBodyPart extends RatedBodyPart {
		constructor(parent: ActorHuman) {
			super(parent, Stat.Body, BodyPart.Face)
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			const faceValue = this.human.stat(Stat.Body, BodyPart.Face);
			if (options?.brief) return Leveling.adjective(Stat.Body, BodyPart.Face, faceValue, options.colorize);
			let output = rating(BodyPart.Face, faceValue, this.actor, this, options?.colorize, viewer);
			const ratedString = options?.colorize ? PR.colorizeString(this.human.makeupRating, this.human.makeupStyle) : this.human.makeupStyle
			if (this.human.makeupRating === 0) {
				output += " it is bare and devoid of cosmetics.";
			} else if (this.human.makeupRating < 40) {
				output += ` it is poorly done up in ${ratedString} makeup.`;
			} else if (this.human.makeupRating < 60) {
				output += ` it is moderately well done up in ${ratedString} makeup, somewhat enhancing ${this.actor.pr.his} appeal.`;
			} else if (this.human.makeupRating < 80) {
				output += ` it is expertly done up in ${ratedString} makeup, enhancing ${this.actor.pr.his} appeal.`;
			} else {
				output += ` it is flawlessly painted in ${ratedString} makeup, greatly enhancing ${this.actor.pr.his} appeal.`;
			}
			return output;
		}
	}

	class HairBodyPart extends RatedBodyPart {
		constructor(parent: ActorHuman) {
			super(parent, Stat.Body, BodyPart.Hair);
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			const viewerKnowsAboutWig = !viewer || viewer.isSame(this.actor);
			const wig = this.human.equipmentInSlot(ClothingSlot.Wig);
			if (wig && viewerKnowsAboutWig) {
				if (options?.brief) {
					return `${PR.lengthString(wig.hairLength)} ${wig.hairColor} wig`;
				}
				return `${this.actor.pr.He} ${this.actor.verb("is")} wearing a wig to hide ${this.actor.pr.his} natural hair. It is ${wig.hairColor} and \
					${PR.lengthString(wig.hairLength)} long, styled in ${PR.colorizeString(wig.hairBonus, wig.hairStyle)}.`;
			}
			if (options?.brief) {
				return PR.lengthString(PR.statToCm(this.human, BodyPart.Hair), false) + " " + this.human.hairColor;
			}
			return `${this.actor.pr.His} hair is ${this.human.hairColor} and ${PR.lengthString(PR.statToCm(this.human, BodyPart.Hair), false)} \
				long, styled in ${PR.colorizeString(this.human.hairRating, this.human.hairStyle)}.`;
		}
	}

	class HeightDesc implements SizebaleAspect {
		readonly #parant: ActorHuman;
		constructor(parent: ActorHuman) {
			this.#parant = parent;
		}

		get size(): string {
			return this.sizeString({colorize: true});
		}

		get bSize(): string {
			return this.sizeString({brief: true, colorize: true});
		}

		get cSize(): string {
			return this.sizeString();
		}

		get cbSize(): string {
			return this.sizeString({brief: true});
		}

		sizeString(options?: AspectDescriptionOptions): string {
			const pHeight = PR.statToCm(this.#parant.entity, BodyProperty.Height);
			return PR.lengthString(pHeight, true) + (options?.brief ? "" : " tall");
		}

		toString(): string {
			return this.sizeString({colorize: true});
		}

		get raw(): number {
			return this.#parant.entity.stat(Stat.Body, BodyProperty.Height);
		}
	}

	abstract class RatedAspectDesc extends AspectDescImpl implements StatAspectDesc{
		readonly #ratingKey: RatingKey;
		constructor(actor: ActorHuman, ratingKey: RatingKey) {
			super(actor);
			this.#ratingKey = ratingKey;
		}

		override pNoun(adjectives: boolean, colorize?: boolean | undefined): string {
			return adjectives
				? this.describe({colorize: colorize})
				: (colorize ? PR.colorizeString(this.raw, this.#ratingKey) : this.#ratingKey);
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			const value = this.raw;
			const desc = rating(this.#ratingKey, value, this.actor, this, options?.colorize, viewer);
			return options?.colorize ? PR.colorizeString(value, desc) : desc;
		}

		abstract get raw(): number;
	}

	class RatedStatDesc<T extends Stat, S extends StatTypeStrMap[T] = StatTypeStrMap[T]> extends AspectDescBase implements StatAspectDesc {
		readonly #statType: T;
		readonly #stat: S;
		constructor(actor: ActorHuman, statType: T, stat: S) {
			super(actor);
			this.#statType = statType;
			this.#stat = stat;
		}

		override get full(): string {
			return this.adj;
		}

		override get cFull(): string {
			return this.cAdj;
		}

		override get noun(): string {
			return this.cNoun;
		}

		override get cNoun(): string {
			return this.#stat;
		}

		get raw(): number {
			return this.human.stat(this.#statType, this.#stat);
		}

		override get adj(): string {
			return Leveling.levelingProperty(this.#statType, this.#stat, 'adjective', this.raw, true);
		}

		override get cAdj(): string {
			return Leveling.levelingProperty(this.#statType, this.#stat, 'adjective', this.raw, false);
		}

		override pNoun(adjectives: boolean, colorize?: boolean | undefined): string {
			return colorize
				? (adjectives ? this.adj : "") + this.noun
				: (adjectives ? this.cAdj : "") + this.cNoun;
		}

		override describe(options?: AspectDescriptionOptions, _viewer?: ActorHuman): string {
			return this.pNoun(!options?.brief, options?.colorize);
		}

		protected get statType(): T {
			return this.#statType;
		}

		protected get stat(): S {
			return this.#stat;
		}
	}

	class RatedDesc<Type extends Stat> extends RatedStatDesc<Type, Extract<StatTypeStrMap[Type], RatingKey>> {
		override get full(): string {
			return this.describe({colorize: true});
		}

		override get cFull(): string {
			return this.describe({colorize: false});
		}

		override describe(options?: AspectDescriptionOptions, viewer?: ActorHuman): string {
			return rating(this.stat, this.raw, this.actor, this, options?.colorize, viewer);
		}
	}

	//#region Look

	class FigureDesc extends AspectDescImpl implements StatAspectDesc {
		override describe(options?: AspectDescriptionOptions, _viewer?: ActorHuman): string {
			const pBust = PR.statToCm(this.actor.entity, BodyPart.Bust);
			const pWaist = PR.statToCm(this.actor.entity, BodyPart.Waist);
			const pHips = PR.statToCm(this.actor.entity, BodyPart.Hips);

			const statsString = `${PR.lengthValue(pBust)}-${PR.lengthValue(pWaist)}-${PR.lengthValue(pHips)} figure`;
			if (options?.brief) {
				return statsString;
			}

			const rBustHips = pBust / pHips;

			// Cases for being fat (too much waist). Go on a diet.
			if (pWaist >= 120) return `a ${PR.colorizeString(0, "morbidly obese", 1)} ${statsString}`;
			if (pWaist >= 100) return `an ${PR.colorizeString(0.05, "obese", 1)} ${statsString}`;
			if (pWaist >= 90) return `a ${PR.colorizeString(0.1, "fat", 1)} ${statsString}`;

			// Boobs and hips are in proportion
			if (rBustHips <= 1.1 && rBustHips >= 0.9) {
				if (pBust >= 95 && pWaist <= 75) return `an ${PR.colorizeString(1, "extreme hourglass", 1)} ${statsString}`;
				if (pBust >= 95) return `an ${PR.colorizeString(0.9, "hourglass", 1)} ${statsString}`;
				if (pBust >= 90) return `a ${PR.colorizeString(0.8, "very curvy", 1)} ${statsString}`;
				if (pBust >= 85) return `a ${PR.colorizeString(0.7, "curvy", 1)}  ${statsString}`;
				if (pBust >= 82) return `a ${PR.colorizeString(0.6, "slightly curvy", 1)} ${statsString}`;
				if (pWaist <= 55) return `a ${PR.colorizeString(0.5, "petite", 1)} ${statsString}`;
				if (pWaist <= 70) return `a ${PR.colorizeString(0.4, "slender", 1)} ${statsString}`;
				if (pWaist < 80) return `a ${PR.colorizeString(0.3, "thin", 1)} ${statsString}`;
				return `an ${PR.colorizeString(0.2, "average", 1)} ${statsString}`;
			}

			// Boobs are bigger than hips
			if (rBustHips >= 1.25 && pBust >= 90) return `a ${PR.colorizeString(0.6, "top heavy", 1)} ${statsString}`;

			if (rBustHips <= 0.75 && pHips >= 90) return `a ${PR.colorizeString(0.6, "bottom heavy", 1)} ${statsString}`;

			if (pWaist <= 55) return `a ${PR.colorizeString(0.4, "petite", 1)} ${statsString}`;
			if (pWaist <= 70) return `a ${PR.colorizeString(0.4, "slender", 1)} ${statsString}`;
			if (pWaist < 80) return `a ${PR.colorizeString(0.4, "thin", 1)} ${statsString}`;

			return `an ${PR.colorizeString(0.2, "average", 1)} ${statsString}`;
		}

		override pNoun(adjectives: boolean, colorize?: boolean): string {
			return this.describe({brief: !adjectives, colorize: colorize});
		}

		get raw(): number {
			return this.human.figure;
		}
	}

	class BeautyDesc extends RatedAspectDesc {
		constructor(actor: ActorHuman) {
			super(actor, "beauty");
		}

		override get raw(): number {
			return this.human.beauty;
		}
	}

	class ClothingDesc extends RatedAspectDesc {
		constructor(actor: ActorHuman) {
			super(actor, "clothing");
		}

		override get raw(): number {
			return this.human.clothesRating;
		}
	}

	class FetishDesc extends RatedAspectDesc {
		constructor(actor: ActorHuman) {
			super(actor, "fetish");
		}

		override get raw(): number {
			return this.human.fetish;
		}
	}

	class StyleDesc extends RatedAspectDesc {
		constructor(actor: ActorHuman) {
			super(actor, "style");
		}

		override get raw(): number {
			return this.human.style;
		}
	}

	class MakeupDesc extends AspectDescImpl implements StatAspectDesc {
		pNoun(_adjectives: boolean, colorize?: boolean): string {
			return colorize ? PR.colorizeString(this.human.makeupRating, "makeup") : "makeup";
		}

		override describe(_options?: AspectDescriptionOptions, _viewer?: ActorHuman): string {
			return this.actor.entity.makeupStyle === Data.Fashion.Makeup.Plain
				? `${this.actor.pr.He} ${this.actor.verb("is")} plain faced and not wearing any makeup`
				: `${this.actor.pr.He} ${this.actor.verb("is")} wearing ${PR.colorizeString(this.human.makeupRating, this.human.makeupStyle)} makeup`;
		}

		get raw(): number {
			return this.human.makeupRating;
		}
	}

	class EyesDesc extends AspectDescImpl {
		pNoun(adjectives: boolean, _colorize?: boolean): string {
			return adjectives ? `${this.human.eyeColor} eyes` : "eyes";
		}

		override describe(options?: AspectDescriptionOptions, _viewer?: ActorHuman): string {
			if (options?.brief) {
				return `${this.human.eyeColor} eyes`;
			}
			const lashes = this.human.statPercent(Stat.Core, CoreStat.Hormones) >= 75 ? "long" : "average length";
			return `${this.actor.pr.He} ${this.actor.verb("have")} ${lashes} eyelashes and ${this.human.eyeColor} colored eyes.`;
		}
	}

	//#endregion

	interface BodyPropertyDescMap {
		[BodyPart.Ass]: BodyPartDesc;
		[BodyPart.Balls]: BodyPartDesc;
		[BodyPart.Bust]: BodyPartDesc;
		[BodyPart.Face]: StatAspectDesc;
		[BodyPart.Hair]: BodyPartDesc;
		[BodyPart.Hips]: BodyPartDesc;
		[BodyPart.Hips]: BodyPartDesc;
		[BodyPart.Lips]: StatAspectDesc;
		[BodyPart.Penis]: BodyPartDesc;
		[BodyPart.Waist]: BodyPartDesc;
		[BodyProperty.BustFirmness]: StatAspectDesc;
		[BodyProperty.Height]: SizebaleAspect;
		[BodyProperty.Lactation]: StatAspectDesc;
	}

	type BodyStatDesc = {
		[X in BodyStat]: BodyPropertyDescMap[X];
	}

	export interface Body extends BodyStatDesc {
		eyes: AspectDesc;
	}

	export interface Core extends Record<CoreStatStr, AspectDesc> {

	}

	export interface Look {
		figure: StatAspectDesc;
		beauty: StatAspectDesc;
		style: StatAspectDesc;
		makeup: StatAspectDesc;
		clothing: StatAspectDesc;
		fetish: StatAspectDesc;
	}

	export class CoreDescImpl implements Core {
		constructor(actor: ActorHuman) {
			this._parts = {
				energy: new RatedStatDesc(actor, Stat.Core, CoreStat.Energy),
				femininity: new RatedStatDesc(actor, Stat.Core, CoreStat.Femininity),
				fitness: new RatedDesc(actor, Stat.Core, CoreStat.Fitness),
				futa: new RatedStatDesc(actor, Stat.Core, CoreStat.Futa),
				health: new RatedStatDesc(actor, Stat.Core, CoreStat.Health),
				hormones: new RatedStatDesc(actor, Stat.Core, CoreStat.Hormones),
				nutrition: new RatedStatDesc(actor, Stat.Core, CoreStat.Nutrition),
				perversion: new RatedStatDesc(actor, Stat.Core, CoreStat.Perversion),
				toxicity: new RatedStatDesc(actor, Stat.Core, CoreStat.Toxicity),
				willpower: new RatedStatDesc(actor, Stat.Core, CoreStat.Willpower),
			};
		}

		get health(): AspectDesc {return this._parts.health;}
		get nutrition(): AspectDesc {return this._parts.nutrition;}
		get willpower(): AspectDesc {return this._parts.willpower;}
		get perversion(): AspectDesc {return this._parts.perversion;}
		get femininity(): AspectDesc {return this._parts.femininity;}
		get fitness(): AspectDesc {return this._parts.fitness;}
		get toxicity(): AspectDesc {return this._parts.toxicity;}
		get hormones(): AspectDesc {return this._parts.hormones;}
		get energy(): AspectDesc {return this._parts.energy;}
		get futa(): AspectDesc {return this._parts.futa;}

		private readonly _parts: Record<CoreStatStr, AspectDesc>;
	}

	export class BodyDesc implements Body {
		constructor(actor: ActorHuman) {
			this._parts = {
				ass: new AssBodyPart(actor),
				balls: new StdRatedBodyPart(actor, BodyPart.Balls),
				bust: new BustBodyPart(actor),
				face: new FaceBodyPart(actor),
				eyes: new EyesDesc(actor),
				hair: new HairBodyPart(actor),
				hips: new StdRatedBodyPart(actor, BodyPart.Hips),
				lips: new StdRatedBodyPart(actor, BodyPart.Lips),
				penis: new StdRatedBodyPart(actor, BodyPart.Penis),
				waist: new StdRatedBodyPart(actor, BodyPart.Waist),
				height: new HeightDesc(actor),
				lactation: new RatedDesc(actor, Stat.Body, BodyProperty.Lactation),
				bustFirmness: new RatedDesc(actor, Stat.Body, BodyProperty.BustFirmness),
			}
		}

		get ass(): BodyPartDesc {return this._parts.ass;}
		get balls(): BodyPartDesc {return this._parts.balls;}
		get bust(): BodyPartDesc {return this._parts.bust;}
		get eyes(): AspectDesc {return this._parts.eyes;}
		get face(): StatAspectDesc {return this._parts.face;}
		get hair(): BodyPartDesc {return this._parts.hair;}
		get hips(): BodyPartDesc {return this._parts.hips;}
		get lips(): StatAspectDesc {return this._parts.lips;}
		get penis(): BodyPartDesc {return this._parts.penis;}
		get waist(): BodyPartDesc {return this._parts.waist;}
		get height(): SizebaleAspect {return this._parts.height;}
		get lactation(): StatAspectDesc {return this._parts.lactation;}
		get bustFirmness(): StatAspectDesc {return this._parts.bustFirmness;}

		private readonly _parts: Body;
	}

	export class LookImpl implements Look {
		readonly #beauty: BeautyDesc;
		readonly #clothing: ClothingDesc;
		readonly #fetish: FetishDesc;
		readonly #figure: FigureDesc;
		readonly #makeup: MakeupDesc;
		readonly #style: StyleDesc;

		constructor(actor: ActorHuman) {
			this.#beauty = new BeautyDesc(actor);
			this.#clothing = new ClothingDesc(actor);
			this.#fetish = new FetishDesc(actor);
			this.#figure = new FigureDesc(actor);
			this.#makeup = new MakeupDesc(actor);
			this.#style = new StyleDesc(actor);
		}

		get beauty(): StatAspectDesc {
			return this.#beauty;
		}

		get figure(): StatAspectDesc {
			return this.#figure;
		}

		get makeup(): StatAspectDesc {
			return this.#makeup;
		}

		get style(): StatAspectDesc {
			return this.#style;
		}

		get clothing(): StatAspectDesc {
			return this.#clothing;
		}

		get fetish(): StatAspectDesc {
			return this.#fetish;
		}
	}
}
