namespace App.Choices {
	export function applyChoiceAction<Context>(data: Data.Choices.Choice<Context>, ctx: Context, world: Entity.World): void {
		if (!data.action) {
			return;
		}
		if (typeof data.action === 'function') {
			data.action(ctx);
		} else {
			const actions = Actions.parseActions(data.action);
			const actionContext = new Actions.EvaluationContext(new Conditions.EvaluationContext({world}));
			for (const a of actions) {
				a.preCompute(actionContext);
			}
			for (const a of actions) {
				a.apply(actionContext);
			}
		}
	}

	export function isAvailable<Context>(data: Data.Choices.Choice<Context>, world: Entity.World): boolean {
		if (!data.available) {
			return true;
		}

		// TODO maybe grantsXp: true is en error
		return new Conditions.Expression(data.available).compute(new Conditions.EvaluationContext({world})).status;
	}
}
