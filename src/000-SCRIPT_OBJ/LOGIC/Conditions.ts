namespace App {

	export namespace Conditions {

		export interface EvaluationOptions {
			grantsXp: boolean;
		}

		export class ActivatorResult {
			value: number;
			target: number;

			constructor(value: number, target: number) {
				this.value = value;
				this.target = target;
			}

			get mod(): number {
				return this.value / this.target;
			}

			get percent(): number {
				return Math.floor(this.mod * 100);
			}

			/**
			 * The value used in task checks
			 */
			get checkValue(): number {
				return this.percent;
			}

			static readonly EMPTY: ActivatorResult = new ActivatorResult(0, 0);
		}

		export class EvaluationContext extends App.EvaluationContext<Base, ActivatorResult>{
			#taggedValues: Record<string, Base> = {};
			readonly #defaults: Data.Conditions.Defaults;

			constructor(definition: DefinitionContext, defaults?: Data.Conditions.Defaults) {
				super(definition);
				this.#defaults = defaults ?? {};
			}

			storeNumeric(activator: Base, value: number, target: number, tag?: string) {
				super.store(activator, new ActivatorResult(value, target));
				if (tag) {
					console.debug("Storing numeric task result for tag", tag, "with value",
						this.getNumeric(activator).value, "for target", this.getNumeric(activator).target, '(', this.getNumeric(activator).percent, "%)");
					if (this.#taggedValues.hasOwnProperty(tag)) {
						throw new Error("Overwriting activator tags are not allowed");
					}
					this.#taggedValues[tag] = activator;
				}
			}

			storeBool(activator: Base, value: boolean, target: boolean, tag?: string) {
				this.storeNumeric(activator, value ? 1 : 0, target ? 1 : 0, tag);
			}

			getNumeric(activator: Base, defaultValue?: ActivatorResult) {
				const value = this.get<ActivatorResult>(activator, defaultValue);
				if (value === undefined) {
					throw "No stored value for this activator";
				}
				return value;
			}

			getBool(activator: Base) {
				return this.getNumeric(activator).value ? true : false;
			}

			tagged(tag: string, defaultValue?: ActivatorResult): ActivatorResult {
				return this.getNumeric(this.#taggedValues[tag], defaultValue);
			}

			allTags(): string[] {
				return Object.keys(this.#taggedValues);
			}

			hasTag(tag: string): boolean {
				return this.#taggedValues.hasOwnProperty(tag);
			}

			get defaults(): Data.Conditions.Defaults {return this.#defaults;}

			static makeFreeContext(): EvaluationContext {
				return new EvaluationContext({world: setup.world}, {body: {noXp: true}})
			}

			static readonly REQUIREMENTS_CHECK_DEFAULTS: Data.Conditions.Defaults = {
				body: {raw: true, noXp: true},
				core: {raw: true, noXp: true},
				skill: {raw: true, noXp: true},
				meta: {raw: true},
			};
		}

		export abstract class Base {
			abstract evaluate(ctx: EvaluationContext): void;
			abstract status(ctx: EvaluationContext): boolean;
			abstract visible(ctx: EvaluationContext): boolean;
			abstract acceptVisitor(visitor: ActivatorVisitor): void;
		}

		function replaceAlternateContionTerms(c: Data.Conditions.EqualityCondition): Data.Conditions.EqualityConditionMain;
		function replaceAlternateContionTerms(c: Data.Conditions.Condition): Data.Conditions.ConditionMain;
		function replaceAlternateContionTerms(c: Data.Conditions.Condition): Data.Conditions.ConditionMain {
			const map: Record<Data.Conditions.ConditionAlternate, Data.Conditions.ConditionMain> = {
				eq: '==',
				ne: '!=',
				gt: '>',
				gte: '>=',
				lt: '<',
				lte: '<=',
			};
			for (const [alternate, main] of Object.entries(map)) {
				if (alternate === c) {
					return main;
				}
			}
			return c as Data.Conditions.ConditionMain;
		}

		function numericConditionOrDefault(c?: Data.Conditions.Condition): Data.Conditions.ConditionMain {
			return replaceAlternateContionTerms(c ?? '>=');
		}

		function boolConditionOrDefault(c?: Data.Conditions.EqualityCondition): Data.Conditions.EqualityConditionMain {
			return replaceAlternateContionTerms(c ?? '==');
		}

		function boolConditionOrDefaultCheck(c?: Data.Conditions.Condition): Data.Conditions.EqualityConditionMain {
			const res = replaceAlternateContionTerms(c ?? '==');
			switch (res) {
				case '<':
				case '<=':
				case '>':
				case '>=':
					throw new Error(`Numeric condition ${c} is not allowed for non-numeric values`);
				default:
					return res;
			}
		}

		abstract class Individual<TType extends string, TData extends Data.Conditions.Base<TType>> extends Base {
			readonly #data: TData;

			constructor(data: TData) {
				super();
				this.#data = data;
			}

			override visible(ctx: EvaluationContext): boolean {
				return this.data.altTitle === null ? false : (this.#data.hidden ? this.status(ctx) : true);
			}

			get data(): TData {
				return this.#data;
			}
		}

		// type Individual2DataType<T> = T extends Individual<infer _, infer TData> ? TData : never;

		type NonNumericValues = string | boolean;

		abstract class Bool<
			TType extends string,
			TValue extends NonNumericValues,
			TData extends Data.Conditions.SimpleValue<TType, TValue>
		> extends Individual<TType, TData> {
			get condition(): Data.Conditions.EqualityConditionMain {
				return replaceAlternateContionTerms(this.data.condition ?? '==');
			}

			override evaluate(ctx: EvaluationContext): void {
				ctx.storeBool(this, cmp(this.currentValue(ctx), this.targetValue(ctx), this.condition), true, this.data.tag);
			}

			override status(ctx: EvaluationContext): boolean {
				return ctx.getBool(this);
			}

			targetValue(_ctx: EvaluationContext) {
				return this.data.value;
			}

			protected abstract currentValue(ctx: EvaluationContext): TValue;
		}

		abstract class Numeric<
			TType extends string,
			TData extends Data.Conditions.SimpleNumeric<TType>
		> extends Individual<TType, TData> {
			override evaluate(ctx: EvaluationContext): void {
				const targetValue = this.targetValue(ctx) + ((State.random() * 2 - 1) * (this.data.randomShift ?? 0));
				ctx.storeNumeric(this, this.currentValue(ctx), targetValue, this.data.tag);
			}

			override status(ctx: EvaluationContext): boolean {
				const res = ctx.getNumeric(this);
				return cmp(res.value, res.target, this.condition);
			}

			static substituteDefaultCondition(c?: Data.Conditions.Condition): Data.Conditions.Condition {
				return c ?? ">="; // default condition for numeric requirements
			}

			targetValue(_ctx: EvaluationContext): number {
				return this.data.value;
			}

			get condition(): Data.Conditions.ConditionMain {
				return replaceAlternateContionTerms(Numeric.substituteDefaultCondition(this.data.condition));
			}

			protected abstract currentValue(ctx: EvaluationContext): number;
		}

		export class RequirementGroup extends Base {
			readonly #op: Data.Conditions.BinaryOp;
			readonly #title: string | null;
			protected items: Base[] = [];

			constructor(op: Data.Conditions.BinaryOp = '&&', title?: string) {
				super();
				this.#op = op;
				this.#title = title ?? null;
			}

			get op() {
				return this.#op;
			}

			get title(): string | null {
				return this.#title;
			}

			add(r: Base): void {
				this.items.push(r);
			}

			override evaluate(ctx: EvaluationContext): void {
				for (const item of this.items) {
					item.evaluate(ctx);
				}
			}

			override status(ctx: EvaluationContext): boolean {
				if (this.items.length === 0) {
					return true;
				}
				switch (this.#op) {
					case '||': return this.items.some(r => r.status(ctx));
					case '&&': return this.items.every(r => r.status(ctx));
				}
			}

			/**
			 * Combines evaluate() and status() and evaluates only so many of sub-expressions
			 * until the status can be computed
			 */
			evaluateFast(ctx: EvaluationContext): boolean {
				if (this.items.length === 0) {
					return true;
				}

				const evaluateAndCheckStatus = (r: Base) => {
					r.evaluate(ctx);
					return r.status(ctx);
				}
				switch (this.#op) {
					case '||': return this.items.some(evaluateAndCheckStatus);
					case '&&': return this.items.every(evaluateAndCheckStatus);
				}
			}

			override visible(ctx: EvaluationContext): boolean {
				return this.items.some(item => item.visible(ctx));
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.beginGroup(this);
				for (const item of this.items) {
					item.acceptVisitor(visitor);
				}
				visitor.endGroup();
			}

			get length(): number {
				return this.items.length;
			}
		}

		abstract class VariableGeneric<
			TType extends string,
			TValue extends GameVariable,
			TData extends Data.Conditions.GameVariableBase<TType, TValue>
		> extends Individual<TData['type'], TData> {
			override evaluate(ctx: EvaluationContext): void {
				const value = ctx.gameVariableValue(this.data.name, ctx.definedVariableScope(this.data.scope));
				ctx.storeBool(this, cmp(value, this.data.value, this.condition), true, this.data.tag);
			}

			override status(ctx: EvaluationContext): boolean {
				return ctx.getBool(this);
			}

			protected abstract get condition(): Data.Conditions.ConditionMain;
		}

		class Variable extends VariableGeneric<Data.Conditions.Variable['type'], GameVariable, Data.Conditions.Variable> {
			protected override get condition(): Data.Conditions.ConditionMain {
				if ((this.data.value === true || this.data.value === false)) {
					return replaceAlternateContionTerms(this.data.condition ?? '==');
				}
				return replaceAlternateContionTerms(this.data.condition ?? '>=');
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitVariable(this);
			}
		}

		class VariableBoolean extends VariableGeneric<Data.Conditions.VariableBoolean['type'], boolean, Data.Conditions.VariableBoolean> {
			protected override get condition(): Data.Conditions.ConditionMain {
				return replaceAlternateContionTerms(this.data.condition ?? '==');
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitVariableBoolean(this);
			}
		}

		class VariableString extends VariableGeneric<Data.Conditions.VariableString['type'], string, Data.Conditions.VariableString> {
			protected override get condition(): Data.Conditions.ConditionMain {
				return replaceAlternateContionTerms(this.data.condition ?? '==');
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitVariableString(this);
			}
		}

		class VariableNumeric extends Numeric<Data.Conditions.VariableNumeric['type'], Data.Conditions.VariableNumeric> {
			protected override currentValue(ctx: EvaluationContext): number {
				const v = ctx.gameVariableValue(this.data.name, ctx.definedVariableScope(this.data.scope));
				if (typeof v !== 'number') {
					throw new TypeError(`'var:n' refers to a game variable those type is ${typeof v}`);
				}
				return v;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitVariableNumeric(this);
			}
		}

		abstract class CharacterStat<
			Type extends Stat,
			TName extends StatTypeMap[Type]
		> extends Numeric<Type, Data.Conditions.StatSkillCheck<Type, TName>>{
			constructor(data: Data.Conditions.NamedNumericCharacter<Type, TName>) {
				super(data);

				if (data.character && data.character !== "pc") {
					throw "Stat and skill checks for non-player characters are not yet supported";
				}
			}

			character(ctx: EvaluationContext): Entity.Human {
				return ctx.world.character(this.data.character);
			}

			protected override currentValue(ctx: EvaluationContext): number {
				const defaults = ctx.defaults[this.data.type] as Partial<Data.Conditions.StatSkillCheck<Type, TName>> | undefined;
				if (this.data.raw ?? defaults?.raw) {
					return this.character(ctx).statPercent(this.data.type, this.data.name)
				} else {
					if (this.data.value === 0) {
						throw new Error("stat test agains 0 fails unless made raw");
					}
					const pc = this.character(ctx) as Entity.Player;
					return this.data.type === Stat.Skill && !this.data.noXp && !defaults?.noXp
						? pc.skillRoll(this.data.name as Skills.Any, this.data.value) * this.data.value
						: pc.statRoll(this.data.type, this.data.name, this.data.value) * this.data.value;
				}
			}
		}

		class CoreStat extends CharacterStat<Stat.Core, App.CoreStat> {
			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitCoreStat(this);
			}
		}

		class BodyStat extends CharacterStat<Stat.Body, App.BodyStat> {
			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitBodyStat(this);
			}
		}

		class SkillStat extends CharacterStat<Stat.Skill, App.Skills.Any> {
			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitSkill(this);
			}
		}

		class Meta extends Numeric<Data.Conditions.Meta['type'], Data.Conditions.Meta> {
			private _statValue(ctx: EvaluationContext) {
				switch (this.data.name) {
					case 'danceStyle':
						return (ctx.player.getStyleSpecRating(
							Data.Fashion.Style.SexyDancer) + ctx.player.getStyleSpecRating(Job.getDance())) / 2;
					default:
						return ctx.player[this.data.name];
				}
			}

			protected override currentValue(ctx: EvaluationContext): number {
				return this.data.raw ?? ctx.defaults.meta?.raw
					? this._statValue(ctx)
					: Entity.Player.genericRoll(this._statValue(ctx), this.data.value) * this.data.value;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitMeta(this);
			}
		}

		class HairColor extends Bool<Data.Conditions.HairColor['type'], boolean, Data.Conditions.HairColor> {
			protected override  currentValue(ctx: EvaluationContext): boolean {
				const getColor = () => {
					switch (this.data.hairType) {
						case 'wig':
							return ctx.world.character(this.data.character).equipmentInSlot(ClothingSlot.Wig)?.color;
						case 'natural':
							return ctx.world.character(this.data.character).hairColor;
						default:
							return ctx.world.character(this.data.character).getHairColor();
					}
				};
				return getColor() === this.data.name;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitHairColor(this);
			}
		}

		class HairStyle extends Bool<Data.Conditions.HairStyle['type'], boolean, Data.Conditions.HairStyle> {
			protected override currentValue(ctx: EvaluationContext): boolean {
				const getStyle = () => {
					switch (this.data.hairType) {
						case 'wig':
							return ctx.world.character(this.data.character).equipmentInSlot(ClothingSlot.Wig)?.style;
						case 'natural':
							return ctx.world.character(this.data.character).hairStyle;
						default:
							return ctx.world.character(this.data.character).getHairStyle();
					}
				};
				return getStyle() === this.data.name;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitHairStyle(this);
			}
		}

		class DaysPassed extends Numeric<Data.Conditions.DaysPassed['type'], Data.Conditions.DaysPassed> {
			// eslint-disable-next-line class-methods-use-this
			protected override currentValue(ctx: EvaluationContext): number {
				return ctx.world.day;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitDaysPassed(this);
			}
		}

		class EventCount extends Numeric<Data.Conditions.EventCount['type'], Data.Conditions.EventCount> {
			protected override currentValue(ctx: EvaluationContext): number {
				return ctx.player.flags[EventEngine.eventCountKey(this.data.name)] as number ?? 0;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitEventCount(this);
			}
		}

		class Equipped extends Bool<Data.Conditions.Equipped['type'], boolean, Data.Conditions.Equipped> {
			protected override currentValue(ctx: EvaluationContext): boolean {
				return ctx.world.character(this.data.character).isEquipped(this.data.name);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitEquipped(this);
			}
		}

		class IsWearing extends Bool<Data.Conditions.IsWearing['type'], boolean, Data.Conditions.IsWearing> {
			protected override currentValue(ctx: EvaluationContext): boolean {
				return this.data.isLocked === undefined
					? this.equipmentItem(ctx) !== null
					: this.equipmentItem(ctx)?.isLocked ?? false;
			}

			equipmentItem(ctx: EvaluationContext): Items.EquipmentItem | null {
				const character = ctx.world.character(this.data.character);
				return this.data.slot ? character.equipmentInSlot(this.data.slot) : character.equipmentItem(this.data.name);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitIsWearing(this);
			}
		}

		class StyleCategory extends Numeric<Data.Conditions.StyleCategory['type'], Data.Conditions.StyleCategory> {
			protected override currentValue(ctx: EvaluationContext): number {
				return Math.clamp(ctx.player.getStyleSpecRating(Data.Fashion.Style[this.data.name]), 0, 100);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitStyleCategory(this);
			}
		}

		class Item extends Numeric<Data.Conditions.Item['type'], Data.Conditions.Item> {
			protected override currentValue(ctx: EvaluationContext): number {
				const item = ctx.world.character(this.data.character).getItemById(this.data.name);
				if (!item) {
					return 0;
				}
				if (item instanceof Items.Equipment) {
					return 1;
				}
				return item.charges;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitItem(this);
			}
		}

		class IsInPort extends Individual<Data.Conditions.InPort['type'], Data.Conditions.InPort> {
			override evaluate(ctx: EvaluationContext): void {
				ctx.storeBool(this, ctx.player.isInPort(this.data.value) === this.condition, true, this.data.tag);
			}

			override status(ctx: EvaluationContext): boolean {
				return ctx.getBool(this);
			}

			get condition(): boolean {
				return this.data.condition ?? true;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitInPort(this);
			}
		}

		class PortName extends Bool<Data.Conditions.PortName['type'], boolean, Data.Conditions.PortName> {
			protected override currentValue(ctx: EvaluationContext): boolean {
				return cmp(ctx.player.getShipLocation().title, this.data.value);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitPortName(this);
			}
		}

		class Money extends Numeric<Data.Conditions.Money['type'], Data.Conditions.Money> {
			// eslint-disable-next-line class-methods-use-this
			protected override currentValue(ctx: EvaluationContext): number {
				return ctx.player.money;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitMoney(this);
			}
		}

		class Tokens extends Numeric<Data.Conditions.Tokens['type'], Data.Conditions.Tokens> {
			// eslint-disable-next-line class-methods-use-this
			protected override currentValue(ctx: EvaluationContext): number {
				return ctx.player.tokens;
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitTokens(this);
			}
		}

		class NpcStat extends Numeric<Data.Conditions.NPCStat['type'], Data.Conditions.NPCStat> {
			protected override currentValue(ctx: EvaluationContext): number {
				return ctx.npc(this.data.option)[0][this.data.name];
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitNPCStat(this);
			}
		}

		abstract class NamedProperty<
			TType extends string,
			TMap, TProperty extends keyof TMap
		> extends Individual<TType, Data.Conditions.NamedProperty<TType, TMap, TProperty>> {}

		abstract class TaskProperty<
			TType extends string,
			TMap extends Data.Conditions.TaskProperties,
			TProperty extends keyof TMap = keyof TMap
		> extends NamedProperty<TType, TMap, TProperty> {
			override evaluate(ctx: EvaluationContext): void {
				switch (this.data.property) {
					case 'giver':
						ctx.storeBool(this,
							cmp(this.task(ctx).giver.id, this.data.value as string,
								boolConditionOrDefaultCheck(this.data.condition)), true, this.data.tag);
						break;
					default:
						throw new Error(`Task condition evaluation failed: unrecognized property '${String(this.data.property)}'`);
				}
			}

			override status(ctx: EvaluationContext): boolean {
				switch (this.data.property) {
					case 'giver':
						return ctx.getBool(this);
					default:
						throw new Error(`Task condition evaluation failed: unrecognized property '${String(this.data.property)}'`);
				}
			}

			protected abstract task(ctx: EvaluationContext): App.Task;
		}

		class QuestProperty extends TaskProperty<Data.Conditions.Quest['type'], Data.Conditions.QuestProperties> {
			override evaluate(ctx: EvaluationContext): void {
				switch (this.data.property) {
					case 'status':
						ctx.storeBool(this,
							cmp(this.task(ctx).status(ctx.player), this.data.value, boolConditionOrDefaultCheck(this.data.condition)),
							true, this.data.tag);
						break;
					case 'acceptedOn':
						ctx.storeNumeric(this, this.task(ctx).acceptedOn(ctx.player, Number.NaN), this.data.value as number, this.data.tag);
						break;
					case 'completedOn':
						ctx.storeNumeric(this, this.task(ctx).completedOn(ctx.player, Number.NaN), this.data.value as number, this.data.tag);
						break;
					case 'acceptedFor':
						ctx.storeNumeric(this, ctx.world.day - this.task(ctx).acceptedOn(ctx.player, Number.NaN),
							this.data.value as number, this.data.tag);
						break;
					case 'completedFor':
						ctx.storeNumeric(this, ctx.world.day - this.task(ctx).completedOn(ctx.player, Number.NaN),
							this.data.value as number, this.data.tag);
						break;
					default:
						super.evaluate(ctx);
						break;
				}
			}

			override status(ctx: EvaluationContext): boolean {
				switch (this.data.property) {
					case 'status':
						return ctx.getBool(this);
					case 'acceptedOn':
					case 'completedOn':
					case 'acceptedFor':
					case 'completedFor': {
						const res = ctx.getNumeric(this);
						return cmp(res.value, res.target, numericConditionOrDefault(this.data.condition));
					}
					default:
						return super.status(ctx);
				}
			}

			protected override task(ctx: EvaluationContext): App.Quest {
				return ctx.quest(this.data.name);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitQuestProperty(this);
			}
		}

		class JobProperty extends TaskProperty<Data.Conditions.Job['type'], Data.Conditions.JobProperties> {
			override evaluate(ctx: EvaluationContext): void {
				switch (this.data.property) {
					case 'available':
						ctx.storeBool(this,
							cmp(this.task(ctx).available(ctx.player), this.data.value, boolConditionOrDefaultCheck(this.data.condition)),
							true, this.data.tag);
						break;
					case 'lastCompletedFor':
						ctx.storeNumeric(this, this.task(ctx).lastCompletedOn(ctx.player, Number.NaN) - ctx.world.day,
							this.data.value as number, this.data.tag);
						break;
					case "timesCompleted":
						ctx.storeNumeric(this, this.task(ctx).timesCompleted(ctx.player, 0),
							this.data.value as number, this.data.tag);
						break;
					default:
						super.evaluate(ctx);
						break;
				}
			}

			override status(ctx: EvaluationContext): boolean {
				switch (this.data.property) {
					case 'available':
						return ctx.getBool(this);
					case 'lastCompletedFor': {
						const res = ctx.getNumeric(this);
						return cmp(res.value, res.target, numericConditionOrDefault(this.data.condition));
					}
					default:
						return super.status(ctx);
				}
			}

			protected override task(ctx: EvaluationContext): App.Job {
				return ctx.job(this.data.name);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitJobProperty(this);
			}
		}

		class DayPhase extends Individual<Data.Conditions.DayPhase['type'], Data.Conditions.DayPhase> {
			override evaluate(ctx: EvaluationContext): void {
				ctx.storeBool(this,
					ctx.world.phase >= (this.data.min ?? App.DayPhase.Morning) && ctx.world.phase <= (this.data.max ?? App.DayPhase.LateNight),
					true, this.data.tag);
			}

			override status(ctx: EvaluationContext): boolean {
				return ctx.getBool(this);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitDayPhase(this);
			}
		}

		class TrackProgress extends Numeric<Data.Conditions.TrackProgress['type'], Data.Conditions.TrackProgress> {
			override currentValue(ctx: EvaluationContext): number {
				switch (this.data.type) {
					case 'trackCustomers':
						return ctx.player.getHistory("customers", this.data.name) - ctx.gameVariableValue<number>(trackCustomersVariableName(this.data.name), ctx.definedVariableScope(this.data.scope));
					case 'trackProgress':
						return ctx.gameVariableValue(trackProgressVariableName(this.data.name), ctx.definedVariableScope(this.data.scope));
				}
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitTrackProgress(this);
			}
		}

		class Random extends Numeric<Data.Conditions.Random['type'], Data.Conditions.Random> {
			// eslint-disable-next-line class-methods-use-this
			override currentValue(_ctx: EvaluationContext): number {
				return State.random() * (this.data.factor ?? 1);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitRandom(this);
			}
		}

		class Tag extends Individual<Data.Conditions.Tag['type'], Data.Conditions.Tag> {
			// eslint-disable-next-line @typescript-eslint/no-empty-function, class-methods-use-this
			override evaluate(_ctx: EvaluationContext): void {}

			override status(ctx: EvaluationContext): boolean {
				const result = ctx.tagged(this.data.name, ActivatorResult.EMPTY);
				const tagValue = this.data.raw ? result.checkValue : result.percent / 100;
				return cmp(tagValue, this.data.value ?? 1, this.condition);
			}

			get condition() {
				return replaceAlternateContionTerms(Numeric.substituteDefaultCondition(this.data.condition));
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitTag(this);
			}
		}

		class Statistics extends Individual<Data.Conditions.Statistics['type'], Data.Conditions.Statistics> {
			override evaluate(ctx: EvaluationContext): void {
				ctx.storeNumeric(this, this._currentValue(ctx), this.data.value, this.data.tag);
			}

			override status(ctx: EvaluationContext): boolean {
				const res = ctx.getNumeric(this);
				return cmp(res.value, res.target, this.condition);
			}

			override acceptVisitor(visitor: ActivatorVisitor): void {
				visitor.visitStatistics(this);
			}

			get condition(): Data.Conditions.ConditionMain {
				return replaceAlternateContionTerms(Numeric.substituteDefaultCondition(this.data.condition));
			}

			private _currentValue(ctx: EvaluationContext): number {
				const tags: number[] = [];
				for (const t of this.data.tags) {
					tags.push(ctx.tagged(t).mod);
				}

				tags.sort((a, b) => a - b);

				switch (this.data.op) {
					case 'min': return tags[0];
					case 'max': return tags.last();
					case 'mean': return _.mean(tags);
					case 'median': return tags[tags.length / 2];
					case 'sum': return _.sum(tags);
					case 'product': return tags.reduce((previous, current) => previous * current, 1);
				}
			}
		}

		function makeTest(r: Data.Conditions.Any): Base {
			switch (r.type) {
				case Stat.Body:
					return new BodyStat(r);
				case Stat.Core:
					return new CoreStat(r);
				case Stat.Skill:
					return new SkillStat(r);
				case 'daysPassed':
					return new DaysPassed(r);
				case 'dayPhase':
					return new DayPhase(r);
				case 'eventCount':
					return new EventCount(r);
				case 'equipped':
					return new Equipped(r);
				case 'hairColor':
					return new HairColor(r);
				case 'hairStyle':
					return new HairStyle(r);
				case 'inPort':
					return new IsInPort(r);
				case 'portName':
					return new PortName(r);
				case 'isWearing':
					return new IsWearing(r);
				case 'item':
					return new Item(r);
				case 'meta':
					return new Meta(r);
				case 'money':
					return new Money(r);
				case 'npcStat':
					return new NpcStat(r);
				case 'quest':
					return new QuestProperty(r);
				case 'job':
					return new JobProperty(r)
				case 'styleCategory':
					return new StyleCategory(r);
				case 'tokens':
					return new Tokens(r);
				case 'trackCustomers':
				case 'trackProgress':
					return new TrackProgress(r);
				case 'random':
					return new Random(r);
				case 'tag':
					return new Tag(r);
				case 'statistic':
					return new Statistics(r);
				case 'var':
					return new Variable(r);
				case 'var:b':
					return new VariableBoolean(r);
				case 'var:n':
					return new VariableNumeric(r);
				case 'var:s':
					return new VariableString(r);
			}
			// assertUnreachable(r.type);
		}

		abstract class ActivatorVisitor {
			abstract visitBodyStat(activator: BodyStat): void;
			abstract visitCoreStat(activator: CoreStat): void;
			abstract visitSkill(activator: SkillStat): void;
			abstract visitMeta(activator: Meta): void;
			abstract visitMoney(activator: Money): void;
			abstract visitTokens(activator: Tokens): void;
			abstract visitNPCStat(activator: NpcStat): void;
			abstract visitDaysPassed(activator: DaysPassed): void;
			abstract visitEventCount(activator: EventCount): void;
			abstract visitQuestProperty(activator: QuestProperty): void;
			abstract visitJobProperty(activator: JobProperty): void;
			abstract visitItem(activator: Item): void;
			abstract visitEquipped(activator: Equipped): void;
			abstract visitIsWearing(activator: IsWearing): void;
			abstract visitStyleCategory(activator: StyleCategory): void;
			abstract visitHairColor(activator: HairColor): void;
			abstract visitHairStyle(activator: HairStyle): void;
			abstract visitPortName(activator: PortName): void;
			abstract visitInPort(activator: IsInPort): void;
			abstract visitTrackProgress(activator: TrackProgress): void;
			abstract visitDayPhase(activator: DayPhase): void;
			abstract visitRandom(activator: Random): void;
			abstract visitTag(activator: Tag): void;
			abstract visitStatistics(activator: Statistics): void;
			abstract visitVariable(activator: Variable): void;
			abstract visitVariableBoolean(activator: VariableBoolean): void;
			abstract visitVariableNumeric(activator: VariableNumeric): void;
			abstract visitVariableString(activator: VariableString): void;

			beginGroup(group: RequirementGroup) {
				this._groups.push(group);
			}

			endGroup() {
				this._groups.pop();
			}

			private readonly _groups: RequirementGroup[] = [];
		}

		function isImplicitAndExpression(data: Data.Conditions.Expression): data is Data.Conditions.ImplicitAndExpression {
			return Array.isArray(data);
		}

		function isExplicitExpression(
			data: Data.Conditions.Expression | Data.Conditions.Any
		): data is Data.Conditions.ExplicitExpression {
			return data.hasOwnProperty('args');
		}

		function parseExpresionArray(group: RequirementGroup, data: Data.Conditions.ImplicitAndExpression) {
			for (const expression of data) {
				if (isExplicitExpression(expression)) {
					group.add(parseExpression(expression));
				} else {
					group.add(makeTest(expression));
				}
			}
		}

		function parseExpression(data: Data.Conditions.Expression): RequirementGroup {
			if (isImplicitAndExpression(data)) {
				const res = new RequirementGroup();
				parseExpresionArray(res, data);
				return res;
			} else {
				const res = new RequirementGroup(data.op, data.title);
				parseExpresionArray(res, data.args);
				return res;
			}
		}

		export class Expression {
			readonly #root: RequirementGroup;
			constructor(data: Data.Conditions.Expression) {
				this.#root = parseExpression(data);
			}

			compute(ctx: EvaluationContext): ComputedExpression {
				this.#root.evaluate(ctx);
				return new ComputedExpression(this.#root, ctx);
			}

			evaluateFast(ctx: EvaluationContext): boolean {
				return this.#root.evaluateFast(ctx);
			}
		}

		export class ComputedExpression {
			readonly #root: RequirementGroup;
			readonly #ctx: EvaluationContext;

			constructor(root: RequirementGroup, ctx: EvaluationContext) {
				this.#root = root;
				this.#ctx = ctx;
			}

			get root(): RequirementGroup {
				return this.#root;
			}

			get ctx(): EvaluationContext {
				return this.#ctx;
			}

			get status(): boolean {
				return this.#root.status(this.#ctx);
			}
		}

		export class RequirementRenderer extends ActivatorVisitor {
			readonly #ctx: EvaluationContext;
			/**
			 * @param ctx Context filled in with data using Base.compute()
			 */
			constructor(ctx: EvaluationContext, parent: ParentNode) {
				super();
				this.#ctx = ctx;
				this._parents = [];
				this._parents.push(parent);
			}

			override beginGroup(group: RequirementGroup): void {
				super.beginGroup(group);
				if (!group.visible(this.#ctx)) {
					return
				}

				const makeHeader = () => {
					switch (group.op) {
						case '||':
							return UI.appendNewElement('div', this._currentParent, group.length > 1 ? "Any of:" : "", ['requirement-group-any']);
						case '&&':
							return UI.appendNewElement('div', this._currentParent, group.length > 1 ? "All of:" : "", ['requirement-group-all']);
					}
				}

				const header = makeHeader();
				const list = UI.appendNewElement('div', this._currentParent);
				this._parents.push(list);
				header.classList.add(group.status(this.#ctx) ? 'requirement-group-done' : 'requirement-group-undone')
			}

			override endGroup(): void {
				super.endGroup();
				this._parents.pop();
			}

			override visitCoreStat(activator: CoreStat): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderUncountableNumeric(a, p, a.data.name);
					});
			}

			override visitBodyStat(activator: BodyStat): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						const statConfig = PR.statConfig(Stat.Body)[a.data.name];
						const inverted = isInvertedCondition(activator.condition);
						this._renderNumeric(a, p,
							a => `${Leveling.levelingProperty(a.data.type, a.data.name, "adjective", a.data.value, true)} ${Leveling.noun(a.data.type, a.data.name, a.data.value, true)}`, undefined, false, false, inverted ? statConfig.max : statConfig.min);
					});
			}

			override visitSkill(activator: SkillStat): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						const skillName = Data.Lists.skillConfig[a.data.name].altName ?? a.data.name;
						this._renderUncountableNumeric(a, p, `the ${skillName} skill`);
					});
			}

			override visitMeta(activator: Meta): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						switch (a.data.name) {
							case "style":
							case "beauty":
								this._renderNumeric(a, p,
									a => `Have your ${a.data.name} ${RequirementRenderer._spellConditionAdjective(a.condition)} ${PR.colorizeString(a.data.value, Leveling.levelingRecord(Data.ratings[a.data.name], a.data.value) as string)}`);
								break;
							default:
								this._renderUncountableNumeric(a, p, _.lowerCase(a.data.name));
								break;
						}
					});
			}

			override visitDaysPassed(activator: DaysPassed): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							const waitConditions: Data.Conditions.Condition[] = ['eq', 'gt', 'gte', '==', '>', ">="];
							const value = this.#ctx.getNumeric(a);
							const days = value.target - value.value;
							if (waitConditions.includes(a.condition)) {
								return `wait ${days} days`;
							}
							return a.status(this.#ctx) ? `${days} days left` : `expired ${0 - days} days ago`;
						});
					});
			}

			override visitEventCount(activator: EventCount): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderCountableNumeric(a, p,
							`event '${a.data.name}'`, `events '${a.data.name}'`);
					});
			}

			override visitEquipped(activator: Equipped): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							const sid = Items.splitId(a.data.name)
							const item = Items.factory(sid.category, sid.tag);
							return (a.data.value ?? true) ? `Must have ${item.description} equipped` : `May not have ${item.description} equipped`;
						});
					});
			}

			override visitIsWearing(activator: IsWearing): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							let desc = "";
							if (!a.data.value) {
								desc = "''NOT'' ";
							}
							if (a.data.slot) {
								desc += `wearing anything in slot '${a.data.slot}'`;
							} else {
								const itemName = Items.splitId(a.data.name);
								desc += "wearing " + Items.factory(itemName.category, itemName.tag).description;
							}

							if (a.data.isLocked !== undefined) {
								desc += ` and ${a.data.isLocked ? "locked" : "unlocked"}`;
							}
							return desc;
						});
					});
			}

			override visitHairColor(activator: HairColor): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							let desc = "";
							switch (a.data.hairType) {
								case 'natural':
									desc += "Natural hair";
									break;
								case 'wig':
									desc += "Wig";
									break;
								default:
									desc += "Hair";
									break;
							}
							desc += " color: ";
							if (a.condition === '!=') {
								desc += " not "
							}
							desc += a.data.name;
							return desc;
						});
					});
			}

			override visitHairStyle(activator: HairStyle): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							let desc = "";
							switch (a.data.hairType) {
								case 'natural':
									desc += "Natural hair";
									break;
								case 'wig':
									desc += "Wig";
									break;
								default:
									desc += "Hair";
									break;
							}
							desc += " style: ";
							if (a.condition === '!=') {
								desc += "not "
							}
							desc += a.data.name;
							return desc;
						});
					});
			}

			override visitStyleCategory(activator: StyleCategory): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderUncountableNumeric(a, p, `${_.lowerCase(a.data.name)} style`);
					});
			}

			override visitMoney(activator: Money): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderUncountableNumeric(a, p, "coin", 'item-money');
					});
			}

			override visitTokens(activator: Tokens): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderCountableNumeric(a, p,
							"courtesan token", "courtesan tokens", 'item-courtesan-token');
					});
			}

			override visitItem(activator: Item): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderCountableNumeric(a, p, Items.factoryById(activator.data.name).description);
					});
			}

			override visitJobProperty(activator: JobProperty): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						switch (a.data.property) {
							case 'available':
								this._renderBool(a, p, (a) => {
									return `Job '${this.#ctx.job(a.data.name).title()}' is ${GameVariables.toString(a.data.value)}`;
								});
								break;
							case "lastCompletedFor":
								this._renderBool(a, p, (a) => {
									return `Job '${this.#ctx.job(a.data.name).title()}' is last completed for ${GameVariables.toString(a.data.value)}`;
								});
								break;
							case "timesCompleted":
								this._renderBool(a, p, (a) => {
									return `Job '${this.#ctx.job(a.data.name).title()}' is completed for ${GameVariables.toString(a.data.value)}`;
								});
								break;
						}
					});
			}

			private static _questStatusToString(qs: QuestStatus): string {
				switch (qs) {
					case QuestStatus.Unavailable: return 'unavailable';
					case QuestStatus.Available: return 'available';
					case QuestStatus.Active: return 'active';
					case QuestStatus.CanComplete: return 'can be completed';
					case QuestStatus.Completed: return 'completed';
				}
			}

			override visitQuestProperty(activator: QuestProperty): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						const qt = this.#ctx.quest(a.data.name).title();
						switch (a.data.property) {
							case 'status':
								this._renderBool(a, p, (a) => {
									return `Quest '${qt}' is ${GameVariables.toString(a.data.value as QuestStatus | QuestStatus[],
										RequirementRenderer._questStatusToString)}`;
								});
								break;
							case 'acceptedFor':
								this._renderBool(a, p, () => {
									let res = `Accept quest '${qt}'`;
									const waitDays = a.data.value as number;
									if (waitDays > 0) {
										res += ` and wait for ${RequirementRenderer._descForCountableCondition(waitDays, '>=', 'day')}`;
										if (this.#ctx.quest(a.data.name).isActive(this.#ctx.player)) {
											const diff = waitDays -
												(this.#ctx.world.day - this.#ctx.quest(a.data.name).acceptedOn(this.#ctx.player));
											if (diff > 0) {
												res += ` (${diff} more days)`;
											}
										}
									}
									return res;
								});
								break;
							case 'completedFor':
								break;
						}
					});
			}

			override visitDayPhase(activator: DayPhase): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							return `Day phase between ${PR.phaseIcon(a.data.min ?? App.DayPhase.Morning)} and ${PR.phaseIcon(a.data.max ?? App.DayPhase.LateNight)}`
						})
					});
			}

			override visitPortName(activator: PortName): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							return (a.data.condition ?? true) ? `Be in port ${a.data.name}` : `Be outside of port ${a.data.name}`;
						})
					});
			}

			override visitTrackProgress(activator: TrackProgress): void {
				// track customers and progress are rather different cases.
				// customers are countable, progress is not, customers need location
				switch (activator.data.type) {
					case 'trackCustomers':
						this._render(activator, this._currentParent,
							(a, p) => {
								this._renderNumeric(a, p, (a) => {
									const whoringDesc = Data.whoring[a.data.name];
									return `Satisfy ${this._descForCountableNumericCondition(a, "customer")} at ${whoringDesc.desc}`;
								});
							});
						break;
					case 'trackProgress':
						this._render(activator, this._currentParent,
							(a, p) => {
								this._renderNumeric(a, p, (a) => {
									return a.data.name; // FIXME
								}, undefined, true);
							});
						break;
				}
			}

			override visitInPort(activator: IsInPort): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							let res = a.condition ? "Be in port" : "Be at sea";
							if (a.data.value > 0) {
								res += ` in ${a.data.value} days`;
							}
							return res;
						})
					});
			}

			override visitRandom(activator: Random): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderUncountableNumeric(a, p, "Are you lucky today?");
					});
			}

			override visitNPCStat(activator: NpcStat): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderNumeric(a, p, a => `@@.npc;${this.#ctx.npc(a.data.option)[0].name}@@ ${RequirementRenderer._spellConditionAdjective(a.condition)} ${Entity.NPC.statString(a.data.name, a.targetValue(this.#ctx))}`)
						// this._renderUncountableNumeric(a, p, `@@.npc;${this.#ctx.npc(a.data.option)[0].name}@@'s ${a.data.name}`);
					});
			}

			override visitStatistics(activator: Statistics): void {
				this._render(activator, this._currentParent,
					(a, p) => {
						this._renderBool(a, p, (a) => {
							return a.data.altTitle ?? "ERROR_NO_ALTTITLE_FOR_STATISTICAL_CONDITION";
						})
					});
			}

			private _boolOrStringVariable(activator: VariableBoolean | VariableString) {
				if (this.#ctx.world.debugMode) {
					this._render(activator, this._currentParent,
						(a, p) => {
							this._renderBool(a, p, (a) => {
								return a.data.altTitle ?? `Variable ${a.data.name} (${this._scopeDesc(a.data.scope)}) is
									${boolConditionOrDefault(a.data.condition)} to ${a.data.value}`;
							})
						});
				}
			}

			override visitVariableBoolean(activator: VariableBoolean): void {
				this._boolOrStringVariable(activator);
			}

			override visitVariableString(activator: VariableString): void {
				this._boolOrStringVariable(activator);
			}

			override visitVariableNumeric(activator: VariableNumeric): void {
				if (this.#ctx.world.debugMode) {
					this._render(activator, this._currentParent,
						(a, p) => {
							this._renderNumeric(a, p, (a) => {
								return a.data.altTitle ?? `Variable ${a.data.name} (${this._scopeDesc(a.data.scope)}) is ${a.data.condition ?? '>='} to ${a.data.value}`;
							})
						});
				}
			}

			override visitVariable(activator: Variable): void {
				if (this.#ctx.world.debugMode) {
					this._render(activator, this._currentParent,
						(a, p) => {
							this._renderBool(a, p, (a) => {
								return a.data.altTitle ?? `Variable ${a.data.name} (${this._scopeDesc(a.data.scope)}) is ${a.data.condition ?? '=='} to ${a.data.value}`;
							})
						});
				}
			}

			// eslint-disable-next-line class-methods-use-this
			private _scopeDesc(scope?: VariableScope.Any): string {
				if (!scope) {
					return 'automatic scope';
				}

				switch (scope.type) {
					case 'current':
						return 'current scope';
					case 'job':
						return `job ${scope.id}`;
					case 'quest':
						return `quest ${scope.id}`;
					case 'player':
						return "player scope";
				}
			}

			// eslint-disable-next-line @typescript-eslint/no-empty-function, class-methods-use-this
			override visitTag(_activator: Tag): void {}

			private _render<T extends Base>(activator: T, parent: ParentNode, renderer: ((a: T, parent: HTMLDivElement) => void)) {
				if (activator.visible(this.#ctx)) {
					const res = UI.makeElement('div');
					renderer(activator, res);
					parent.append(res);
				}
			}

			private _renderBool<
				TType extends string,
				TData extends Data.Conditions.Base<TType>,
				TActivator extends Individual<TType, TData>
			>(activator: TActivator, parent: HTMLDivElement, desc: ((a: TActivator) => string), descStyle?: string | string[]) {
				this._renderCheckMark(activator, parent);
				if (descStyle) {
					RequirementRenderer._renderStyledDesc(activator.data.altTitle ?? desc(activator), descStyle, parent);
				} else {
					RequirementRenderer._renderDesc(activator.data.altTitle ?? desc(activator), parent);
				}
			}

			private _renderNumeric<
				TType extends string,
				TData extends Data.Conditions.SimpleNumeric<TType>,
				TActivator extends Numeric<TType, TData>
			>(activator: TActivator, parent: HTMLDivElement, desc: ((a: TActivator) => string),
				descStyle?: string | string[], makePercents = false, inverseColors = false, min = 0) {
				this._renderCheckMark(activator, parent);
				const value = this.#ctx.getNumeric(activator);
				const meterValue = makePercents
					? {val: value.percent, max: 100}
					: {val: value.value, max: value.target};
				const range: {min: number, max: number} = {min: min, max: meterValue.max};
				const meter = UI.rMeter(meterValue.val, range,
					{
						inverseColors: inverseColors,
						showScore: settings.displayMeterNumber,
						showMax: settings.displayMeterNumber,
					});
				meter.style.marginRight = '0.5em';
				meter.classList.add('fixed-font');
				parent.append(meter);
				if (descStyle) {
					RequirementRenderer._renderStyledDesc(activator.data.altTitle ?? desc(activator), descStyle, parent);
				} else {
					RequirementRenderer._renderDesc(activator.data.altTitle ?? desc(activator), parent);
				}
			}

			private _renderUncountableNumeric<
				TType extends string,
				TData extends Data.Conditions.SimpleNumeric<TType>,
				TActivator extends Numeric<TType, TData>
			>(activator: TActivator, parent: HTMLDivElement, noun: string, descStyle?: string | string[]) {
				return this._renderNumeric(activator, parent,
					a => `${RequirementRenderer._spellConditionUncountable(a.targetValue(this.#ctx), a.condition)} ${noun}`, descStyle);
			}

			private _renderCountableNumeric<
				TType extends string,
				TData extends Data.Conditions.SimpleNumeric<TType>,
				TActivator extends Numeric<TType, TData>
			>(activator: TActivator, parent: HTMLDivElement, singularNoun: string, pluralNoun?: string, descStyle?: string | string[]) {
				return this._renderNumeric(activator, parent,
					a => this._descForCountableNumericCondition(a, singularNoun, pluralNoun), descStyle);
			}

			private _renderCheckMark(activator: Base, parent: ParentNode): void {
				const span = UI.appendNewElement('span', parent);
				if (activator.status(this.#ctx)) {
					span.innerHTML = '&#x2713; ';
					span.classList.add('state-positive');
				} else {
					span.innerHTML = '&#x2717; ';
					span.classList.add('state-negative');
				}
			}

			private static _renderDesc(desc: string, parent: HTMLDivElement) {
				$(parent).wiki(desc);
			}

			private static _renderStyledDesc(desc: string, style: string | string[], parent: HTMLDivElement) {
				parent.appendFormattedText({text: desc, style});
			}

			private static _descForCountableCondition(
				value: number, condition: Data.Conditions.ConditionMain, singularNoun: string, pluralNoun?: string
			): string {
				const cnd = RequirementRenderer._spellConditionCountable(value, condition);
				if (value === 1) {
					return `${cnd} ${singularNoun}`;
				}
				return `${cnd} ${pluralNoun ?? singularNoun + 's'}`;
			}

			private _descForCountableNumericCondition<
				TType extends string,
				TData extends Data.Conditions.SimpleNumeric<TType>,
				TActivator extends Numeric<TType, TData>
			>(ac: TActivator, singularNoun: string, pluralNoun?: string): string {
				return RequirementRenderer._descForCountableCondition(ac.targetValue(this.#ctx), ac.condition, singularNoun, pluralNoun);
			}

			private static _spellConditionCountable(value: number, c: Data.Conditions.ConditionMain): string {
				// special cases
				if (value === 0) {
					switch (c) {
						case '<=':
						case '==':
							return 'no'; // as "no showels"
						case '>':
						case '!=':
							return 'some';
					}
				} else if (value === 1) {
					switch (c) {
						case '<':
							return 'no'; // as "no showels"
						case '==':
							return 'a single';
						case '!=':
							return 'any number but not a single';
					}
				}
				switch (c) {
					case ">": return `more than ${PR.numberToWords(value)}`;
					case '>=': return `at least ${PR.numberToWords(value)}`;
					case '<': return `less than ${PR.numberToWords(value)}`;
					case '<=': return `at most ${PR.numberToWords(value)}`;
					case '==': return `exactly ${PR.numberToWords(value)}`;
					case '!=': return `any number but not ${PR.numberToWords(value)}`;
				}
				throw new Error(`Unexpected condition value ${c}`);
			}

			private static _spellConditionUncountable(value: number, c: Data.Conditions.ConditionMain): string {
				// special cases
				if (value === 0) {
					switch (c) {
						case '<=':
						case '==':
							return 'no'; // as "no showels"
						case '>':
						case '!=':
							return 'some';
					}
				}
				switch (c) {
					case ">": return `more than ${PR.numberToWords(value)} of`;
					case '>=': return `at least ${PR.numberToWords(value)} of`;
					case '<': return `less than ${PR.numberToWords(value)} of`;
					case '<=': return `at most ${PR.numberToWords(value)} of`;
					case '==': return `exactly ${PR.numberToWords(value)} of`;
					case '!=': return `any quantity but not ${PR.numberToWords(value)} of`;
				}
				throw new Error(`Unexpected condition value ${c}`);
			}

			private static _spellConditionAdjective(c: Data.Conditions.ConditionMain): string {
				switch (c) {
					case ">": return `higher than`;
					case '>=': return `at least`;
					case '<': return `lower than`;
					case '<=': return `at most`;
				}
				throw new Error(`Unexpected condition value ${c}`);
			}

			private get _currentParent(): ParentNode {
				return this._parents.last();
			}

			private readonly _parents: ParentNode[];
		}

		/**
		 * Helper function - runs comparisons.
		 */
		export function cmp(a: number, b: number, c?: Data.Conditions.ConditionMain): boolean;
		export function cmp(a: string, b: string, c?: "==" | "!=" | boolean): boolean;
		export function cmp(a: GameVariable, b: GameVariable, c?: Data.Conditions.ConditionMain): boolean;
		export function cmp<T>(a: T, b: T | ReadonlyArray<T>, c?: Data.Conditions.EqualityConditionMain): boolean;
		export function cmp<T>(a: T, b: T, c: Data.Conditions.ConditionMain = true) {
			if (c === null) { // special case, no comparison is performed
				return true;
			}

			if ((a == null) || (b == null)) {
				switch (c) {
					case "==":
					case true:
						return a === b;
					case "!=":
					case false:
						return a !== b;
					default:
						throw "Only equality tests are allowed with undefined values";
				}
			}

			if (Array.isArray(b)) {
				switch (c) {
					case '==':
					case true:
						// eslint-disable-next-line eqeqeq
						return b.some(belem => a == belem);
					case '!=':
					case false:
						// eslint-disable-next-line eqeqeq
						return b.every(belem => a != belem);
					default:
						throw new Error(`Condition '${c}' is not applicable to arrays`);
				}
			}

			switch (c) {
				case ">=":
					return a >= b;
				case "<=":
					return a <= b;
				case ">":
					return a > b;
				case "<":
					return a < b;
				case "==":
					// eslint-disable-next-line eqeqeq
					return a == b;
				case "!=":
					// eslint-disable-next-line eqeqeq
					return a != b;
				default:
					// eslint-disable-next-line eqeqeq
					return (a == b) === c;
			}
		}

		export function cmpAny(test: GameVariable, value: GameVariable | GameVariable[], c?: Data.Conditions.ConditionMain): boolean {
			if (Array.isArray(value)) {
				return value.some(v => cmp(test, v, c));
			}
			return cmp(test, value, c);
		}

		export function isInvertedCondition(c: Data.Conditions.Condition = '=='): boolean {
			return invertedConditions.has(c);
		}

		const invertedConditions: Set<Data.Conditions.Condition> = new Set(["lte", "lt", "<=", "<"]);
	}
}
