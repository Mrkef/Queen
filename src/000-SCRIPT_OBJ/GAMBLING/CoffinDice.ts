// Gambling classes.
// Real rough, probably clean up some a bit in the future, but maybe not.
// 'Pirates Dice' (poker like game with dice) - TBD
// 'Shut the Coffin' simple point like dice game
// 'Blackjack' We all know and love. Right? - TBD
// 'Grand Hazard' Sorta like craps. Old West style game. - TBD

namespace App.Gambling {

	type Offer = "Item" | "Coins";
	type Want = "Coins" | number;
	type WantChoice = "Hand Job" | "Blow Job" | "Tit Fuck" | "Ass Fuck";
	const enum BetStatus {
		Nothing = 0,
		Loss = 1,
		Win = 2
	}
	interface Gambler {
		/** name */
		n: string;

		bet1Offer: Offer;
		bet1Want: Want;
		bet1Value: number;
		bet1Status: BetStatus;

		bet2Offer: Offer;
		bet2Want: Want;
		bet2Value: number;
		bet2Status: BetStatus;

		lust: number;
	}

	interface Sex {
		name: string;
		val: number;
		sex: Skills.Sexual
	}

	interface WonItem {
		data: Items.ItemListingRecord;
		name: string;
	}

	export class Coffin {
		private _myScore: number[] = [0, 0, 0, 0, 0, 0, 0, 0];
		private _opponentScore = [0, 0, 0, 0, 0, 0, 0, 0];
		private _returnPassage = "Deck"; // default
		private _casinoFlag = false;
		private readonly _wants: WantChoice[] = ["Hand Job", "Blow Job", "Tit Fuck", "Ass Fuck"];
		private readonly _nouns = ["a slutty handjob", "a sloppy cock sucking", "a titty wank", "your sissy asshole"];
		private readonly _skills: Skills.Sexual[] = [
			Skills.Sexual.HandJobs, Skills.Sexual.BlowJobs,
			Skills.Sexual.TitFucking, Skills.Sexual.AssFucking,
		];

		private readonly _icons = ["handBetIcon", "bjBetIcon", "titBetIcon", "assBetIcon"];

		private _gamblers: Gambler[] = [];
		private _gamblerPosition = 0;
		private _selectedBet: number | null = null;
		private readonly _element = "#GamblingGUI";
		private _roundNum = 1;
		private readonly _maxRounds = 6;
		private _gamesPlayed = 1;
		private readonly _maxGames = 5;
		private readonly _diceRolling = false; // FIXME readonly? is it unused or what?
		private _playerTurn = true;
		private _suddenDeath = false;
		// TODO convert to enum
		private _screen = "NEW"; // NEW -> BET -> GAME -> OVER (BACK TO BET OR END PASSAGE)
		private _chatLog: string[] | null = null;
		private _interval = 0;

		get returnPassage(): string {return this._returnPassage;}
		set returnPassage(v: string) {this._returnPassage = v;}

		setupGame(returnPassage: string, casino = false): void {
			console.log("Setting up game of Coffin…");
			this.returnPassage = returnPassage;
			this._casinoFlag = casino;
			$(document).one(":passageend", this._setupGame.bind(this));
		}

		printResults(): string {
			let buffer = "";
			// Tally money
			const cash = this._totalMoney();
			const items = this._totalItems();
			const sex = this._totalSex();

			if (cash === 0 && items.length === 0 && sex.length === 0) {
				return "You walk away from the game, none the richer, but none the worse for wear. Maybe you'll play next time?";
			}

			buffer += "The game is over and it's time to settle accounts…\n\n";

			buffer += Coffin._printSex(sex);
			buffer += Coffin._printItems(items);

			if (cash > 0) buffer += `You @@.state-positive;collect@@ @@.item-money;${cash} coins@@ in winnings.`;
			if (cash < 0) buffer += `You @@.state-negative;pay@@ @@.item-money;${Math.abs(cash)} coins@@ to cover your losses.`;

			Coffin._awardSex(sex);
			Coffin._awardItems(items);

			return buffer;
		}

		private static _printItems(items: WonItem[]) {
			let buffer = "";
			for (const item of items) {
				buffer += "@@color:cyan;" + item.name + "@@ gives you " + item.data.desc + ".\n\n";
			}

			return buffer;
		}

		// Fetch a random sex scene, then format it.
		private static _printSex(sex: Sex[]) {
			let buffer = "";
			let lastName = "", lastSex: Skills.Sexual | null = null;
			const picBase = (s: Sex) => {
				switch (s.sex) {
					case Skills.Sexual.AssFucking: return "ss_anal_";
					case Skills.Sexual.HandJobs: return "ss_hand_";
					case Skills.Sexual.BlowJobs: return "ss_bj_";
					case Skills.Sexual.TitFucking: return "ss_tit_";
					default: return "ss_anal_";
				}
			};

			for (const s of sex) {
				// Don't repeat sex scenes on pirates if they are the same.
				if (lastName === s.name && lastSex === s.sex) continue;
				lastName = s.name;
				lastSex = s.sex;
				// Fetch an appropriate random passage by skill.
				let passage = scenes[s.sex].randomElement();
				// Make a fake npc to pass to tokenizer.
				const npc = setup.world.npc(Entity.NpcId.Dummy); // Fake NPC
				npc.name = s.name;
				passage = PR.tokenizeString(setup.world.pc, npc, passage); // Tokenize passage
				// Fetch picture
				const pictureNumber = Math.max(1, Math.min(Math.round(State.random() * 10), 10));
				const pic = `${picBase(s)}${pictureNumber}`;

				buffer += "\
            <div style='width:800px'><div style='float:right;width:180px;height:122px' class='"+ pic + "'></div>" + passage + "\n\n</div>";
			}

			return buffer;
		}

		// Sex that needs to be paid
		private _totalSex() {
			const sex: Sex[] = [];

			for (let i = 0; i < this._gamblers.length; i++) {
				const o = this._gamblers[i];
				if (o.bet1Want !== "Coins" && o.bet1Status === BetStatus.Loss) {
					sex.push({name: o.n, val: o.bet1Value, sex: this._skills[o.bet1Want]});
				}

				if (o.bet2Want !== "Coins" && o.bet2Status === BetStatus.Loss) {
					sex.push({name: o.n, val: o.bet2Value, sex: this._skills[o.bet2Want]});
				}
			}

			if (sex.length > 0) {
				const cashValue = sex.length > 1 ? sex.reduce((accumulator, obj) => accumulator + obj.val, 0) : sex[0].val;

				Coffin._trackStat('sexPaid', cashValue);
			}
			return sex;
		}

		private static _awardSex(sex: Sex[]) {
			const crew = setup.world.npc(Entity.NpcId.Crew);
			for (const s of sex) {
				// Call skill check to award xp, then fetch the modifier back to use
				// as an indicator of how much to adjust the NPC's feelings.
				// FIXME this should take game place into account
				const result = setup.world.pc.skillRoll(s.sex, s.val);
				crew.adjustCoreStat(NpcStat.Mood, Math.ceil((5 * result)));
				crew.adjustCoreStat(NpcStat.Lust, Math.ceil((-5 * result)));
			}
		}

		private static _awardItems(items: WonItem[]) {
			for (let i = 0; i < items.length; i++)
				setup.world.pc.addItem(items[i].data.cat, items[i].data.tag, 0);
		}

		private _adjustMoney(status: BetStatus) {
			const o = this._gamblers[this._gamblerPosition];
			const offer = this._selectedBet === 1 ? o.bet1Offer : o.bet2Offer;
			const want = this._selectedBet === 1 ? o.bet1Want : o.bet2Want;
			const value = this._selectedBet === 1 ? o.bet1Value : o.bet2Value;

			if (status === BetStatus.Loss && want === "Coins") {   // Player lost
				setup.world.pc.spendMoney(value, GameState.CommercialActivity.Gambling);
				App.PR.refreshTwineMoney();
			} else if (status === BetStatus.Win && offer === "Coins") { // Player won
				setup.world.pc.earnMoney(value, GameState.CommercialActivity.Gambling);
				App.PR.refreshTwineMoney();
			}
		}

		// Items won.
		private _totalItems() {
			const items: WonItem[] = [];
			const scale = (n: number) => {
				const out = n + Math.ceil(n * State.random()) + Math.ceil((n / 2) * State.random());
				console.log(`Item adjusted from value (${n}) to (${out})`);
				return out;
			};

			for (let i = 0; i < this._gamblers.length; i++) {
				const o = this._gamblers[i];
				let d: Items.ItemListingRecord | null = null;

				if (o.bet1Offer === "Item" && o.bet1Status === BetStatus.Win)
					d = Items.pickItem([Items.Category.Food, Items.Category.Drugs, Items.Category.Cosmetics], {price: scale(o.bet1Value)});

				if (o.bet2Offer === "Item" && o.bet2Status === BetStatus.Win)
					d = Items.pickItem([Items.Category.Food, Items.Category.Drugs, Items.Category.Cosmetics], {price: scale(o.bet2Value)});

				if (d != null) {
					items.push({data: d, name: o.n});
				}
			}

			if (items.length > 0) {
				Coffin._trackStat('itemsWon', items.length);
				const cashValue = items.length > 1 ? items.reduce((accumulator, obj) => { // Reduce objects if needed.
					return accumulator + obj.data.price;
				}, 0) : items[0].data.price;

				Coffin._trackStat('itemsWonValue', cashValue);
			}
			return items;
		}

		// Money won or lost.
		private _totalMoney() {
			let cash = 0;
			for (let i = 0; i < this._gamblers.length; i++) {
				const o = this._gamblers[i];
				if (o.bet1Offer === "Coins" && o.bet1Status === BetStatus.Win) cash += o.bet1Value;
				if (o.bet1Want === "Coins" && o.bet1Status === BetStatus.Loss) cash -= o.bet1Value;
				if (o.bet2Offer === "Coins" && o.bet2Status === BetStatus.Win) cash += o.bet2Value;
				if (o.bet2Want === "Coins" && o.bet2Status === BetStatus.Loss) cash -= o.bet2Value;
			}

			if (cash > 0) Coffin._trackStat('coinsWon', cash);
			if (cash < 0) Coffin._trackStat('coinsLost', Math.abs(cash));
			return cash;
		}

		// Setup functions
		private _setupGame() {
			switch (this._screen) {
				case "NEW":
					this._createGamblers(this._casinoFlag);
					this._setupPoints();
					this._buttonSetup();
					this._displayGambler(0);
					break;
				case "BET":
					this._setupPoints();
					this._buttonSetup();
					this._cbStartGame();
					this._displayGambler(this._gamblerPosition);
					this._highlightBet();
					break;
				case "GAME":
				case "OVER":
					this._reDrawPoints();
					this._buttonSetup();
					this._printRound();
					$('#CoffinBetContainer').css('display', 'none');
					$('#CoffinDiceContainer').css('display', 'block');
					$('#betButtons').css('display', 'none');

					if (this._playerTurn && this._screen === "GAME") {
						this._dialogBox("YOUR TURN", "gold");
						$('#playButtons').css('display', 'block');
					} else {
						$('#playButtons').css('display', 'none');
						$('#endButtons').css('display', 'block');
						if (this._gamesPlayed > this._maxGames) $('#cmdPlayAgain').css('display', 'none');
					}
					break;
			}

			this._refreshChatLog();
		}

		private _createGamblers(flag = false) {
			this._gamblers = [];
			this._gamblerPosition = 0;
			this._selectedBet = null;
			this._roundNum = 1;
			this._gamesPlayed = 1;
			this._chatLog = null;

			for (let i = 0; i < 6; i++) {
				let n = Data.names.Male.randomElement();
				if (flag) {
					const bet1Offer = "Coins";
					const bet1Want = "Coins";
					const bet1Value = 50 + Math.ceil(50 * State.random());
					const bet1Status = 0;

					const bet2Offer = "Coins";
					const bet2Want = "Coins";
					const bet2Value = 100 + Math.ceil(100 * State.random());
					const bet2Status = 0;

					const lust = 50;
					this._gamblers.push({n, bet1Offer, bet1Want, bet1Value, bet1Status, bet2Offer, bet2Want, bet2Value, bet2Status, lust});
				} else {
					n = "Pirate " + n;
					const bet1Offer = (100 * State.random()) > 90 ? "Item" : "Coins";
					const bet1Want = (100 * State.random() > 40) ? "Coins" : Math.round((this._wants.length - 1) * State.random());
					const bet1Value = this._calculateBetValue(bet1Offer, bet1Want);
					const bet1Status = 0; // 0 nothing, 1 lose, 2 win

					const bet2Offer = (100 * State.random()) > 90 ? "Item" : "Coins";
					const bet2Want = (100 * State.random() > 40) ? "Coins" : Math.round((this._wants.length - 1) * State.random());
					const bet2Value = this._calculateBetValue(bet2Offer, bet2Want);
					const bet2Status = 0;

					// Add lust
					const npcLust = setup.world.npc(Entity.NpcId.Crew).lust;
					// 1-10 + random 0-40
					const lust = (Math.floor(npcLust / 10)) + Math.floor(((40 * npcLust / 100) * State.random()));
					this._gamblers.push({n, bet1Offer, bet1Want, bet1Value, bet1Status, bet2Offer, bet2Want, bet2Value, bet2Status, lust});
				}
			}
		}

		private _calculateBetValue(offer: Offer, want: Want) {
			if (want === "Coins") {
				if ((offer === "Item" || offer === "Coins")) return 25 + Math.ceil(25 * State.random());
				throw new Error("Unhandled bet value");
			} else {
				return Math.max(Math.ceil(25 * State.random()) + Math.ceil(75 * (setup.world.pc.skills[this._skills[want]] / 100)), 5);
			}
		}

		private _displayGambler(pos = 0) {
			const g = this._gamblers[pos];
			console.log(g);
			$("#CoffinNPCName").text(g.n);

			// Setup first bet.
			let icon = g.bet1Offer === "Coins" ? "coinBetIcon" : "itemBetIcon";
			let value = g.bet1Offer === "Coins" ? g.bet1Value : "";
			$('#betIcon1a').removeClass().addClass(icon).text(value);

			icon = g.bet1Want === "Coins" ? "coinBetIcon" : this._icons[g.bet1Want];
			value = g.bet1Want === "Coins" ? g.bet1Value : "";
			$('#betIcon1b').removeClass().addClass(icon).text(value);

			this._decorateBet(g, 1);

			// Setup second bet.
			icon = g.bet2Offer === "Coins" ? "coinBetIcon" : "itemBetIcon";
			value = g.bet2Offer === "Coins" ? g.bet2Value : "";
			$('#betIcon2a').removeClass().addClass(icon).text(value);

			icon = g.bet2Want === "Coins" ? "coinBetIcon" : this._icons[g.bet2Want];
			value = g.bet2Want === "Coins" ? g.bet2Value : "";
			$('#betIcon2b').removeClass().addClass(icon).text(value);
			this._decorateBet(g, 2);
		}

		private _decorateBet(g: Gambler, n: 1|2) {
			const value = n === 1 ? g.bet1Value : g.bet2Value;
			const want = n === 1 ? g.bet1Want : g.bet2Want;
			const status = n === 1 ? g.bet1Status : g.bet2Status;
			const elementID = n === 1 ? "#CoffinBet1Blocker" : "#CoffinBet2Blocker";
			const opts = n === 1 ? {on: 1, off: 2} : {on: 2, off: 1};

			const e = $(elementID).empty().removeClass(); // Clear
			const d = $('<div>');

			if (want === "Coins" && setup.world.pc.money < value) { // Not enough cash.
				e.addClass("disabledGamePanel");
				d.html('<span style="color:red">Not enough money.</span>');
				e.append(d);
			} else if (value === 0) { // Player sex skill too low for pirate lust
				e.addClass("disabledGamePanel");
				d.html("<span style='color:red'>Not skillfull enough.</span>");
				e.append(d);
			} else if (status !== BetStatus.Nothing) {
				e.addClass("disabledGamePanel");
				const s = status === BetStatus.Loss ? "<span style='color:red'>Bet already lost.</span>" : "<span style='color:lime'>Bet already won.</span>";
				d.html(s);
				e.append(s);
			} else {
				e.addClass("activeGamePanel").on("click", opts, this._cbSelectBet.bind(this));
			}
		}

		private _setupPoints() {
			this._myScore = [0, 0, 0, 0, 0, 0, 0, 0];
			this._opponentScore = [0, 0, 0, 0, 0, 0, 0, 0];
			this._suddenDeath = false;

			for (let i = 0; i < this._myScore.length; i++) {
				$(`#pcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointEmpty');
				$(`#npcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointEmpty');
			}
		}

		private _reDrawPoints() {
			for (let i = 0; i < this._myScore.length; i++) {
				if (this._myScore[i] === 1) {
					$(`#pcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointHit');
					$(`#npcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointMissed');
				} else if (this._opponentScore[i] === 1) {
					$(`#pcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointMissed');
					$(`#npcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointHit');
				} else {
					$(`#pcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointEmpty');
					$(`#npcbox${i}`).removeClass().addClass('coffinNumBox CoffinpointEmpty');
				}
			}
		}

		private _buttonSetup() {
			// Initial buttons
			$('#cmdQuit').on("click", this._cbQuitGame.bind(this));
			$('#cmdStart').on("click", this._cbStartGame.bind(this));

			// Betting Phase buttons;
			$('#cmdNextBet').on("click", this._cbNextBet.bind(this));
			$('#cmdPrevBet').on("click", this._cbPrevBet.bind(this));
			$('#cmdTakeBet').on("click", this._cbTakeBet.bind(this));
			$("#cmdQuitBet").on("click", this._cbQuitGame.bind(this));

			// Game playing buttons
			$('#cmdRollDice').on("click", this._cbRollDice.bind(this));
			$('#cmdPlayAgain').on("click", this._cbPlayAgain.bind(this));
			$("#cmdQuitPlay").on("click", this._cbQuitGame.bind(this));
		}

		private _highlightBet(data?: {off: number}) {
			if (this._selectedBet != null) {
				$(`#CoffinBet${this._selectedBet}Blocker`).addClass("selectedBet");
				if (data !== undefined)
					$(`#CoffinBet${data.off}Blocker`).removeClass("selectedBet");
			}
		}

		private _printRound() {
			let str = this._playerTurn ? "<span style='color:green'>YOUR TURN</span>" : "<span style='color:red'>OPPONENTS TURN</span>";
			str = this._suddenDeath ? str + " <span style='color:orange'>SUDDEN DEATH</span>" : str;
			$('#RoundContainer').css('display', 'block').html(`<span>Round ${this._roundNum}/${this._maxRounds}</span> ${str}`);
		}

		private _printGames() {
			$('#GamesPlayedContainer').css("display", 'block').html(`<span>Games played ${this._gamesPlayed}/${this._maxGames}</span>`);
		}

		private _writeChat(msg: string) {
			const o = this._gamblers[this._gamblerPosition];
			const npc = setup.world.npc(Entity.NpcId.Dummy) // Fake NPC
			npc.name = o.n; // Swapsie name
			msg = PR.tokenizeString(setup.world.pc, npc, msg);
			$('#cofUItray').append("<P>" + msg + "</P>");
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			$('#cofUItray').animate({scrollTop: $('#cofUItray').prop("scrollHeight")}, 1000);
			if (this._chatLog == null) {
				this._chatLog = [];
			}
			this._chatLog.push(msg);
		}

		private _writeStartChat() {
			this._writeChat(`You start to play dice with NPC_NAME.\nThe wager is his ${this._getOfferBetString()} for ${this._getWantBetString()}.`);
		}

		private _refreshChatLog() {
			if (this._chatLog != null) {
				const cofUItray = document.getElementById('cofUItray');
				assertIsDefined(cofUItray);
				this._chatLog.forEach((lm) => {
					UI.appendNewElement('p', cofUItray, lm);
				})
				cofUItray.scrollTop = cofUItray.scrollHeight;
			}
		}

		private _getOfferBetString() {
			const o = this._gamblers[this._gamblerPosition];
			const bet = this._selectedBet === 1 ? o.bet1Offer : o.bet2Offer;
			const value = this._selectedBet === 1 ? o.bet1Value : o.bet2Value;
			switch (bet) {
				case "Coins":
					return `${value} coins`;
				case "Item":
					return "a mystery item";
			}
		}

		private _getWantBetString() {
			const o = this._gamblers[this._gamblerPosition];
			const bet = this._selectedBet === 1 ? o.bet1Want : o.bet2Want;
			const value = this._selectedBet === 1 ? o.bet1Value : o.bet2Value;
			switch (bet) {
				case "Coins":
					return `${value} coins`;
				default:
					return this._nouns[bet];
			}
		}

		// Callbacks
		private _cbQuitGame() {
			console.log("_cbQuitGame called");
			Engine.play("CoffinGameEnd");
			this._screen = "NEW"; // force the game to reset the next time it's loaded.
			this._playerTurn = true;
		}

		private _cbStartGame() {
			console.log("_cbStartGame called");
			$('#CoffinBetContainer').css("display", "block");
			$('#startButtons').css("display", "none");
			$('#betButtons').css("display", "block");
			if (this._selectedBet == null) this._dialogBox("PLACE BET", "gold");
			this._screen = "BET";
		}

		private _cbPlayAgain() {
			console.log("_cbPlayAgain called");
			this._setupPoints();
			$('#CoffinBetContainer').css("display", "block");
			$('#CoffinDiceContainer').css('display', 'none');
			$('#endButtons').css("display", "none");
			$('#betButtons').css("display", "block");
			$('#RoundContainer').css('display', 'none');
			if (this._selectedBet == null) this._dialogBox("PLACE BET", "gold");
			this._displayGambler(this._gamblerPosition);
			this._playerTurn = true;
			this._suddenDeath = false;
			this._roundNum = 1;
			this._screen = "BET";
		}

		private _cbNextBet() {
			const pos = this._gamblerPosition + 1 >= this._gamblers.length ? 0 : this._gamblerPosition + 1;
			this._displayGambler(pos);
			this._gamblerPosition = pos;
			this._selectedBet = null;
			$('#CoffinBet1Blocker').removeClass("selectedBet");
			$('#CoffinBet2Blocker').removeClass("selectedBet");
		}

		private _cbPrevBet() {
			const pos = this._gamblerPosition - 1 < 0 ? this._gamblers.length - 1 : this._gamblerPosition - 1;
			this._displayGambler(pos);
			this._gamblerPosition = pos;
			this._selectedBet = null;
		}

		// This is where we place the bet and start the game playing
		private _cbTakeBet() {
			if (this._selectedBet == null) {
				this._dialogBox("PLACE BET", "gold");
				return;
			}
			$('#CoffinBetContainer').css('display', 'none');
			$('#CoffinDiceContainer').css('display', 'block');
			$('#betButtons').css('display', 'none');
			$('#playButtons').css('display', 'block');
			this._screen = "GAME";

			if ((this._gamesPlayed % 2) === 0) {
				this._dialogBox("YOUR TURN", "gold");
			} else {
				this._dialogBox("OPPONENTS TURN", "gold");
				$("#cmdRollDice").css('display', 'none');
				this._playerTurn = false;
				this._interval = setInterval(this._npcRollDice.bind(this), Coffin._npcDiceDelay());
				Coffin._disableMenuLinks();
			}

			this._printRound();
			this._printGames();
			this._writeStartChat();
			Coffin._trackStat("played", 1);
		}

		private _cbSelectBet(e: JQuery.ClickEvent<unknown, {on: number, off: number}>) {
			this._selectedBet = e.data.on;
			this._highlightBet(e.data);
		}

		private _cbRollDice() {
			if (/* FIXME this._diceRolling == true || */ this._playerTurn == false) return; // noop
			rollADie({element: document.getElementById('CoffinDiceContainer') as HTMLElement, numberOfDice: 2, callback: this._cbDiceRolled.bind(this), delay: 3000});
		}

		private _cbDiceRolled(e: number[]) {
			const point = this._checkPoint(e);
			if (point !== -1) this._assignPoint(point);
			const win = this._checkWin(e);

			// Scored hit.
			if (point !== -1 || (this._suddenDeath && win !== 0)) {
				if (win !== 0) { // Something happened and we need to end the game.
					this._screen = "OVER";

					if (!this._playerTurn ) {
						clearInterval(this._interval); // Stop the npc if he's rolling
						Coffin._enableMenuLinks();
						$("#cmdRollDice").css('display', 'inline-block'); // turn button back on
					}

					this._gamesPlayed += 1;

					// switch button trays
					$('#playButtons').css("display", "none");
					$('#endButtons').css("display", "block");
					if (this._gamesPlayed > this._maxGames) $('#cmdPlayAgain').css('display', 'none');
					if (win === 1 || win === 2) {
						if (this._selectedBet === 1) {
							this._gamblers[this._gamblerPosition].bet1Status = win;
						} else {
							this._gamblers[this._gamblerPosition].bet2Status = win;
						}
					}
				}
			} else {
				// Code for handling misses

				if (this._playerTurn) {
					this._writeChat("<span style='color:red'>You miss the roll!</span>");
				} else {
					this._writeChat("NPC_NAME <span style='color:red'>misses the roll!</span>");
				}

				if ((this._gamesPlayed % 2) === 1) { // THE NPC IS PLAYER 1
					if (this._playerTurn) { // THE PC WAS THE ONE ROLLING
						this._roundNum++; // END THIS ROUND
						this._printRound();

						if (this._roundNum >= this._maxRounds) { // Check to see who wins or DRAW
							if (Coffin._totalPoints(this._myScore) > Coffin._totalPoints(this._opponentScore)) {
								this._dialogBox("YOU WIN!", "gold");
								this._writeChat("You won the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								Coffin._trackStat("won", 1);
								this._gamesPlayed += 1;
								this._adjustMoney(2);
								if (this._selectedBet === 1) {
									this._gamblers[this._gamblerPosition].bet1Status = BetStatus.Win;
								} else {
									this._gamblers[this._gamblerPosition].bet2Status = BetStatus.Win;
								}
							} else if (Coffin._totalPoints(this._myScore) < Coffin._totalPoints(this._opponentScore)) {
								this._dialogBox("YOU LOSE!", "red");
								this._writeChat("You lost the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								Coffin._trackStat("lost", 1);
								this._gamesPlayed += 1;
								this._adjustMoney(1);
								if (this._selectedBet === 1) {
									this._gamblers[this._gamblerPosition].bet1Status = BetStatus.Loss;
								} else {
									this._gamblers[this._gamblerPosition].bet2Status = BetStatus.Loss;
								}
							} else {
								this._dialogBox("DRAW", "gold");
								this._gamesPlayed += 1;
								this._writeChat("The game ends in a draw.");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								// this._trackStat('Draw', 1);
							}
						} else {
							// HAND OVER CONTROL TO NPC
							$("#cmdRollDice").css('display', 'none'); // HIDE ROLL BUTTON
							setTimeout(() => {this._dialogBox("OPPONENTS TURN", "gold")}, 500);
							this._playerTurn = false;
							this._interval = setInterval(this._npcRollDice.bind(this), Coffin._npcDiceDelay());
							Coffin._disableMenuLinks();
							this._printRound();
						}
					} else { // THE NPC WAS ROLLING
						clearInterval(this._interval); // STOP NPC ROLLING
						Coffin._enableMenuLinks();
						$("#cmdRollDice").css('display', 'inline-block'); // SHOW ROLL BUTTON
						// HAND OVER CONTROL TO PC
						this._playerTurn = true;
						setTimeout(() => {this._dialogBox("YOUR TURN", "gold");}, 500);
					}
					// TURN OFF PLAY AGAIN BUTTON AT MAX GAMES
					if (this._gamesPlayed > this._maxGames) $('#cmdPlayAgain').css('display', 'none');
				} else { // THE PC IS PLAYER 1
					if (this._playerTurn) { // THE PC WAS THE ONE ROLLING
						// HAND OVER CONTROL TO NPC
						$("#cmdRollDice").css('display', 'none'); // HIDE ROLL BUTTON
						setTimeout(() => {this._dialogBox("OPPONENTS TURN", "gold");}, 500);
						this._playerTurn = false;
						this._interval = setInterval(this._npcRollDice.bind(this), Coffin._npcDiceDelay());
						Coffin._disableMenuLinks();
					} else { // THE NPC WAS ROLLING
						this._roundNum++; // END THIS ROUND
						this._printRound();

						if (this._roundNum >= this._maxRounds) { // DRAW THE GAME.
							if (Coffin._totalPoints(this._myScore) > Coffin._totalPoints(this._opponentScore)) {
								this._dialogBox("YOU WIN!", "gold");
								this._writeChat("You won the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								Coffin._trackStat("won", 1);
								this._gamesPlayed += 1;
								this._adjustMoney(2);
								if (this._selectedBet === 1) {
									this._gamblers[this._gamblerPosition].bet1Status = 2; // win
								} else {
									this._gamblers[this._gamblerPosition].bet2Status = 2;
								}
							} else if (Coffin._totalPoints(this._myScore) < Coffin._totalPoints(this._opponentScore)) {
								this._dialogBox("YOU LOSE!", "red");
								this._writeChat("You lost the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								Coffin._trackStat("lost", 1);
								this._gamesPlayed += 1;
								this._adjustMoney(1);
								if (this._selectedBet === 1) {
									this._gamblers[this._gamblerPosition].bet1Status = BetStatus.Loss;
								} else {
									this._gamblers[this._gamblerPosition].bet2Status = BetStatus.Loss;
								}
							} else {
								this._dialogBox("DRAW", "gold");
								this._gamesPlayed += 1;
								this._writeChat("The game ends in a draw.");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								// this._trackStat('Draw', 1);
							}
						}

						this._playerTurn = true;
						clearInterval(this._interval); // STOP NPC ROLLING
						Coffin._enableMenuLinks();
						setTimeout(() => {this._dialogBox("YOUR TURN", "gold");}, 500);
						$("#cmdRollDice").css('display', 'inline-block'); // SHOW ROLL BUTTON
					}
					// TURN OFF PLAY AGAIN BUTTON AT MAX GAMES
					if (this._gamesPlayed > this._maxGames) $('#cmdPlayAgain').css('display', 'none');
				}
			}
		}

		private static _disableMenuLinks() {
			document.getElementById('game-info-links')!.style.visibility = "hidden";
		}

		private static _enableMenuLinks() {
			document.getElementById('game-info-links')!.style.removeProperty('visibility');
		}

		private _npcRollDice() {
			rollADie({element: document.getElementById('CoffinDiceContainer') as HTMLElement, numberOfDice: 2, callback: this._cbDiceRolled.bind(this), delay: 3000});
		}

		private static _npcDiceDelay() {
			return settings.fastAnimations ? 1000 : 2500;
		}

		private _checkWin(e: number[]) {
			const me = Coffin._totalPoints(this._myScore);
			const him = Coffin._totalPoints(this._opponentScore);

			// Doubles are an automatic win.
			if (this._suddenDeath && (e[0] === e[1])) {
				this._risingDialog("DOUBLES", "orange");
				if (this._playerTurn) {
					this._dialogBox("YOU WIN!", "gold");
					this._writeChat("You won the match!");
					Coffin._trackStat("won", 1);
					this._adjustMoney(2);
					return 2;
				} else {
					this._dialogBox("YOU LOSE!", "red");
					this._writeChat("You lost the match!");
					Coffin._trackStat("lost", 1);
					this._adjustMoney(1);
					return 1;
				}
			}

			if (me === 4 && him === 4) {
				this._dialogBox("DRAW", "gold");
				this._writeChat("The game ends in a draw.");
				// this._trackStat("draw", 1);
				return -1;
			} else if (me >= 5) {
				this._dialogBox("YOU WIN!", "gold");
				this._writeChat("You won the match!")
				Coffin._trackStat("won", 1);
				this._adjustMoney(2);
				return 2;
			} else if (him >= 5) {
				this._dialogBox("YOU LOSE!", "red");
				this._writeChat("You lost the match!");
				Coffin._trackStat("lost", 1);
				this._adjustMoney(1);
				return 1;
			} else if (me + him === 7) {
				if (!this._suddenDeath) {
					this._risingDialog("SUDDEN DEATH", "orange");
					this._writeChat("The game enters <span style='color:orange'>SUDDEN DEATH!</span>")
				}
				this._suddenDeath = true;
				return 0;
			}

			return 0;
		}

		private _checkPoint(dice: number[]) {
			const point = dice.reduce((a, b) => a + b);
			if (point > 9) return -1; // Out of range of any possible points.
			const index = point - 2; // shift 2-9 to 0-7
			if (this._myScore[index] === 1 || this._opponentScore[index] === 1) return -1; // Point already assigned.
			return index;
		}

		private _assignPoint(index: number) {
			if (this._playerTurn) {
				this._myScore[index] = 1;
				$(`#pcbox${index}`).addClass("CoffinpointHit");
				$(`#npcbox${index}`).addClass("CoffinpointMissed");
				this._writeChat(`<span style='color:lime'>You hit the ${index + 2}!</span>`);
			} else {
				this._opponentScore[index] = 1;
				$(`#npcbox${index}`).addClass("CoffinpointHit");
				$(`#pcbox${index}`).addClass("CoffinpointMissed");
				this._writeChat(`NPC_NAME <span style='color:lime'>hits the ${index + 2}!</span>`);
			}
		}

		private static _totalPoints(points: number[]) {
			return points.reduce((a, b) => a + b);
		}

		private _dialogBox(message: string, color: string) {
			const dialogProperties = {top: "90px", right: "0px", width: "800px", color: "DeepPink"};
			if (color !== undefined) dialogProperties.color = color;
			PR.dialogBox(this._element, message, dialogProperties);
		}

		private _risingDialog(message: string, color: string) {
			PR.risingDialog(this._element, message, color);
		}

		private static _trackStat(stat: keyof GameState.CoffinDiceStats, value: number) {
			if (setup.world.pc.gameStats.coffin.hasOwnProperty(stat))
				setup.world.pc.gameStats.coffin[stat] += value;
		}
	}

	// Sex scenes
	const scenes: Record<string, string[]> = {};

	scenes[Skills.Sexual.HandJobs] =
		[
			"You kneel in front of NPC_NAME and assume the familiar position of an experienced cock stroker. He quickly fetches his \
    impressive schlong out of his pants and waves it lightly in your face. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    You marvel at it's size and girth and involuntarily lick your lips. You'd love to be able to suck on it, or even \
    better, have him hammer away at your sissy asshole, but the bet was only for a quick handy, and it'd be \
    unprofessional of you to put your pleasure ahead of business.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    You marvel at it's size and girth and feel a faint flutter tingle up your backside. Wait? What?! When did you \
    start not only finding the cocks of other men impressive, but… sexually desirable? You concentrate and try to \
    suppress the twitching in your arsehole and instead get down to the task at hand.\n\n\
    <<else>>\
    It's bigger than average and you feel a slight tinge of envy, coupled with the shame of having to give this \
    bastard some sexual release, but a bet is a bet and a whore is a whore and unfortunately for you, you're a \
    whore who lost a bet. Time to get to work.\n\n\
    <</if>>\
    <</if>>\
    You take NPC_NAME's cock firmly in hand and start to gently tug him to erection. You feel his member stiffen \
    under your ministrations and eventually he lets loose a small gasp. You rub his cock back and forth across your \
    your face, looking him in the eyes while you slather it with your own drool for lubrication. Satisified that he's \
    now wet enough, you begin to stroke his meat in ernest. Within minutes he's panting and ready to blow. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    With practiced care, you angle his erection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your forehead, but some of it drips down to your lips and you \
    eagerly lick it up, earning you a smile from NPC_NAME.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    With practiced care, you angle his erection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your forehead, but some of it drips down to your lips and it \
    takes a surprisingly amount of restraint for you to resist simply licking it up.\n\n\
    <<else>>\
    You try your best to avoid the stream of jizz coming right at you, but unfortunately most of it lands on your face. \
    Apparently, NPC_NAME doesn't notice your look of disgust, but how could he since you're practically covered in a thick \
    layer of his baby batter.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a good wank)\
    "
		];

	scenes[Skills.Sexual.BlowJobs] =
		[
			"You kneel in front of NPC_NAME and assume the familiar position of an experienced cock sucker. He quickly fetches his \
    impressive schlong out of his pants and waves it lightly in your face. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    You marvel at it's size and girth and involuntarily lick your lips. Being able to suck on such a beautiful \
    dick hardly seems like a penalty for losing a bet, but alas here you are, about to get a mouth full of \
    delicious dong.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    You marvel at it's size and girth and involuntarily lick your lips, a slight trail of drool \
    escapes them and pools on your chest… Wait? What?! When did you \
    start not only finding the cocks of other men impressive, but… sexually desirable? You concentrate and try to \
    suppress the desire welling up in you and instead get down to the task at hand.\n\n\
    <<else>>\
    It's bigger than average and you feel a slight tinge of envy, coupled with the shame of having to give this \
    bastard some sexual release, but a bet is a bet and a whore is a whore and unfortunately for you, you're a \
    whore who lost a bet. Time to get to work.\n\n\
    <</if>>\
    <</if>>\
    You take NPC_NAME's cock firmly in hand and start to gently tug him to erection. You feel his member stiffen \
    under your ministrations and eventually he lets loose a small gasp. You rub his cock back and forth across your \
    your face, looking him in the eyes while you run it back and forth across your lips. Slowly you engulf the \
    head, rolling your tongue across it with long circular motions. NPC_NAME's dick instantly becomes as hard \
    as a rock and your start to suck on it in earnest. It doesn't take long after that before he's holding the \
    side of your head and jackhammering his dick down your throat. Without warning he smashes his pelvis into \
    your face and seizes up, a hot torrent of cum bubbling forth from his balls. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    Your eyes widen as his hot jizz starts to pour down your throat, you quickly move your head back so that the \
    remaining ejaculate pools your mouth. Moaning, you play with it on your tongue and savor it's flavor.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    Your eyes widen as his hot jizz starts to pump down your throat. Unbidden, a strong desire to taste this \
    man's cum springs forth in your mind - a shocking relevation to say the least. Fortunately, or perhaps \
    unfortunately, he's holding you too tightly and all you can do is whimper while his dick twitches in \
    your mouth.\n\n\
    <<else>>\
    You try your best to pull away, but NPC_NAME is holding you firmly and your forced to try to desperately \
    breathe through your nose as a seemingly endless amount of sperm is pumped into your stomach.\n\n\
    <</if>>\
    <</if>>\
    Eventually NPC_NAME is spent and he pulls out of your mouth with a load 'pop', a trail of your saliva mixed \
    with his cum spurts from your lips and lands on your chest.\n\n\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a blow job)\
    "
		];

	scenes[Skills.Sexual.TitFucking] =
		[
			"You kneel in front of NPC_NAME and assume the familiar position of an experienced whore. He quickly fetches his \
    impressive schlong out of his pants and waves it lightly in your face. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    You marvel at it's size and girth and involuntarily lick your lips. You'd love to be able to suck on it, or even \
    better, have him hammer away at your sissy asshole, but the bet was only for a titty fuck, and it'd be \
    unprofessional of you to put your pleasure ahead of business.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    You marvel at it's size and girth and involuntarily lick your lips, a slight trail of drool \
    escapes them and pools on your chest… Wait? What?! When did you \
    start not only finding the cocks of other men impressive, but… sexually desirable? You concentrate and try to \
    suppress the desire welling up in you and instead get down to the task at hand.\n\n\
    <<else>>\
    It's bigger than average and you feel a slight tinge of envy, coupled with the shame of having to give this \
    bastard some sexual release, but a bet is a bet and a whore is a whore and unfortunately for you, you're a \
    whore who lost a bet. Time to get to work.\n\n\
    <</if>>\
    <</if>>\
    You engulf NPC_NAME's cock with your nBUST and start to gently massage it. You feel his member stiffen \
    under your ministrations and eventually he lets loose a small gasp. You gently lick the tip of his \
    dick, letting large streams of saliva drool out of your mouth to coat his member and your tits. Once \
    sufficiently lubricated, you begin to earnestly jack him off with your fuck bags. It doesn't take long \
    after that before you feel his hands grasp your shoulders and his shoulders stiffen. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    With practiced care, you angle his erection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your forehead, but some of it drips down to your lips and you \
    eagerly lick it up, earning you a smile from NPC_NAME.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    With practiced care, you angle his erection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your tits, but some of it spatters your lips and it \
    takes a surprisingly amount of restraint for you to resist simply licking it up.\n\n\
    <<else>>\
    You try your best to avoid the stream of jizz coming right at you, but unfortunately most of it lands on your face and tits. \
    Apparently, NPC_NAME doesn't notice your look of disgust, but how could he since you're practically covered in a thick \
    layer of his baby batter.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a tit fuck)\
    "
		];

	scenes[Skills.Sexual.AssFucking] =
		[
			"You bend over in front of NPC_NAME, your bare ass and sissy slut hole exposed to the air. \
    You feel something wet and warm being rubbed between your arsecheeks, then a finger starts to gently \
    probe your anus. After a couple of minutes of these ministrations, the finger is replaced with the head \
    of a meaty cock. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    You shudder in anticipation. You weren't always such an anal whore, but now that you have a taste for it \
    you can't imagine going back. Just the thought of what's about to happen causes your nPENIS to stiffen and \
    your sissy asshole to quiver in excitement.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    You catch yourself in act of starting to drive back your sissy asshole to engulf his cock. A deep feeling of \
    horror wells up in your breast as you realize that you've begun to not only enjoy, but to greet the prospects \
    of such perverse acts with anticipation.\n\n\
    <<else>>\
    Your stomach does a small flip and you begin to feel ill at the prospect of another man ravaging your \
    slutty asshole. You do your best to ignore the feeling and swear to yourself that no matter what, you'll \
    never succumb to the life of a whore… even if sometimes it does feel a little good.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME works his tool up your well lubricated backside. His hands firmly grip onto your nHIPS and \
    he uses them for leverage as he starts to pounds your pASS arse. \
    <<if setup.world.pc.stat('core', 'perversion') > 80>>\
    You've long become accustomed to cocks pounding your sissy hole and the feeling it provides is one of \
    intense pleasure. Within moments you already feel your own orgasm building up as NPC_NAME continues to \
    mercilessly rail you. Eventually, he cums hard - impaling himself in you up to the hilt. The deep action \
    stirs something inside you and you feel your pBALLS start to well with your own cum. You scream out \
    sp(Yes! Fuck me! Fuck my ass!) and have an intense orgasm as NPC_NAME empties himself deep within your \
    slutty ass.\n\n\
    <<else>>\
    <<if setup.world.pc.stat('core', 'perversion') > 50>>\
    Even though you've become accustomed to cocks pounding your sissy hole, the feeling at first is quite \
    painful. You grit your teeth and endure while the sound of NPC_NAME slapping against your nASS \
    echoes in air. Slowly, you notice that the feeling of pain starts to turn into one of pleasure. You panic \
    at first, but the almost hypnotic sound of your ass being fucked combined with these new sensations \
    start to cause your breath to hitch and your mind to go blank. You almost don't even notice it when \
    NPC_NAME finally cums, impaling himself deep within your bowels. You let out a cry of shock, but one \
    tinged wth an undercurrent of desire.\n\n\
    <<else>>\
    The feeling of NPC_NAME's cock entering you is an almost indescribable mix of pain with an undercurrent \
    of something that your mind refuses to recognize as pleasure. The slapping sounds that fill the air from \
    NPC_NAME pillaging your arse form a strange cadence and you find yourself disassociating with your body \
    while you endure the act. Somewhere deep in the back of your mind a faint seed of perversion is sprouting, \
    a thought that you find entirely disturbing. You almost don't notice when NPC_NAME finally cums, filling \
    your bowels with his hot sticky seed.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a arse fuck)\
    "
		];

}
