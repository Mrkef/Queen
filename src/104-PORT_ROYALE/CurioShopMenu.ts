namespace App.UI {
	Macro.add('relicItem', {
		handler() {
			const entry = this.args[0] as Data.RelicStoreEntry;
			const d = Items.fetchData(entry.category, entry.tag);
			const itemDiv = makeElement('div', undefined, ['relic-item']);
			const topRow = itemDiv.appendNewElement('div');
			topRow.appendNewElement('div', d.shortDesc, ['relic-item-name']);
			const createButtonDiv = topRow.appendNewElement('div', undefined, ['relic-create'])
			const createButton = createButtonDiv.appendNewElement('button', 'CREATE', ['store-button']);
			const canAfford = entry.cost.every((ci) => {
				const item = setup.world.pc.getItemById(ci.type);
				return item && item.charges >= ci.amount;
			});
			if (canAfford) {
				createButton.addEventListener('click', () => {
					setup.world.pc.addItem(entry.category, entry.tag);
					for (const ci of entry.cost) {
						setup.world.pc.takeItem(ci.type, ci.amount);
					}
				})
			} else {
				createButton.classList.add('disabled-store-button');
			}

			const middleRow = itemDiv.appendNewElement('div');
			middleRow.appendNewElement('span', "Effects:", ['relic-effects-caption']);
			middleRow.appendNewElement('div', undefined, ['relic-effects']);
			middleRow.appendFormattedText(PR.getAllEffects(entry.category, entry.tag));

			const bottomRow = itemDiv.appendNewElement('div');
			bottomRow.appendNewElement('span', "Recipe:", ['relic-recipe-caption']);
			const recipeContent = entry.cost.map((ci) => {
				const r = Items.splitId(ci.type);
				return Items.fetchData(r.category, r.tag).shortDesc + (ci.amount > 1 ? ` × ${ci.amount}` : "");
			}).join(" + ");
			const recipe = bottomRow.appendNewElement('div', undefined, ['relic-recipe']);
			recipe.appendFormattedText(recipeContent);

			this.output.append(itemDiv);
		},
	});
}
