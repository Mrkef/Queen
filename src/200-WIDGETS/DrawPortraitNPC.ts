Macro.add("DrawPortraitNPC", {
	handler() {
		const elementId = 'npcPortraitUI';
		const div = App.UI.appendNewElement('div', this.output);
		div.id = elementId;
		div.style.display = 'inline-block';
		$('<div id="hiddenNPCPortraitCanvas" style = "display:none" > </div>').appendTo(this.output as DocumentFragment);
		setup.avatar.drawPortraitNPC(this.args[0] as string, elementId, 200, 200);
	},
});
