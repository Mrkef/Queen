Macro.add("tarot", {
	handler() {
		const pl = this.args[0] as App.Entity.Player;
		const card = pl.flags["TAROT_CARD"] as string;
		if (card && App.Data.tarot.hasOwnProperty(card)) {
			const dict = App.Data.tarot[card];
			$(this.output).wiki(dict.msg);
			pl.applyEffects(dict.effects);
			delete pl.flags["TAROT_CARD"];
		}
	},
});

Macro.add("readTarot", {
	handler() {
		const pl = this.args[0] as App.Entity.Player;
		const card = Object.keys(App.Data.tarot).randomElement();
		console.log(card);
		this.output.append(App.UI.makeElement('div', undefined, ['tcard', ...App.Data.tarot[card].css.split(' ')]));
		$(this.output).wiki(App.Data.tarot[card].chat);
		pl.flags["TAROT_CARD"] = card;
	},
});
