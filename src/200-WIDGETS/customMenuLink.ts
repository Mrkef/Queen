Macro.add("customMenuLink", {
	handler() {
		const link = App.UI.appendNewElement('a', this.output, this.args[0] as string);
		if (this.args[1] !== undefined) {
			link.dataset.menuaction = this.args[1] as string;
		}
		link.dataset.target = this.args[2] as string ?? this.args[0] as string;
		link.addEventListener('click', (event) => {
			const tgt = event.target as HTMLElement;
			const menuAction = tgt.dataset.menuaction;
			if (menuAction) {
				State.variables.menuAction = menuAction;
			}
			if (!tags().includes('custom-menu')) {
				State.variables.gameBookmark = passage();
			}
			const targetPassage = tgt.dataset.target;
			if (targetPassage) {
				Engine.play(targetPassage);
			}
		});
	},
});
