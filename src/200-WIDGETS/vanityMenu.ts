namespace App.UI.Widgets {

	interface BeautyResource {
		name: string;
		short: string;
		id: Data.CosmeticsType;
	}
	abstract class VanityTable<T extends string> extends SelfUpdatingTable {
		readonly #player: Entity.Player;
		readonly #groupName: string;
		readonly #resources: BeautyResource[];
		#resourceAvailable: number[];
		#foot: HTMLTableSectionElement | null;

		constructor(human: Entity.Player, groupName: string, name: string, resources: BeautyResource[]) {
			super();
			this.table.classList.add('vanity-table');
			this.#player = human;
			this.#groupName = groupName;
			this.#resources = resources;

			this.header.append(VanityTable.vanityTableHeader(name, resources.map(r => r.short)));

			this.#resourceAvailable = this.#resources.map(r => this.#player.itemChargesOfType(r.id));
			this.#foot = null;
		}

		get player() {
			return this.#player;
		}

		protected get availableResources() {
			return this.#resourceAvailable;
		}

		protected static vanityTableHeader(name: string, resourceNames: string[]): HTMLTableRowElement {
			const res = document.createElement('tr');
			const radioCol = App.UI.appendNewElement('th', res);
			radioCol.style.width = "1.5em";
			radioCol.style.textAlign = "center";
			const nameCol = App.UI.appendNewElement('th', res, name);
			nameCol.style.width = "100%";
			for (const rn of resourceNames) {
				const col = App.UI.appendNewElement('th', res, rn);
				col.style.fontSize = "smaller";
			}
			return res;
		}

		protected makeVanityTableRow(value: T, checked: boolean, enabled: boolean, desc: string,
			resources: number[]): HTMLTableRowElement {
			const row = document.createElement('tr');
			const cRadio = appendNewElement('td', row);
			if (enabled) {
				const inp = appendNewElement('input', cRadio);
				inp.type = "radio";
				inp.checked = checked;
				inp.name = this.#groupName;
				inp.value = value;
			}

			const cDesc = appendNewElement('td', row);
			$(cDesc).wiki(desc);

			// eslint-disable-next-line unicorn/no-for-loop
			for (let i = 0; i < resources.length; ++i) {
				const rn = resources[i];
				const avail = this.availableResources[i];
				if (rn === 0) {
					appendNewElement('td', row, '-');
				} else {
					appendNewElement('td', row, String(rn), avail >= rn ? [] : ['state-disabled']);
				}
			}
			return row;
		}

		override update(): void {
			this.#resourceAvailable = this.#resources.map(r => this.#player.itemChargesOfType(r.id));
			replace(this.body, this.renderBody());
			if (this.#foot) {
				this.#foot.remove()
				this.#foot = null;
			}
			const foot = this.renderFooter();
			if (foot.length > 0) {
				this.#foot = document.createElement('tfoot');
				this.#foot.append(...foot);
				this.table.append(this.#foot);
			}
		}

		get selectedStyle(): T {
			const selector = `input[name="${this.#groupName}"]:checked`
			const element = document.querySelector(selector);
			if (element) {
				return (element as HTMLInputElement).value as T;
			}
			throw "Could not find selected input element";
		}

		protected abstract renderBody(): DocumentFragment;
		protected abstract renderFooter(): HTMLTableRowElement[];
	}

	export class VanityHairTable extends VanityTable<Data.Fashion.HairStyle | Data.ItemNameTemplateClothing> {
		constructor(player: Entity.Player) {
			super(player, "radiogroup-hairstyle", "Hair Style", [
				{id: Data.CosmeticsType.HairTool, name: "Accessories", short: "Acc."},
				{id: Data.CosmeticsType.HairTreatment, name: "Products", short: "Prod."},
			]);
		}

		override renderBody(): DocumentFragment {
			const res = new DocumentFragment();

			const wig = this.player.equipmentInSlot(ClothingSlot.Wig);
			const wigs = this.player.wardrobeItemsBySlot(ClothingSlot.Wig);
			if (wig) {
				res.append(this._makeWigRow(wig, true));
			}
			for (const wig of wigs) {res.append(this._makeWigRow(wig, false));}

			const hL = this.player.stat(Stat.Body, BodyPart.Hair);

			const styleName = (hs: Data.HairStyleDesc, enabled: boolean): string => {
				if (!enabled) {
					if (hL < hs.min) {
						return `@@.state-negative;⇓Hair@@ ${hs.short}`;
					}
					if (hL > hs.max) {
						return `@@.state-negative;⇑Hair@@ ${hs.short}`;
					}
					return `@@.state-disabled;${hs.short}@@`;
				}
				return `${PR.colorizeString(hs.style, "⇑Style")} ${hs.short}`;
			};

			for (const [h, hs] of Object.entries(Data.Lists.hairStyles)) {
				if (hs.difficulty < 0) {continue;}

				const enabled = hs.resource1 <= this.availableResources[0] && hs.resource2 <= this.availableResources[1]
					&& hL <= hs.max && hL >= hs.min;
				const selected = !wig && this.player.lastUsedHair === h;
				res.append(this.makeVanityTableRow(h, selected, enabled, styleName(hs, enabled), [hs.resource1, hs.resource2]));
			}

			return res;
		}

		override renderFooter(): HTMLTableRowElement[] {
			const row = document.createElement('tr');
			appendNewElement('td', row);
			const text = appendNewElement('td', row, "Accessories & Products Owned");
			text.style.textAlign = "right";
			appendNewElement('td', row, String(this.availableResources[0]));
			appendNewElement('td', row, String(this.availableResources[1]));
			return [row];
		}

		private _makeWigRow(wig: Items.Wig, worn: boolean): HTMLTableRowElement {
			return this.makeVanityTableRow(wig.id, worn, true,
				`${PR.colorizeString(wig.hairBonus, "⇑Wig")} ${wig.description}`, [0, 0]);
		}
	}

	export class VanityMakeupTable extends VanityTable<Data.Fashion.Makeup> {
		constructor(player: Entity.Player) {
			super(player, "makeup", "Makeup Styles", [
				{id: Data.CosmeticsType.BasicMakeup, name: "Basic makeup", short: "Bas."},
				{id: Data.CosmeticsType.ExpensiveMakeup, name: "Expensive makeup", short: "Exp."},
			]);
		}

		override renderBody(): DocumentFragment {
			const res = new DocumentFragment();
			for (const [m, ms] of Object.entries(Data.Lists.makeupStyles)) {
				const enabled = ms.resource1 <= this.availableResources[0] && ms.resource2 <= this.availableResources[1];
				res.append(this.makeVanityTableRow(m, this.player.makeupStyle === m, enabled,
					`${PR.colorizeString(ms.style, "⇑Style")} ${ms.short}`, [ms.resource1, ms.resource2]));
			}
			return res;
		}

		override renderFooter(): HTMLTableRowElement[] {
			const row = document.createElement('tr');
			appendNewElement('td', row);
			const text = appendNewElement('td', row, "Basic & Expensive Makeup Owned");
			text.style.textAlign = "right";
			appendNewElement('td', row, String(this.availableResources[0]));
			appendNewElement('td', row, String(this.availableResources[1]));
			return [row];
		}
	}

	Macro.add("vanityMenu", {
		skipArgs: true,
		handler() {
			const res = new DocumentFragment();
			const stylingEnergyRequirement: UI.FormattedFragment = {text: "(1 Energy)", style: "item-energy"};

			const hairStyles = new VanityHairTable(setup.world.pc);
			const makeups = new VanityMakeupTable(setup.world.pc);

			const tables = appendNewElement('div', res);
			tables.style.marginBottom = "1ex";
			tables.style.marginTop = "1ex";
			tables.style.display = "flex";
			tables.append(hairStyles.render(), makeups.render());

			const actionsDiv = appendNewElement('div', res);
			const energy = setup.world.pc.stat(Stat.Core, CoreStat.Energy);
			appendFormattedText(actionsDiv, {text: "Actions", style: "action-general"}, ": ");
			const actions: (string | Node)[] = [];

			const optionalAction = (enabled: boolean, handler: () => void, ...text: (string | FormattedFragment)[]) => {
				if (enabled) {
					const link = document.createElement('a');
					appendFormattedText(link, ...text);
					link.addEventListener('click', handler);
					actions.push(link);
				} else {
					const span = document.createElement('span');
					appendFormattedText(span, ...text);
					actions.push(span);
					span.classList.add('state-disabled');
				}
			};

			optionalAction(energy > 1, () => {
				setup.world.pc.adjustCoreStat(CoreStat.Energy, -1);
				setup.world.pc.doStyling(hairStyles.selectedStyle, makeups.selectedStyle);
				Engine.play(passage());
			}, "Do Makeup and Hair ", stylingEnergyRequirement);

			optionalAction(energy > 1 && setup.world.pc.makeupStyle !== Data.Fashion.Makeup.Plain, () => {
				setup.world.pc.makeupStyle = Data.Fashion.Makeup.Plain;
				setup.world.pc.makeupBonus = 0;
				setup.world.pc.adjustCoreStat(CoreStat.Energy, -1);
				Engine.play(passage());
			}, "Remove Makeup ", stylingEnergyRequirement);

			const us = App.unitSystem;
			const hairLength = setup.world.pc.stat(Stat.Body, BodyPart.Hair);
			const newHairLength = Math.max(5, hairLength - 5);
			optionalAction(energy > 1 && hairLength > 5, () => {
				setup.world.pc.clothing.takeOffSlot(ClothingSlot.Wig);
				setup.world.pc.adjustCoreStat(CoreStat.Energy, -1);
				setup.world.pc.adjustBody(BodyPart.Hair, newHairLength - hairLength);
				Engine.play(passage());
			}, `Trim hair ${us.lengthString(PR.statValueToCM(hairLength, BodyPart.Hair), false)} → ${us.lengthString(PR.statValueToCM(newHairLength, BodyPart.Hair), false)}`, stylingEnergyRequirement);

			actionsDiv.append(linksStrip(...actions));
			this.output.append(res);
		},
	});
}
