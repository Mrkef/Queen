namespace App {
	Macro.add("whoreLink", {
		handler() {
			const whoringData = Data.whoring[this.args[0] as string];
			const phases = whoringData.phases ?? [DayPhase.Morning, DayPhase.Afternoon, DayPhase.Evening, DayPhase.Night];
			const available = whoringData.available
				? new Conditions.Expression(whoringData.available).evaluateFast(Conditions.EvaluationContext.makeFreeContext())
				: true;
			if (!available || setup.world.pc.stat(Stat.Core, CoreStat.Energy) < 1 || !phases.includes(setup.world.phase)) {
				this.output.append(UI.makeElement('span', "[Whore]", ['state-disabled']));
			} else {
				if (setup.world.whoreTutorial) {
					this.output.append(UI.passageLink("Whore", "Whore", () => {
						setup.world.pc.adjustCoreStat(CoreStat.Energy, -1);
						setup.slotEngine.loadScene(this.args[0] as string, passage());
					}));
				} else {
					this.output.append(UI.passageLink("Whore", "WhoreTutorial", () => {
						setup.world.whoreTutorial = true;
						variables().whore.link = this.args[0] as string;
						variables().whore.passage = passage();
					}));
				}
			}
		},
	});
}
