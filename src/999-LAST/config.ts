// Social Media Links
// const redditLink = "https://www.reddit.com/r/QueenOfTheSeas/";

// Sugarcube config
State.prng.init();

console.log("Sugarcube Config…");
Config.history.controls = false;
Config.history.maxStates = 1;
Config.passages.descriptions = App.PR.passageDescription;
Config.ui.updateStoryElements = false; // we draw buttons ourselves
// and remove the ones provided by SugarCube
document.querySelector("div#ui-bar-body nav#menu[role='navigation']")?.remove();
Config.saves.version = App.Data.game.version.save;

console.log("Loading QoS Internal engines…");
setup.world = new App.Entity.World();

// Jobs
App.Job.loadJobs();
// Quests
App.Quest.loadQuests();
// Initialize Avatar Engine
setup.avatar = new App.Entity.AvatarEngine();
// Event Engine
setup.eventEngine = new App.EventEngine();
// Load gambling classes
setup.slotEngine = new App.SlotEngine(setup.world.pc);
setup.coffinGame = new App.Gambling.Coffin();
// Load Combat Engine
setup.combat = new App.Combat.CombatEngine();
// Load Spectator Engine
setup.spectator = new App.Combat.SpectatorEngine();
// Load notification engine
setup.notifications = new App.Notifications.Engine();
// Load Loot engine
setup.loot = new App.Loot();
// Audio
setup.audio = new App.Audio();
console.log("Finished loading QoS engines!");

// Funnel navigation to the event engine
Config.navigation.override = (passageName) => {
	return setup.eventEngine.checkEvents(setup.world.pc, State.passage, passageName);
};

Setting.addHeader("Display and UI");
Setting.addToggle("displayAvatar", {
	label: "Display the PC Avatar in mirrors and Portrait in UI",
	default: true,
	onInit: setup.avatar.settingHandler.bind(setup.avatar),
	onChange: setup.avatar.settingHandler.bind(setup.avatar),
});

Setting.addToggle("displayNPC", {
	label: "Display NPC Avatars and Portraits in UI",
	default: true,
	onInit: setup.avatar.npcSettingHandler.bind(setup.avatar),
	onChange: setup.avatar.npcSettingHandler.bind(setup.avatar),
});

Setting.addToggle("displayBodyScore", {
	label: "Display numerical body parameters",
	default: false,
	onChange: App.PR.handleDisplayBodyScoreChanged,
});

Setting.addToggle("inlineItemDetails", {
	label: "Inline detailed information in inventory and shops",
	desc: "Toggles displaying additional information in the inventory and shop interfaces.",
	default: false,
});

// Setting for showing numbers along with the meters
Setting.addToggle("displayMeterNumber", {
	label: "Show numbers in (pseudo-)graphical meters",
	desc: "Controls printing number values for each meter bar in addition to the graphical representation",
	default: false,
	onInit: App.PR.handleMetersNumberValueSettingChanged,
	onChange: App.PR.handleMetersNumberValueSettingChanged,
});

Setting.addList("theme", {
	label: "Theme",
	desc: "Color theme. //Reloading might be required//",
	list: ["dark", "light", "grey", "custom"],
	default: "dark",
	onInit: () => App.UI.loadTheme(settings.theme),
	onChange: () => App.UI.loadTheme(settings.theme),
});

Setting.addHeader("Gameplay")

Setting.addToggle("alternateControlForRogue", {
	label: "Alternate Control Scheme for Rogue-like elements",
	desc: "Use this if your device does not have a numpad. You may have to exit/enter Rogue-like again for it to take effect.",
	default: false,
});

Setting.addToggle("autosaveAtSafePlaces", {
	label: "Automatic saving",
	desc: "Automatically save game at selected safe locations",
	default: false,
	onInit: App.PR.handleAutosaveChanged,
	onChange: App.PR.handleAutosaveChanged,
});

Setting.addRange("bgmVolume", {
	label: "BGM Volume",
	desc: "Background music volume",
	min: 0,
	max: 10,
	step: 1,
	onInit: setup.audio.volumeInit.bind(setup.audio),
	onChange: setup.audio.volumeAdjust.bind(setup.audio),
	default: 2,
});

Setting.addToggle("fastAnimations", {
	label: "Fast Animations",
	default: false,
});

// Setting up a basic list control for the settings property 'units'
Setting.addList("units", {
	label: "Unit system",
	desc: "Choose the unit system.",
	list: ["Imperial", "Metric"],
	default: "Imperial",
	onInit: App.unitSystem.unitSettingChangedHandler,
	onChange: App.unitSystem.unitSettingChangedHandler,
});

// SugarCube event handlers
App.EventHandlers.init();

Save.onLoad.add(App.EventHandlers.onLoad);
Save.onSave.add(App.EventHandlers.onSave);
Config.saves.isAllowed = function () {
	return !tags().includes("no-saving");
};

// Setup some divs that will not get refreshed on passage navigation.
if ($('#hiddenCanvas').length === 0) {
	$("<canvas id='hiddenCanvas'></div>").appendTo(document.body);
}

// temp

App.UI.PassageRenderManager.instance.registerHandler(App.UI.rPostProcess);
