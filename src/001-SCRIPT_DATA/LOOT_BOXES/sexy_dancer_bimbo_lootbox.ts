// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange
namespace App.Data {
	Object.append(lootBoxes, {
		"common dancer loot box": {
			name: "common dancer chest",
			shortDesc: "@@.item-common;a small tacky looking treasure chest@@",
			longDesc: "This small black treasure chest sports an attractive leopard print pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["DANCER_BIMBO_LOOT_BOX_COMMON"],
		},

		"uncommon dancer loot box": {
			name: "uncommon dancer chest",
			shortDesc: "@@.item-uncommon;a tacky looking  treasure chest@@",
			longDesc: "This black treasure chest sports an attractive leopard print pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["DANCER_BIMBO_LOOT_BOX_UNCOMMON"],
		},

		"rare dancer loot box": {
			name: "rare dancer chest",
			shortDesc: "@@.item-rare;a large tacky looking treasure chest@@",
			longDesc: "This large black treasure chest sports an attractive leopard print pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["DANCER_BIMBO_LOOT_BOX_RARE"],
		},

		"legendary dancer loot box": {
			name: "legendary dancer chest",
			shortDesc: "@@.item-legendary;a huge tacky looking treasure chest@@",
			longDesc: "This huge black treasure chest sports an attractive leopard print pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["DANCER_BIMBO_LOOT_BOX_LEGENDARY"],
		},
	});

	lootTables["DANCER_BIMBO"] = [
		Loot.makeLootTableItemNoFilter("coins", 100, 1, 50),
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "style", [Fashion.Style.Bimbo, Fashion.Style.SexyDancer]),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		Loot.makeLootTableItem(Items.Category.Food, 75, 2, "effects", [
			"BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY",
			"FITNESS_XP_COMMON", "FITNESS_XP_UNCOMMON", "FITNESS_XP_RARE", "FITNESS_XP_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
		]),
		Loot.makeLootTableItem(Items.Category.Food, 30, 4, "effects", ["WHOLESOME_MEAL", "SNACK", "LIGHT_WHOLESOME_MEAL"]),
		Loot.makeLootTableItem(Items.Category.Food, 50, 5, "effects", ["HARD_ALCOHOL"]),
		Loot.makeLootTableItem(Items.Category.Drugs, 75, 2, "effects", [
			"BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY",
			"FITNESS_XP_COMMON", "FITNESS_XP_UNCOMMON", "FITNESS_XP_RARE", "FITNESS_XP_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
		]),
		Loot.makeLootTableItem(Items.Category.Drugs, 50, 2, "effects", [
			"ENERGY_COMMON", "ENERGY_UNCOMMON", "ENERGY_RARE", "ENERGY_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"PERVERSION_XP_COMMON", "PERVERSION_XP_UNCOMMON", "PERVERSION_XP_RARE", "PERVERSION_XP_LEGENDARY",
		]),
	];
}
