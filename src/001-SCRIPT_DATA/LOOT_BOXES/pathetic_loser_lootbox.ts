// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange

namespace App.Data {
	lootBoxes["pathetic loser chest"] = {
		name: "Pathetic loser chest",
		shortDesc: "@@.item-quest;A big box with a big purple bow over it@@",
		longDesc: "This sparkling big box is shaped like a giftbox you used to give to your fiancee and opens with a big bow on it's top. Wonder what's inside?",
		message: Data.defaultLootBotMessage,
		type: ItemTypeConsumable.LootBox,
		// Effect : [ TABLE, Minimum Roll, Bonus to roll
		effects: ["PATHETIC_LOSER_LOOT_BOX_COMMON"],
	};

	lootTables["PATHETIC_LOSER"] = [
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "name", ["wig"], true),
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "name", ["choker"], true),
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "name", ["fishnet tights"], true),
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "name", ["pathetic loser costume"], true),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 100, 20, undefined, undefined, true),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 100, 20, undefined, undefined, true),
		Loot.makeLootTableItemNoFilter(Items.Category.Food, 50, 50, 200),
		Loot.makeLootTableItemNoFilter(Items.Category.Drugs, 50, 100, 200),
	];
}
