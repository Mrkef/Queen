// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange

namespace App.Data {
	Object.append(lootBoxes, {
		"landlubber chest": {
			name: "Landlubber chest",
			shortDesc: "@@.state-girlinness;A small box with a big pink bow over it@@",
			longDesc: "This small box is shaped like a giftbox you used to give to your fiancee and opens with a big bow on it's top. Wonder what's inside?",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["LANDLUBBER_LOOT_BOX_COMMON"],
		},
	});

	Object.append(lootTables, {
		LANDLUBBER: [
			Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "name", ["landlubber costume"], true),
			Loot.makeLootTableItem(Items.Category.Cosmetics, 100, 20, undefined, undefined, true),
			Loot.makeLootTableItem(Items.Category.Cosmetics, 100, 20, undefined, undefined, true),
			Loot.makeLootTableItem(Items.Category.Food, 50, 20, undefined, undefined, true),
			Loot.makeLootTableItemNoFilter(Items.Category.Food, 50, 1, 200),
			Loot.makeLootTableItemNoFilter(Items.Category.Drugs, 50, 1, 200),
		],
	});
}
