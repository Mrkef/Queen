// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange

namespace App.Data {
	Object.append(lootBoxes, {
		"common sissy loot box": {
			name: "common sissy chest",
			shortDesc: "@@.item-common;A small gift box shaped like a treasure chest@@",
			longDesc: "This small pink treasure chest sports an attractive heart pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["SISSY_BIMBO_LOOT_BOX_COMMON"],
		},

		"uncommon sissy loot box": {
			name: "uncommon sissy chest",
			shortDesc: "@@.item-uncommon;A gift box shaped like a treasure chest@@",
			longDesc: "This pink treasure chest sports an attractive heart pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["SISSY_BIMBO_LOOT_BOX_UNCOMMON"],
		},

		"rare sissy loot box": {
			name: "rare sissy chest",
			shortDesc: "@@.item-rare;A large gift box shaped like a treasure chest@@",
			longDesc: "This large pink treasure chest sports an attractive heart pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["SISSY_BIMBO_LOOT_BOX_RARE"],
		},

		"legendary sissy loot box": {
			name: "legendary sissy chest",
			shortDesc: "@@.item-legendary;A huge gift box shaped like a treasure chest@@",
			longDesc: "This huge pink treasure chest sports an attractive heart pattern and is wrapped with a shiny white ribbon and bow.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["SISSY_BIMBO_LOOT_BOX_LEGENDARY"],
		},
	});

	lootTables["SISSY_BIMBO"] = [
		Loot.makeLootTableItemNoFilter("coins", 100, 1, 50),
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "style", [Fashion.Style.Bimbo, Fashion.Style.SissyLolita]),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		Loot.makeLootTableItem(Items.Category.Food, 75, 2, "effects", [
			"BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY",
			"FITNESS_XP_COMMON", "FITNESS_XP_UNCOMMON", "FITNESS_XP_RARE", "FITNESS_XP_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"LIPS_XP_COMMON", "LIPS_XP_UNCOMMON", "LIPS_XP_RARE", "LIPS_XP_LEGENDARY",
		]),
		Loot.makeLootTableItem(Items.Category.Food, 30, 4, "effects", ["WHOLESOME_MEAL", "SNACK", "LIGHT_WHOLESOME_MEAL"]),
		Loot.makeLootTableItem(Items.Category.Food, 50, 5, "effects", ["HARD_ALCOHOL"]),
		Loot.makeLootTableItem(Items.Category.Drugs, 75, 2, "effects", [
			"BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY",
			"FITNESS_XP_COMMON", "FITNESS_XP_UNCOMMON", "FITNESS_XP_RARE", "FITNESS_XP_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"FACE_XP_COMMON", "FACE_XP_UNCOMMON", "FACE_XP_RARE", "FACE_XP_LEGENDARY",
		]),
		Loot.makeLootTableItem(Items.Category.Drugs, 50, 2, "effects", [
			"ENERGY_COMMON", "ENERGY_UNCOMMON", "ENERGY_RARE", "ENERGY_LEGENDARY",
			"SEDUCTION_XP_COMMON", "SEDUCTION_XP_UNCOMMON", "SEDUCTION_XP_RARE", "SEDUCTION_XP_LEGENDARY",
			"PERVERSION_XP_COMMON", "PERVERSION_XP_UNCOMMON", "PERVERSION_XP_RARE", "PERVERSION_XP_LEGENDARY",
			"FEMININITY_XP_COMMON", "FEMININITY_XP_UNCOMMON", "FEMININITY_XP_RARE", "FEMININITY_XP_LEGENDARY",
		]),
	];
}
