// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange

namespace App.Data {
	Object.append(lootBoxes, {
		"common pirate loot box": {
			name: "common pirate chest",
			shortDesc: "@@.item-common;a studded iron treasure chest emblazoned with a skull and bones@@",
			longDesc: "This small treasure chest is banded and studded with iron rivets. It has a white skull and bones on its top and smells faintly of the sea.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["PIRATE_BDSM_LOOT_BOX_COMMON"],
		},

		"uncommon pirate loot box": {
			name: "uncommon pirate chest",
			shortDesc: "@@.item-uncommon;a studded iron treasure chest emblazoned with a skull and bones@@",
			longDesc: "This treasure chest is banded and studded with iron rivets. It has a white skull and bones on its top and smells faintly of the sea.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			charges: 1,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["PIRATE_BDSM_LOOT_BOX_UNCOMMON"],
		},

		"rare pirate loot box": {
			name: "rare pirate chest",
			shortDesc: "@@.item-rare;a studded iron treasure chest emblazoned with a skull and bones@@",
			longDesc: "This large treasure chest is banded and studded with iron rivets. It has a white skull and bones on its top and smells faintly of the sea.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["PIRATE_BDSM_LOOT_BOX_RARE"],
		},

		"legendary pirate loot box": {
			name: "legendary pirate chest",
			shortDesc: "@@.item-legendary;a studded iron treasure chest emblazoned with a skull and bones@@",
			longDesc: "This huge treasure chest is banded and studded with iron rivets. It has a white skull and bones on its top and smells faintly of the sea.",
			message: Data.defaultLootBotMessage,
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["PIRATE_BDSM_LOOT_BOX_LEGENDARY"],
		},
	});

	lootTables["PIRATE_BDSM"] = [
		Loot.makeLootTableItemNoFilter("coins", 100, 1, 50),
		Loot.makeLootTableItem(Items.Category.Clothes, 100, 1, "style", [Fashion.Style.Bdsm, Fashion.Style.PirateSlut]),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		Loot.makeLootTableItem(Items.Category.Cosmetics, 80, 5),
		Loot.makeLootTableItem(Items.Category.Food, 75, 2, "effects", [
			"ASS_XP_COMMON", "ASS_XP_UNCOMMON", "ASS_XP_RARE", "ASS_XP_LEGENDARY",
			"SWASHBUCKLING_XP_COMMON", "SWASHBUCKLING_XP_UNCOMMON", "SWASHBUCKLING_XP_RARE", "SWASHBUCKLING_XP_LEGENDARY",
			"NAVIGATING_XP_COMMON", "NAVIGATING_XP_UNCOMMON", "NAVIGATING_XP_RARE", "NAVIGATING_XP_LEGENDARY",
			"SAILING_XP_COMMON", "SAILING_XP_UNCOMMON", "SAILING_XP_RARE", "SAILING_XP_LEGENDARY",
		]),
		Loot.makeLootTableItem(Items.Category.Food, 30, 4, "effects", ["WHOLESOME_MEAL", "SNACK", "LIGHT_WHOLESOME_MEAL"]),
		Loot.makeLootTableItem(Items.Category.Food, 50, 5, "effects", ["HARD_ALCOHOL"]),
		Loot.makeLootTableItem(Items.Category.Drugs, 75, 2, "effects", [
			"ASS_XP_COMMON", "ASS_XP_UNCOMMON", "ASS_XP_RARE", "ASS_XP_LEGENDARY",
			"SWASHBUCKLING_XP_COMMON", "SWASHBUCKLING_XP_UNCOMMON", "SWASHBUCKLING_XP_RARE", "SWASHBUCKLING_XP_LEGENDARY",
			"NAVIGATING_XP_COMMON", "NAVIGATING_XP_UNCOMMON", "NAVIGATING_XP_RARE", "NAVIGATING_XP_LEGENDARY",
			"SAILING_XP_COMMON", "SAILING_XP_UNCOMMON", "SAILING_XP_RARE", "SAILING_XP_LEGENDARY",
		]),
		Loot.makeLootTableItem(Items.Category.Drugs, 50, 2, "effects", [
			"HEAL_COMMON", "HEAL_UNCOMMON", "HEAL_RARE", "HEAL_LEGENDARY",
			"ASS_XP_COMMON", "ASS_XP_UNCOMMON", "ASS_XP_RARE", "ASS_XP_LEGENDARY",
			"PERVERSION_XP_COMMON", "PERVERSION_XP_UNCOMMON", "PERVERSION_XP_RARE", "PERVERSION_XP_LEGENDARY",
		]),
	];
}
