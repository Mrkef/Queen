namespace App.Data {
	Object.append(App.Data.effectLibrary, {
		//* FACE */
		FACE_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Face, -50);},
			value: 50, knowledge: ["Face Harder+"],
		},
		FACE_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Face, -100);},
			value: 50, knowledge: ["Face Harder++"],
		},
		FACE_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Face, -200);},
			value: 50, knowledge: ["Face Harder+++"],
		},
		FACE_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Face, -300);},
			value: 50, knowledge: ["Face Harder++++"],
		},
		//* BUST */
		BUST_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Bust, -50);},
			value: 50, knowledge: ["Bust Shrink+"],
		},
		BUST_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Bust, -100);},
			value: 100, knowledge: ["Bust Shrink++"],
		},
		BUST_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Bust, -200);},
			value: 200, knowledge: ["Bust Shrink+++"],
		},
		BUST_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Bust, -400);},
			value: 400, knowledge: ["Bust Shrink++++"],
		},
		/** BUST FIRMNESS */
		BUST_SAGGINESS_XP_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.BustFirmness, -50);},
			value: 50, knowledge: ["Bust Sag+"],
		},
		BUST_SAGGINESS_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.BustFirmness, -100);},
			value: 100, knowledge: ["Bust Sag++"],
		},
		BUST_SAGGINESS_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.BustFirmness, -500);},
			value: 500, knowledge: ["Bust Sag+++"],
		},
		BUST_SAGGINESS_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.BustFirmness, -1000);},
			value: 1000, knowledge: ["Bust Sag++++"],
		},
		//* ASS */
		ASS_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Ass, -50);},
			value: 50, knowledge: ["Ass Shrink+"],
		},
		ASS_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Ass, -100);},
			value: 100, knowledge: ["Ass Shrink++"],
		},
		ASS_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Ass, -200);},
			value: 200, knowledge: ["Ass Shrink+++"],
		},
		ASS_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Ass, -400);},
			value: 400, knowledge: ["Ass Shrink++++"],
		},
		/** HIPS */
		HIPS_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Hips, -50);},
			value: 50, knowledge: ["Hips Narrower+"],
		},
		HIPS_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Hips, -100);},
			value: 100, knowledge: ["Hips Narrower++"],
		},
		HIPS_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Hips, -200);},
			value: 200, knowledge: ["Hips Narrower+++"],
		},
		HIPS_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Hips, -400);},
			value: 400, knowledge: ["Hips Narrower++++"],
		},
		/** LIPS */
		LIPS_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Lips, -50);},
			value: 50, knowledge: ["Lips Thinner+"],
		},
		LIPS_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Lips, -100);},
			value: 100, knowledge: ["Lips Thinner++"],
		},
		LIPS_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Lips, -200);},
			value: 200, knowledge: ["Lips Thinner+++"],
		},
		LIPS_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Lips, -400);},
			value: 400, knowledge: ["Lips Thinner++++"],
		},
		/** PENIS */
		PENIS_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Penis, -50);},
			value: 50, knowledge: ["Penis Shrink+"],
		},
		PENIS_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Penis, -100);},
			value: 100, knowledge: ["Penis Shrink++"],
		},
		PENIS_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Penis, -200);},
			value: 200, knowledge: ["Penis Shrink+++"],
		},
		PENIS_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Penis, -400);},
			value: 400, knowledge: ["Penis Shrink++++"],
		},
		/** BALLS */
		BALLS_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Balls, -50);},
			value: 50, knowledge: ["Balls Shrink+"],
		},
		BALLS_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Balls, -100);},
			value: 100, knowledge: ["Balls Shrink++"],
		},
		BALLS_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Balls, -200);},
			value: 200, knowledge: ["Balls Shrink+++"],
		},
		BALLS_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Balls, -400);},
			value: 400, knowledge: ["Balls Shrink++++"],
		},
		/** WAIST */
		WAIST_REVERT_COMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, 50);},
			value: 50, knowledge: ["Waist Thicker+"],
		},
		WAIST_REVERT_UNCOMMON: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, 100);},
			value: 100, knowledge: ["Waist Thicker++"],
		},
		WAIST_REVERT_RARE: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, 200);},
			value: 200, knowledge: ["Waist Thicker+++"],
		},
		WAIST_REVERT_LEGENDARY: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Waist, 400);},
			value: 400, knowledge: ["Waist Thicker++++"],
		},
	});
}
