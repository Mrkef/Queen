namespace App.Data {
	Object.append(events, {
		IslaHarbor: [
			{
				id: 'BoobpireAttack',
				from: 'Any',
				maxRepeat: 0,
				minDay: 0,
				maxDay: 0,
				cool: 5,
				phase: [2, 3, 4],
				passage: 'BoobpireEvent',
				check: p => p.stat(Stat.Body, BodyPart.Bust) >= 50 && State.random() < 0.33,
			},
		],
	});
}
