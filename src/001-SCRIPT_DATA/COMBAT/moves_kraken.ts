namespace App.Data.Combat {
	moves[App.Combat.Style.Kraken] = {
		moves: {
			[App.Combat.Moves.Kraken.Grab]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 5,
				combo: 0, // Costs no combo points
				speed: 5,
				damage: 0.8,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME grabs at you, but misses!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME grabs you in a slimy grip!",
					],
				],
			},
			[App.Combat.Moves.Kraken.Strangle]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0, // Costs no combo points
				speed: 10,
				damage: 1.2,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tries to strangle you, but you break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME wraps around your neck and squeezes!",
					],
				],
			},
			[App.Combat.Moves.Kraken.Mouth]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0, // Costs no combo points
				speed: 20,
				damage: 1.5,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tries to jam into your mouth, but you break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME slides into your mouth and starts to throat fuck you!",
					],
				],
			},
			[App.Combat.Moves.Kraken.Ejaculate1]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0,
				speed: 20,
				damage: 0,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME starts to ejaculate into your throat, but you move your head and break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tenses up and starts to ejaculate into your mouth, pumping a thick stream of goo into your stomach!",
					],
				],
			},
			[App.Combat.Moves.Kraken.Ass]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0, // Costs no combo points
				speed: 20,
				damage: 1.5,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tries to jam into your asshole, but you break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME slides into your asshole and starts to violently fuck you!",
					],
				],
			},
			[App.Combat.Moves.Kraken.Ejaculate2]: {
				description: monsterAttackNoIntro,
				icon: "slash_icon",
				stamina: 10,
				combo: 0,
				speed: 20,
				damage: 0,
				unlock: () => true,
				miss: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME starts to ejaculate into your ass, but you shale your hips and break free!",
					],
				],
				hit: [
					[
						nonPlayerMoveErrorMessage,
						"NPC_NAME tenses up and starts to ejaculate into your ass, pumping a thick stream of goo into your bowels!",
					],
				],
			},
		},
	}
}
