namespace App.Data.Travel {
	export const disableAtLateNight: Data.Conditions.Expression = [{type: 'dayPhase', max: DayPhase.Night}];
}
