function makeReelData(baseTag: string, name: string, basePrice: number, css: string,
	data: Record<App.Items.Rarity, Partial<Record<App.Data.ReelAction, number>>>): Record<string, App.Data.ReelDesc> {
	const bTag = _.pascalCase(baseTag);
	const res: Record<string, App.Data.ReelDesc> = {};
	let price = basePrice;
	for (const [r, d] of Object.entries(data)) {
		const tag = r + bTag;
		res[tag] = {
			name, css,
			value: price,
			rank: r,
			data: d,
		};
		price = basePrice > 4000 ? price + basePrice : price * 2;
	}
	return res;
}

Object.append(App.Data.slots, makeReelData('Whore', "Sissy Whore Reel", 500, 'HandSlotReel',
	{
		common: {ass: 1, hand: 5, tits: 2, bj: 2},
		uncommon: {ass: 1, hand: 3, tits: 1, bj: 2, perv: 1},
		rare: {ass: 1, hand: 2, tits: 1, bj: 2, perv: 1, fem: 1},
		legendary: {ass: 1, hand: 2, tits: 1, bj: 2, beauty: 1, perv: 1, fem: 1},
	}
));

Object.append(App.Data.slots, makeReelData('AnalAngel', "The 'Anal Angel'", 1000, 'AssSlotReel',
	{
		common: {hand: 1, tits: 1, bj: 1, ass: 3}, // 50% Ass
		uncommon: {hand: 1, tits: 1, bj: 1, ass: 5}, // 63% Ass.
		rare: {hand: 1, tits: 1, bj: 1, ass: 6, beauty: 1}, // 70% Ass or Wild
		legendary: {hand: 1, tits: 1, bj: 1, ass: 7, beauty: 1, perv: 1}, // 75% Ass or Wild
	}
));

Object.append(App.Data.slots, makeReelData('BoobjitsuMatriarch', "The 'Boobjitsu Matriarch'", 1000, 'TitsSlotReel',
	{
		common: {ass: 1, tits: 2, bj: 1}, // 50% tits
		uncommon: {ass: 1, hand: 1, bj: 1, tits: 5}, // 5/8 tits
		rare: {ass: 1, hand: 1, bj: 1, tits: 6, beauty: 1}, // 7/10 Tits or Wild
		legendary: {ass: 1, hand: 1, bj: 1, tits: 7, beauty: 1, perv: 1}, // 3/4 Tits or Wild
	}
));

Object.append(App.Data.slots, makeReelData('BreathMint', "The 'Breath Mint'", 1000, 'BJSlotReel',
	{
		common: {ass: 1, hand: 1, tits: 1, bj: 3}, // 50% Bj
		uncommon: {ass: 1, hand: 1, tits: 1, bj: 5}, // 63% Bj.
		rare: {ass: 1, hand: 1, tits: 1, bj: 6, beauty: 1}, // 70% Bj or Wild
		legendary: {ass: 1, hand: 1, tits: 1, bj: 7, beauty: 1, perv: 1}, // 75% Bj or Wild
	}
));

Object.append(App.Data.slots, makeReelData('Concubine', "The 'Concubine'", 5000, 'FemSlotReel',
	{
		common: {ass: 1, bj: 1, hand: 1, tits: 1, fem: 4}, // 50% Fem
		uncommon: {ass: 1, bj: 1, hand: 1, tits: 1, fem: 5}, // 63% Fem
		rare: {ass: 1, bj: 1, hand: 1, tits: 1, fem: 6}, // 60% Fem
		legendary: {ass: 1, bj: 1, hand: 1, tits: 1, fem: 8}, // 66% Fem
	}
));

Object.append(App.Data.slots, makeReelData('Handmaiden', "The 'Handmaiden'", 1000, 'HandSlotReel',
	{
		common: {ass: 1, tits: 1, bj: 1, hand: 3}, // 50% hand
		uncommon: {ass: 1, tits: 1, bj: 1, hand: 5}, // 63% hand
		rare: {ass: 1, tits: 1, bj: 1, hand: 6, beauty: 1}, // 70% hand or wild
		legendary: {ass: 1, tits: 1, bj: 1, hand: 7, beauty: 1, perv: 1}, // 75% hand or wild
	}
));

Object.append(App.Data.slots, makeReelData('PervertedSlut', "The 'Freaky Slut'", 5000, 'PervSlotReel',
	{
		common: {ass: 1, tits: 1, bj: 1, hand: 1, perv: 4}, // 50% perv
		uncommon: {ass: 1, tits: 1, bj: 1, hand: 1, perv: 5}, // 55% perv
		rare: {ass: 1, tits: 1, bj: 1, hand: 1, perv: 6}, // 60% perv
		legendary: {ass: 1, tits: 1, bj: 1, hand: 1, perv: 8}, // 76% perv
	}
));

Object.append(App.Data.slots, makeReelData('Venus', "The 'Venus de Porno'", 5000, 'BeautySlotReel',
	{
		common: {ass: 1, tits: 1, bj: 1, hand: 1, beauty: 4}, // 50% beauty
		uncommon: {ass: 1, tits: 1, bj: 1, hand: 1, beauty: 5}, // 55% beauty
		rare: {ass: 1, tits: 1, bj: 1, hand: 1, beauty: 6}, // 60% beauty
		legendary: {ass: 1, tits: 1, bj: 1, hand: 1, beauty: 8}, // 66% beauty
	}
));

Object.append(App.Data.slots, makeReelData('Wildcard', "Wildcard Reel", 750, 'PervSlotReel',
	{
		common: {ass: 2, hand: 2, bj: 2, tits: 2, beauty: 1, fem: 1, perv: 1}, // 27% wildcard chance.
		uncommon: {ass: 3, hand: 3, bj: 3, tits: 3, beauty: 2, fem: 2, perv: 2}, // 33% wildcard chance
		rare: {ass: 2, hand: 2, bj: 2, tits: 2, beauty: 2, fem: 2, perv: 2}, // 42% wildcard chance
		legendary: {ass: 2, hand: 2, bj: 2, tits: 2, beauty: 3, fem: 3, perv: 3}, // 53% wildcard chance
	}
));
