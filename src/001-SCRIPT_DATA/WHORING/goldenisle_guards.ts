namespace App.Data {
	App.Data.whoring.GoldenIsleGuards = {
		// Shows in the UI as location.
		desc: "Golden Isle Guard House",
		// Sex act weighted table.
		wants: {hand: 2, ass: 4, bj: 4, tits: 3},
		// Minimum payout rank.
		minPay: 2,
		// Maximum payout rank.
		maxPay: 2,
		// Bonus payout, weighted table
		bonus: {
			body: {bust: 4, ass: 3, face: 2, lips: 2},
			style: {Bimbo: 2, PirateSlut: 1, SexyDancer: 3, SluttyLady: 2},
			stat: {[CoreStat.Perversion]: 3, [CoreStat.Femininity]: 2, beauty: 3},
		},
		names: names.Male,
		title: "Guard",
		rares: ["Justus"],
		npcTag: "GoldenIsleGuards",
	};

	App.Data.whoring.Justus = {
		// Shows in the UI as location.
		desc: "Golden Isle Guard House",
		// Sex act weighted table.
		wants: {ass: 1, bj: 1, tits: 1, hand: 1},
		// Minimum payout rank.
		minPay: 3,
		// Maximum payout rank.
		maxPay: 5,
		// Bonus payout, weighted table
		bonus: {
			body: {bust: 4, ass: 2, face: 2, lips: 1},
			style: {Bimbo: 2, SexyDancer: 2},
			stat: {[CoreStat.Perversion]: 2, beauty: 2, [CoreStat.Femininity]: 2},
		},
		names: ["Justus"],
		title: "Guard Cpt.",
		rares: [],
		npcTag: "Justus",
	};
}
