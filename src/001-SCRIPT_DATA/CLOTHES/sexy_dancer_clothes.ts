// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT
		"exotic wig": { // +12
			name: "exotic wig", shortDesc: "an exotic {COLOR} wig done in twin tails",
			longDesc: "This exotic wig is made of finely dyed human hair and strung with jewels.",
			slot: ClothingSlot.Wig, color: "lavender", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			hairLength: 80, hairStyle: Fashion.HairStyle.TwinTails, hairBonus: 90,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SexyDancer, Fashion.Style.SissyLolita, Fashion.Style.Bimbo],
		},
		// HAT SLOT

		// NECK SLOT

		// NIPPLES SLOT

		// BRA SLOT
		"nipple tassels": { // +15
			name: "nipple tassels", shortDesc: "a pair of red nipple tassels",
			longDesc: "These tassels can be pasted onto your nipples with a strong adhesive, allowing you to twirl and shake them as you gyrate. You can't wear them with a bra.",
			slot: ClothingSlot.Bra, color: "red", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"], style: [Fashion.Style.SexyDancer],
		},

		// CORSET SLOT

		// PANTY SLOT
		"sequined g-string": { // +15
			name: "sequined g-string", shortDesc: "a {COLOR} sequined g-string",
			longDesc: "It's more like a patch connected by dental floss. The main point, other than the extreme ass exposure, are the shiny silver sequins.",
			slot: ClothingSlot.Panty, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"], style: [Fashion.Style.SexyDancer],
		},

		// STOCKINGS SLOT
		"silk stockings": { // +10
			name: "silk stockings", shortDesc: "a pair of {COLOR} silk stockings and garters",
			longDesc: "Luxurious and sexy silk stockings held up with garters.",
			slot: ClothingSlot.Stockings, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SexyDancer, Fashion.Style.HighClassWhore, Fashion.Style.NaughtyNun, Fashion.Style.SluttyLady],
		},
		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT

		// COSTUME SLOT
		"stripper costume": { // +20
			name: "stripper costume", shortDesc: "a tiny {COLOR} naval uniform",
			longDesc: "This costume consists of a two piece outfit with accessories that is designed to mimic a navy officers uniform, if the navy was a service filled with " +
				"brazen whores. The skirt is flared and too low, showing off your bottom. The top is tight and mostly open, it has patches that can be pulled off to reveal tassels hanging " +
				"off your tits from pasties that are shaped like anchors. Instead of sleeves or a collar it has little accessories designed to look like them, including the epaulets of the navy " +
				"suit jacket.",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "white", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"], activeEffect: ["MINOR_STRIPPERS_ALLURE"], style: [Fashion.Style.SexyDancer],
		},

		"burlesque costume": { // +30
			name: "burlesque costume", shortDesc: "a fancy {COLOR} burlesque costume",
			longDesc: "This two piece red outfit is designed to be taken off in pieces. With the accompanying giant fan and feather boa, you're sure to put on an excellent show.",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "red", rarity: Items.Rarity.Rare, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"], activeEffect: ["MAJOR_STRIPPERS_ALLURE"], style: [Fashion.Style.SexyDancer],
		},

		"slutty schoolgirl": { // +40
			name: "slutty schoolgirl", shortDesc: "skimpy school uniform",
			longDesc: "This costume consists of a tiny shirt tied over your breasts, exposing your midriff, a high-waisted school jacket and a tartan skirt so small it's barely there.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "blue", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING", "KINKY_CLOTHING"], activeEffect: ["GREATER_STRIPPERS_ALLURE"], style: [Fashion.Style.SexyDancer],
		},

		// SHOES SLOT
		"go-go boots": { // +10
			name: "go-go boots", shortDesc: "a pair of {COLOR} platform go-go boots",
			longDesc: "These knee high boots sport an elevated platform and a chunky heel. Perfect for strutting your stuff and getting down on the dance floor.",
			slot: ClothingSlot.Shoes, restrict: [ClothingSlot.Shoes], color: "white", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], activeEffect: ["FANCY_MOVES"], style: [Fashion.Style.Bimbo, Fashion.Style.SexyDancer],
		},

		"stripper heels": { // +20
			name: "stripper heels", shortDesc: "a pair of outrageously tall {COLOR} platform heels",
			longDesc: "\
    These type of shoes are commonly worn by exotic dancers or women who want to make themselves the center \
    of attention. The thick platform on the bottom is outrageously tall, giving you several inches on your \
    normal height. They're also so heavy that it takes practice care to walk with them, more so than regular \
    heels. The effect is that you prance and waddle while you step, forcing your ass to jiggle and shake \
    everywhere you walk.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["REALLY_FANCY_MOVES"],
			style: [Fashion.Style.SexyDancer, Fashion.Style.Bimbo],
			inMarket: false,
		},

		"barefoot sandals": {
			name: "barefoot sandals", shortDesc: "a pair of {COLOR} barefoot sandals",
			longDesc: "These ornamental chains elegantly embellish your dainty effeminate feet without appreciable weight or concealment.",
			slot: ClothingSlot.Shoes, color: "silver", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"], activeEffect: ["MINOR_STRIPPERS_ALLURE"], style: [Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// BUTT SLOT

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
