// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"gimp mask": { // +12
			name: "gimp mask", shortDesc: "{COLOR} leather and metal strapped gimp mask",
			longDesc: "\
    This full head mask is designed to be zipped up and locked over it's wearer. There is a flap for both the mouth \
    and eyes, but they can be closed, or in the case of the mouth have various accessories locked into it.\
    ",
			slot: ClothingSlot.Hat,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.Bdsm],
			inMarket: false,
		},

		// NECK SLOT
		collar: { // +3
			name: "collar", shortDesc: "a leather slave collar",
			longDesc: "This thick leather collar is worn to remind everyone that the wearer is a slave.",
			slot: ClothingSlot.Neck, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Accessory, locked: true,
			wearEffect: ["SLAVE_COLLAR"], style: [Fashion.Style.Bdsm],
		},

		// NIPPLES SLOT
		"nipple rings": { // +9
			name: "nipple rings", shortDesc: "a pair of silver nipple rings joined by a chain",
			longDesc: "These rings are connected by a chain that is designed to torment and tug on the wearers nipples.",
			slot: ClothingSlot.Nipples, color: "silver", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING"], style: [Fashion.Style.Bdsm],
		},

		"nipple clamps": { // +6
			name: "nipple clamps", shortDesc: "a pair of silver nipple clamps",
			longDesc: "These sterling silver nipple clamps are designed to inflict punishment… or perhaps arousal?",
			slot: ClothingSlot.Nipples, color: "silver", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING"], style: [Fashion.Style.Bdsm],
		},

		// BRA SLOT

		// CORSET SLOT
		"training corset": { // +10
			name: "training corset", shortDesc: "a whale boned {COLOR} training corset",
			longDesc: "This garment borders on a torture device. It's designed to slim and shape a woman's waist and push up her bust.",
			slot: ClothingSlot.Corset, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "WAIST_TRAINING"], style: [Fashion.Style.Bdsm],
		},

		"bondage corset": { // +15
			name: "bondage corset", shortDesc: "a steel boned {COLOR} leather bondage corset",
			longDesc: "This corset is made of strong leather and boned with steel. It has various straps and locks to ensure it must be worn at all times.",
			slot: ClothingSlot.Corset, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "EXTREME_WAIST_TRAINING"], style: [Fashion.Style.PirateSlut, Fashion.Style.Bdsm],
		},

		// PANTY SLOT

		// STOCKINGS SLOT

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"bondage dress": { // +40
			name: "bondage dress", shortDesc: "leather {COLOR} bondage dress",
			longDesc: "\
    This shiny leather dress has a low cutout in the back that displays a generous portion of ass cleavage \
    and a neckline that could barely contain even the most modest of busts. It is adorned with a variety of \
    straps that look like belts until you realize that they can be used to make the person wearing the \
    dress completely immobile.\
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.Bdsm],
			inMarket: false,
		},

		// COSTUME SLOT
		"rubber nurse outfit": { // +40
			name: "rubber nurse outfit", shortDesc: "{COLOR} rubber nurse's outfit",
			longDesc: "\
    This costume is designed to mimic a stanard nurse's uniform, except it's made entirely out of a thin type \
    of black rubber that stretches and confines the body. The outfit has red piping along the hems and two red \
    crosses, on each breast.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["PERVERTED_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.Bdsm, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// SHOES SLOT
		"shiny knee boots": { // +10
			name: "shiny knee boots", shortDesc: "a shiny pair of high heeled knee-high {COLOR} leather boots",
			longDesc: "These fashionable leather boots are polished to a shine. Very tall and slightly intimidating.",
			slot: ClothingSlot.Shoes, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.PirateSlut, Fashion.Style.Bdsm],
		},

		"spiked boots": { // +20
			name: "spiked boots", shortDesc: "{COLOR} leather spiked platform boots",
			longDesc: "\
    These high heeled platform boots don't look like they are designed to walk in. The impractically high platform \
    weighs enough to make taking a step precarious and the height means that if you fall, you'll take some serious \
    damage. To make matters worse, the boots themselves are adorned with various studs and spikes meaning that it's \
    quite possible to carelessly injure yourself. Perfect for a budding masochist.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.Bdsm, Fashion.Style.SexyDancer],
			inMarket: false,
		},

		// BUTT SLOT
		"large plug": { // +9
			name: "large plug", shortDesc: "a thick {COLOR} anal plug",
			longDesc: "The perfect accessory for the horny masochist, or just good for keeping your arse ready to be used.",
			slot: ClothingSlot.Butt, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING", "GAPE_TRAINING"], style: [Fashion.Style.Bdsm],
		},

		"voodoo anal plug": { // +9
			name: "voodoo anal plug", shortDesc: "an enormous rubbery anal plug",
			longDesc: "\
    This giant plug was hand crafted just for your sissy-hole. It has a special feature that causes it to \
    gradually expand while worn. It also secretes a concentrated essence of various chemical and herbal \
    extracts to promote a truly fuckable arse.\
    ",
			slot: ClothingSlot.Butt, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			style: [Fashion.Style.Bdsm], inMarket: false,
			wearEffect: ["VOODOO_ANAL_PLUG"], activeEffect: ["VOODOO_ANAL_PLUG2"],
		},

		// PENIS SLOT
		"chastity cage": { // +3
			name: "chastity cage", shortDesc: "a metal and leather chastity device",
			longDesc: "This cruel device constricts and crushes your genitals. Wearing it is not only painful but probably bad for your health.",
			slot: ClothingSlot.Penis, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Accessory, locked: true,
			wearEffect: ["CHASTITY_CAGE"], style: [Fashion.Style.Bdsm],
		},

		"magic cock ring": { // +3
			name: "magic cock ring", shortDesc: "<span style='color:purple'>a shiny metal cock ring</span>",
			longDesc: "\
    This shiny silver ring was made custom order from a rare magical ore. It supposedly has the ability to \
    protect your genitals from curses and other magical effects.",
			slot: ClothingSlot.Penis, color: "silver", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory, locked: false,
			wearEffect: ["MAGIC_COCK_RING"], style: [Fashion.Style.Bdsm], inMarket: false,
		},

		// WEAPON SLOT (huh?)
	});
}
