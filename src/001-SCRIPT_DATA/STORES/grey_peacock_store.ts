namespace App.Data {
	stores.peacock = {
		name: "The Grey Peacock", open: [1, 2, 3, 4], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 8, max: 8, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "flip"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "birch wine"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "buttered toddy"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "whip syllabub"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "cordial waters"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "orgeat"},
		],
	};
}
