/* eslint-disable @typescript-eslint/no-unused-vars */
declare namespace App.Data {
	type SkillBonusData = Partial<Record<Skills.Any, [number, number, number]>>;

	// TODO there is a little mess with hair related stuff: there are only 2 types in game, but we use 3 identifiers
	const enum CosmeticsType {
		HairTool = "hair tool",
		HairProducts = "hair products",
		HairTreatment = "hair treatment",
		BasicMakeup = "basic makeup",
		ExpensiveMakeup = "expensive makeup"
	}

	//#region Items
	const enum ItemTypeConsumable {
		Food = "food",
		Potion = "potion",
		MiscConsumable = "misc",
		LootBox = "lootBox",
		MiscLoot = "miscLoot"
	}

	const enum ItemTypeSpecial {
		Quest = "quest",
		Reel = "reel"
	}

	type ItemTypeInventory = ItemTypeConsumable | ItemTypeSpecial | CosmeticsType;

	type ItemTypeEquipment = Items.ClothingType | Items.Category.Weapon;
	type ItemType = ItemTypeInventory | ItemTypeEquipment;
	type ItemTypesShop = Exclude<ItemType, ItemTypeSpecial.Quest>;

	interface ItemDesc {
		name: string;
		inMarket?: boolean;
		quest?: boolean;
		value?: number;
		priceCoefficient?: number;
	}

	interface InventoryItemDesc<TType extends ItemType> extends ItemDesc {
		shortDesc: string;
		longDesc: string;
		type: TType;
	}

	interface ConsumableItemDesc<TType extends ItemType> extends InventoryItemDesc<TType> {
		message?: string;
		/**
		 * Number of uses that are sold in a single item
		 * @default 1
		 */
		charges?: number;
		effects?: string[];
	}

	type DrugItemDesc = ConsumableItemDesc<ItemTypeConsumable.Potion>;
	type FoodItemDesc = ConsumableItemDesc<ItemTypeConsumable.Food>;
	type MiscConsumableItemDesc = ConsumableItemDesc<ItemTypeConsumable.MiscConsumable>;
	type MiscLootItemDesc = ConsumableItemDesc<ItemTypeConsumable.MiscLoot>;
	type LootBoxItemDesc = ConsumableItemDesc<ItemTypeConsumable.LootBox>;
	type QuestItemDesc = InventoryItemDesc<ItemTypeSpecial.Quest>;

	interface CosmeticsItemDesc extends ConsumableItemDesc<CosmeticsType> {
		skillBonus: SkillBonusData;
	}

	type HairColor = "black" | "blond" | "brown" | "red" | "lavender";

	interface ClothingItemDesc extends InventoryItemDesc<Items.ClothingType> {
		slot: ClothingSlot;
		restrict?: ClothingSlot[];
		color: string;
		rarity: Items.Rarity;
		style?: Fashion.Style[];
		wearEffect?: string[];
		activeEffect?: string[];
		inMarket?: boolean;
		locked?: boolean;
		meta?: string[];
	}

	interface WigItemDesc extends ClothingItemDesc {
		slot: ClothingSlot.Wig;
		hairLength: number; // for wigs
		hairStyle: Data.Fashion.HairStyle; // for wigs
		hairBonus: number; // for wigs
	}

	type ReelWildcard = "beauty" | "fem" | "perv";
	type ReelAction = Whoring.SexActStr | ReelWildcard;

	interface ReelDesc extends ItemDesc {
		rank: Items.Rarity;
		css: string;
		data: Partial<Record<ReelAction, number>>;
	}

	type InventoryItemDescAny = DrugItemDesc | FoodItemDesc |
		MiscConsumableItemDesc | LootBoxItemDesc | QuestItemDesc |
		CosmeticsItemDesc;

	type EquipmentItemDescAny = ClothingItemDesc | WigItemDesc;
	type ItemDescAny = InventoryItemDescAny | EquipmentItemDescAny;
	//#endregion

	interface BeautyProcedureDesc {
		difficulty: number;
		style: number;
		short: string;
	}

	interface HairStyleDesc extends BeautyProcedureDesc {
		resource1: number; // RES1 = ACCESSORIES, RES2 = PRODUCT
		resource2: number;
		min: number;
		max: number;
	}

	interface MakeupStylesDesc extends BeautyProcedureDesc { // RES1 = BASIC MAKEUP, RES2 = EXPENSIVE MAKEUP
		resource1: number;
		resource2: number;
	}

	interface ItemTypeDescMap {
		[Items.Category.Drugs]: DrugItemDesc;
		[Items.Category.Food]: FoodItemDesc;
		[Items.Category.Cosmetics]: CosmeticsItemDesc;
		[Items.Category.MiscConsumable]: MiscConsumableItemDesc
		[Items.Category.MiscLoot]: MiscLootItemDesc;
		[Items.Category.Clothes]: ClothingItemDesc | WigItemDesc;
		[Items.Category.Weapon]: ClothingItemDesc;
		[Items.Category.Quest]: QuestItemDesc;
		[Items.Category.LootBox]: LootBoxItemDesc;
		[Items.Category.Reel]: ReelDesc;
	}

	type ItemCategoryAny = keyof ItemTypeDescMap;
	type ItemNameTemplate<T extends keyof ItemTypeDescMap> = `${T}/${string}`;
	type ItemNameTemplateAny = ItemNameTemplate<ItemCategoryAny>;
	type ItemNameTemplateConsumable = ItemNameTemplate<keyof Items.ConsumableItemTypeMap>;
	type ItemNameTemplateClothing = ItemNameTemplate<Items.Category.Clothes>;
	type ItemNameTemplateEquipment = ItemNameTemplate<Items.Category.Clothes | Items.Category.Weapon>;
	type ItemNameTemplateInventory = ItemNameTemplate<keyof Items.InventoryItemTypeMap>;

	export type ItemNameTemplateToItemCategory<T extends Data.ItemNameTemplateAny> =
		T extends ItemNameTemplate<Items.Category.Drugs> ? Items.Category.Drugs :
		T extends ItemNameTemplate<Items.Category.Food> ? Items.Category.Food :
		T extends ItemNameTemplate<Items.Category.Cosmetics> ? Items.Category.Cosmetics :
		T extends ItemNameTemplate<Items.Category.MiscConsumable> ? Items.Category.MiscConsumable :
		T extends ItemNameTemplate<Items.Category.MiscLoot> ? Items.Category.MiscConsumable :
		T extends ItemNameTemplate<Items.Category.LootBox> ? Items.Category.LootBox :
		T extends ItemNameTemplate<Items.Category.Quest> ? Items.Category.Quest :
		T extends ItemNameTemplate<Items.Category.Reel> ? Items.Category.Reel :
		T extends ItemNameTemplate<Items.Category.Clothes> ? Items.Category.Clothes :
		T extends ItemNameTemplate<Items.Category.Weapon> ? Items.Category.Weapon :
		never;

	//#region Effects
	type EffectAction = (human: Entity.Human) => string | void;
	type ItemEffectAction<T extends Items.IBaseItem> = (h: Entity.Human, o: T) => string | void;

	interface EffectDescBase {
		value: number;
		knowledge: string[];
	}

	interface Effect<TAction> extends EffectDescBase {
		fun: TAction;
	}

	interface StatChangeEffectOption {
		xp?: number;
		value?: number;
	}
	type StatChangeEffect = AtLeastOne<StatChangeEffectOption>  & {
		limiter?: number;
	}
	export interface EffectStatChanges {
		core?: Partial<Record<CoreStat, StatChangeEffect>>;
		body?: Partial<Record<BodyStat, StatChangeEffect>>;
		skill?: Partial<Record<Skills.Any, StatChangeEffect>>;
		value: number;
	}

	type EffectDesc = Effect<EffectAction>;
	type EquipmentWearEffectDesc = Effect<ItemEffectAction<Items.IEquipmentItem>>;

	type VirtualSkillStr = 'sharpness' | 'damageResistance';
	type WearSkill = Skills.AnyStr | VirtualSkillStr | CoreStatStr | BodyStatStr | DerivedBodyStatStr;
	type WearEffectAction = (s: WearSkill, o: Items.IEquipmentItem) => number;
	type EquipmentActiveEffectDesc = Effect<WearEffectAction>;
	//#endregion

	//#region Loot boxes
	type ItemFilter<T extends keyof Data.ItemTypeDescMap> = (
		ds: Items.ItemDescDictionary<T>, category: T, pool: number) => Items.ItemDescDictionary<T>;
	interface LootTableItem {
		type: Items.Category | "coins";
		chance: number;
		min?: number;
		max?: number;
		maxCount?: number;
		free?: boolean;
		filter?: ItemFilter<Items.Category>;
	}

	interface LootItem {
		category: Items.Category;
		tag: string;
		min: number;
		max: number;
	}
	//#endregion
}

/* eslint-enable @typescript-eslint/no-unused-vars */
