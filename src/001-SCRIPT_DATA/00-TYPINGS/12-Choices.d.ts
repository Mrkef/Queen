declare namespace App.Data {
	export namespace Choices {
		type DynamicValue<T, Arguments extends readonly unknown[]> = T | ((...args: [...Arguments]) => T);
		export interface Choice<Context> {
			/**
			 * The choice is completely hidden from the player if false
			 * @default true
			 */
			available?: Conditions.Expression;
			/**
			 * The choice is visible but not selectable if false
			 * @default true
			 */
			enabled?: Conditions.Expression;

			caption?: DynamicValue<string, [Context]>;
			/** Optional tooltip */
			hint?: DynamicValue<string, [Context]>;
			/** Action that is performed when the choice is selected */
			action?: Actions.SingleAny[] | ((ctx: Context) => void);
		}
	}
}
