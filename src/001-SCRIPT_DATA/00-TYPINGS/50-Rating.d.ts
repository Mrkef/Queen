declare namespace App.Data {
	export type DynamicRatingText = string | ((object: App.Text.ActorHuman, viewer: App.Text.ActorHuman) => string);
}
