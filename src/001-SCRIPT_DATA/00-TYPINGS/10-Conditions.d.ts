declare namespace App.Data {
	export namespace Conditions {
		/**
		 * null is a special case meaning no comparison is performed, it always returns true
		 */
		type EqualityConditionMain = "==" | "!=" | boolean | null;
		type EqualityConditionAlternate = "eq" | "ne";
		type EqualityCondition = EqualityConditionMain | EqualityConditionAlternate;

		type ConditionMain = ">=" | "<=" | ">" | "<" | EqualityConditionMain;
		type ConditionAlternate = "gte" | "lte" | "gt" | "lt" | EqualityConditionAlternate;
		type Condition = ConditionMain | ConditionAlternate;

		type ValueComparison<T> = T extends number ? Condition : EqualityCondition;

		interface Base<Type extends string> {
			type: Type;
			/**
			 * Replaces auto-generated string when displaying this condition to user.
			 * The special value @null makes the condition permanently invisible
			 */
			altTitle?: string | null;
			/** Do not show this in the requirements until the test returns true
			 * @default false
			 */
			hidden?: boolean;
			/**
			 * Result value of the test will be saved, tagged with the provided key
			 */
			tag?: string;
		}

		interface ValueRequired<T> {
			value: T;
		}

		type ValueOptional<T> = Partial<ValueRequired<T>>;

		interface ValueCondition<T, TCondition = ValueComparison<T>> {
			condition?: TCondition;
		}

		type ValueBase<T, TCondition = ValueComparison<T>, TValue = ValueRequired<T>> = TValue & ValueCondition<T, TCondition>;

		interface ValueNumeric<TCondition = Condition> extends ValueBase<number, TCondition> {
			/**
			 * [-1,1]. Modifies target value by a random number, multiplied by this coefficient
			 * @default 0
			 */
			randomShift?: number;
		}

		type Value<T, TCondition = ValueComparison<T>> = T extends number ? ValueNumeric<TCondition> : ValueBase<T, TCondition>;

		type BaseProperty<
			Type extends string, TMap, TProperty extends keyof TMap
		> = Base<Type> & Value<TMap[TProperty] | ReadonlyArray<TMap[TProperty]>> & {
			property: TProperty;
		}

		interface ValueLimits<T> { // TODO: consider replacesing conditions with the range
			min?: T;
			max?: T;
		}

		interface Named<TName extends string> {
			name: TName;
		}

		type NamedValue<Type extends string, TName extends string, T> = Base<Type> & Named<TName> & Value<T>;
		type NamedNumericValue<Type extends string, TName extends string> = Base<Type> & Named<TName> & ValueNumeric;

		type NamedProperty<Type extends string, TMap, TProperty extends keyof TMap = keyof TMap> =
			BaseProperty<Type, TMap, TProperty> & Partial<Named<string>>;

		type SimpleValue<TType extends string, T> = Base<TType> & Value<T>;
		type SimpleNumeric<TType extends string> = Base<TType> & ValueNumeric;

		interface Character {
			/**
			 * Character name to compute test for
			 * @default "pc"
			 */
			character?: string;
		}

		interface HairType {
			hairType?: 'natural' | 'wig';
		}

		type NamedNumericCharacter<Type extends string, TName extends string> = NamedValue<Type, TName, number> & Character;

		interface StatSkillCheck<TType extends string, TName extends string> extends NamedNumericCharacter<TType, TName> {
			raw?: true;
			noXp?: true;
		}

		type Stat = StatSkillCheck<Stat.Core, CoreStat>;
		type Skill = StatSkillCheck<Stat.Skill, Skills.Any>;
		type Body = StatSkillCheck<Stat.Body, BodyStat>;
		type Meta = StatSkillCheck<'meta', "beauty" | "style" | "danceStyle" | "obviousTrappiness">;

		type Money = SimpleNumeric<"money">;
		type Tokens = SimpleNumeric<"tokens">;

		interface Random extends SimpleNumeric<"random"> {
			factor?: number;
			round?: "floor" | "ceil" | "round";
		}

		// type BoolConst = SimpleValue<"boolConst", boolean>;

		interface NPCStat extends NamedNumericValue<"npcStat", NpcStat> {
			option?: string;
		}

		type DaysPassed = SimpleNumeric<"daysPassed">;

		type DayPhase = Base<"dayPhase"> & RequireAtLeastOne<ValueLimits<App.DayPhase>>;

		interface TaskProperties {
			giver: string;
		}

		interface QuestProperties extends TaskProperties {
			status: QuestStatus;
			/** Relative to today, so today is 0, yesterday is 1, etc. */
			acceptedFor: number;
			completedFor: number;
			acceptedOn: number;
			completedOn: number;
		}

		interface JobProperties extends TaskProperties {
			available: boolean;
			lastCompletedFor: number;
			timesCompleted: number;
		}

		type GameVariableBase<T extends string, V extends GameVariable> = NamedValue<T, string, V> & VariableScope.Scope;

		type Quest = NamedProperty<"quest", QuestProperties>;
		type Job = NamedProperty<'job', JobProperties>;
		/**
		 * Test tagged check result (percents divided by 100 or the raw value) against provided value, which defaults to 1
		 */
		type Tag = Base<"tag"> & Named<string> & ValueBase<number, ValueComparison<number>, ValueOptional<number>> & {
			raw?: boolean;
		};

		type Variable = GameVariableBase<'var', GameVariable>;
		type VariableBoolean = GameVariableBase<'var:b', boolean>;
		type VariableNumeric = GameVariableBase<'var:n', number>;
		type VariableString = GameVariableBase<'var:s', string>;

		type Item = NamedNumericValue<"item", ItemNameTemplateAny> & Character;

		type Equipped = NamedValue<"equipped", ItemNameTemplateEquipment, boolean> & Character;

		interface NameOrSLot {
			slot?: ClothingSlot;
			name?: ItemNameTemplateEquipment;
		}

		type IsWearingBase = SimpleValue<"isWearing", boolean> & {
			isLocked?: boolean;
		}

		type IsWearing = IsWearingBase & RequireAtLeastOne<NameOrSLot> & Character;

		type StyleCategory = NamedNumericValue<"styleCategory", keyof typeof Data.Fashion.Style> & Character;
		type HairStyle = NamedValue<"hairStyle", string, boolean> & Character & HairType;
		type HairColor = NamedValue<"hairColor", Data.HairColor, boolean> & Character & HairType;
		type TrackProgress = NamedNumericValue<"trackCustomers" | "trackProgress", string> & VariableScope.Scope;

		type PortName = NamedValue<"portName", string, boolean>;
		type InPort = Base<"inPort"> & ValueNumeric<boolean>;

		type StatisticsOp = "min" | "max" | "mean" | "median" | "sum" | "product";

		type Statistics = SimpleNumeric<'statistic'>  & {
			op: StatisticsOp;
			tags: NonEmptyArray<string>;
		}

		type EventCount = NamedNumericValue<"eventCount", string>;

		export type Any = Body | Stat | Skill |
			Meta | Money | Tokens |
			NPCStat | DaysPassed | Quest | Job |
			EventCount |
			Variable | VariableBoolean | VariableNumeric | VariableString |
			Item | Equipped | IsWearing | StyleCategory |
			HairStyle | HairColor |
			PortName | InPort | TrackProgress | DayPhase |
			// BoolConst |
			Random | Tag | Statistics;

		type UnaryOp = '!';
		type BinaryOp = '||' | '&&';

		type Defaults = Partial<MapDiscriminatedUnionToOptions<Any, 'type'>>;
		interface ExplicitExpression {
			/** @default '&&' */
			op?: BinaryOp;
			predicate?: UnaryOp;
			title?: string;
			args: ReadonlyArray<Any | ExplicitExpression>;
		}

		type ImplicitAndExpression = ReadonlyArray<ExplicitExpression | Any>;

		export type Expression = ImplicitAndExpression | ExplicitExpression;
	}
}
