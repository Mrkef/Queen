namespace App.Data {
	Object.append(quests, {
		STRIPPING: {
			title: "The Casting Couch",
			giver: "Cook",
			pre: [{type: "quest", name: "BUILD_A_BIMBO1", property: 'status', value: QuestStatus.Completed}],
			checks: [
				{type: Stat.Skill, name: Skills.Charisma.Dancing, value: 20, condition: "gte"},
				{type: Stat.Core, name: CoreStat.Fitness, value: 30, condition: "gte"},
				{type: Stat.Core, name: CoreStat.Femininity, value: 30, condition: "gte"},
				{type: Stat.Skill, name: Skills.Charisma.Seduction, value: 30, condition: "gte"},
			],
			reward: [
				{type: "item", name: "clothes/stripper costume", value: 1},
				{type: "item", name: "cosmetics/hair accessories", value: 20},
				{type: "item", name: "cosmetics/hair products", value: 20},
				{type: "item", name: "cosmetics/expensive makeup", value: 30},
			],
			intro: "\
        NPC_NAME says, s(Nice to see you're putting some meat on those bones PLAYER_NAME.) \
        He eyes you up and down, his gaze lingering on your tits and ass. s(And I gotta say, it's all going to \
        the right places too.)\n\n\
        You know what NPC_NAME is talking about - the rare food and drugs on board the \
        <span class='npc'>Salty Mermaid</span> have started to make dramatic alterations to your body, \
        right down to a hormonal level. You'd almost wouldn't believe such dramatic changes were possible, but \
        the pBUST tits you are dragging around beg otherwise. If only you could find some more wholesome source \
        of nourishment… but maybe it's too late? NPC_NAME interrupts these thoughts and continues speaking.\n\n\
        s(If you feel like earning some extra coin, then you might consider putting on a show.) He motions across \
        the Galley dining room to a small raised dias against the aft wall.\n\n\
        NPC_NAME says, s(We used to have a proper slut on crew and she'd put on the best stripping and burlesque \
        shows for the boys in the evening. It'd really drive them wild and make horny as hell. You might think \
        about doing the same.)\n\n\
        As humiliating as it sounds, being a dancing girl is probably a step up from a whore. Still, chances are \
        with the way you are looking these days there isn't really that much of a difference. Still, you wonder \
        if the tips would be good…\n\n\
        NPC_NAME says, s(You can use the stage to practice in the mornings and when you think you've got a decent \
        routine come back and see me. I'll give you an 'audition' and if you pass then I'll let you perform during \
        the evenings for some extra coin. You may also want to think about getting in better shape - although your \
        dance practice might help with that you could also do with some more physical labor. Maybe \
        something down in the cargo hold?)\
        ",
			middle: "\
        NPC_NAME says, s(Don't take it the wrong way sweet tits, but I don't think you have the right 'stuff' \
        yet in order to make it on stage. Keep working on it and come back.)\
        ",
			finish: "\
        You approach NPC_NAME and request that he give you your 'audition' for the role of the only \
        sissy-stripper on board the <span class='npc'>Salty Mermaid</span>. NPC_NAME and half of the kitchen \
        staff take up seats around the Galley as you get on the stage. One of the cooks, an older man with a \
        strange hat begins to play a raunchy tune on an even older piano that has been recently rolled into place. \
        You start off your gyrations a little awkwardly, trying to get in tune with the music. Each time it comes \
        up to an exclamation, you make sure to stick out either your pBUST tits or nASS with a shake and a \
        jiggle to put extra emphasis on them. As the song progresses you start taking off more and more clothes, \
        trying to do it in a flirty and teasing way, smiling and winking at the audience. By now the men are \
        clapping their hands and hooting as you are gyrating your pBUST tits at them, each nipple has a small \
        tassel firmly attached to it and you are swinging them counter clockwise in an alternating motion. \
        Everyone is really getting into the show and although you're still quite the novice you feel that you \
        have a good grasp of how this should work. You finish off by bending over, your pBUST tits dangling \
        towards the floor and simulating getting butt fucked in tempo to the beat of the music. As you grope and \
        fondle yourself and let out a series of moans, the audience is getting increasingly agitated. It's no \
        mistake that they all want to fuck you right now.\n\n\
        You finish the show with a wink and a blown kiss to the crowd and they all applaud as you fetch your \
        discarded clothing. NPC_NAME comes up to you and says, s(Well done PLAYER_NAME. I had my doubts when I \
        first saw you, but you're going to be a fine slut for this crew. Here take these, they'll help you \
        perform better on stage… oh and by the way, you now can come here and put on shows during the evenings \
        every day. Just be careful, you might drive the men so wild that they'll fuck your asshole inside out!)\
        ",
			journalEntry: "\
        NPC_NAME has told you that you can s!(use the stage in the Galley to practice your dancing in the mornings). \
        If you improve your abilities enough then you will be able to put on shows in the evening for tips.\
        ",
			journalComplete: "\
        Your 'audition' for the cooking crew was a wild success. You bumped and wiggled yourself into their \
        hearts, or at least you made their dicks hard for you. From now on you can perform on the stage during \
        the evenings for extra coin and to raise the lust of the crew.\
        ",
		},

		"FEMINITY_BOOST:COOK_HELP": {
			title: "Boost your feminity with right dieting",
			giver: "Cook",
			pre: [
				{type: "daysPassed", value: 63, condition: "gte"},
				{type: Stat.Body, name: BodyPart.Bust, value: 3, condition: "lte"},
				{type: "npcStat", name: NpcStat.Mood, value: 80, condition: "gte"},
				{type: "quest", name: "BUILD_A_BIMBO1", property: 'status', value: QuestStatus.Completed, condition: "ne"},
				{type: Stat.Skill, name: Skills.Domestic.Cooking, value: 30, condition: "gte"},
			],
			onAccept: [{type: "trackProgress", scope: {type: "job", id: "GALLEY_WORKFORCE"}, name: "GALLEY_WORKFORCE_DAYS", value: 0, op: "set"}],
			intro: "s(So, PLAYER_NAME) says NPC_NAME, s(you are sailing with us for more than a month already, yeah?)\n\
		sp(More than two months, actually…)\n\
		s(Even so… And you were supposed to develop certain female traits and features, yet I can't see it happens. \
		If that is due to a bad luck or something, I can lend you a hand… in a certain way.\n \
		You should have discovered already that there are diets to help you with the task, so here is the deal: \
		you work here for 7 full days, and after that I provide you with that kind of special food you need for your diet?)",
			middle: "s(The deal was about seven full days, no less, PLAYER_NAME)",
			checks: [{type: "trackProgress", scope: {type: "job", id: "GALLEY_WORKFORCE"}, name: "GALLEY_WORKFORCE_DAYS", altTitle: "Galley full working days", value: 7, condition: "gte"}],
			finish: "s(Fine, PLAYER_NAME) says NPC_NAME, s(I hoped for more, but seems line I should get back to work. \
		Here is the promised reward. Made it specially for you, ha ha.)",
			reward: [
				{type: "item", name: "food/peaches and cream", value: 8},
				{type: "item", name: "food/butter gourd pie", value: 8},
			],
			journalEntry: "NPC_NAME has offered you food supplies that suit your 'diet' in exchange of seven full days of \
		hard work at the galley",
			journalComplete: "You worked hard at the galley for seven full days, that left no time for any other activity. \
		In reward NPC_NAME gave you a dozen and a half of pies and deserts, which he claimed he baked himself. Does \
		a week of hard work worth a dozen of sweets?",
		},
	});
}
