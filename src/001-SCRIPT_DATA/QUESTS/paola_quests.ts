namespace App.Data {
	Object.append(quests, {
		PAOLA_FETCH: {
			title: "Voodoo Supplies",
			giver: "Paola",
			checks: [{type: "item", name: "quest/bag of mojo", value: 1, condition: "gte"}],
			onAccept: [{type: "quest", name: "VOODOO_PICKUP", value: "start"}],
			reward: [{type: "money", value: 200}],
			intro: "\
        NPC_NAME laughs heartily as you approach. s(You be seekin' the great powah of NPC_NAME eh???)\n\n\
        You're not sure if that statement is true or not, but… maybe? The strange brown woman continues her speech.\n\n\
        s(Ah well, I can't be doin' mah hexin' until I restock mah mojo, ya ken wot I mean?)\n\n\
        You're not actually sure what she means, but you suspect that if you were to bring her some 'mojo' she might be able \
        to do some magic for you?\n\n\
        NPC_NAME says s(Find that white bitty @@.npc;Blanche@@ in @@.location-name;Port Royale@@ and get me my mojo!)",
			middle: "You need to bring the following items to NPC_NAME:",
			finish: "\
        NPC_NAME says s(Thanks be PLAYER_NAME! I be scraping da bottem o' da barrel, but now with dis, mah mojo is back!)\n\n\
        She throws back her head and cackles to herself, her massive mammaries shaking like two giant globes of jello.\n\n\
        NPC_NAME says s(If yee be wantin' the gifts o' the spirits, let me know and I'll be castin' dem hexes full on ya bet!)",
			journalEntry: "\
        NPC_NAME on @@.location-name;Abamond@@ wants your help in replenishing her 'mojo', whatever that means. Supposedly someone \
        named @@.npc;Blanche@@ in @@.location-name;Port Royale@@ has it?",
			journalComplete: "You fetched the bag of mojo for NPC_NAME. She will now provide some specialized 'voodoo' services.",
		},

		// Part of Bertie's Queen's Favor Part 2
		BERTIE_QUEEN_PT2_INFO_PAOLA: {
			title: "Chasing the Thieves",
			giver: "Paola",
			pre: [
				{type: "quest", name: "BERTIE_QUEEN_PT2_DELIVERY_B", property: "status", value: QuestStatus.Completed, condition: "eq"},
				{type: "quest", name: "BERTIE_QUEEN_PT2_INFO", property: "status",  value: QuestStatus.Active, condition: "eq"},
			],
			post: [
				{type: "quest", name: "BERTIE_QUEEN_PT2_INFO", value: "complete"},
				{type: "quest", name: "BERTIE_QUEEN_PT2_INFO_PAPA_BABA", value: 'cancel'},
			],
			intro: "",
			middle: "",
			finish: "\
    You approach NPC_NAME, the mysterious jungle medicine woman. She greets you normally and offers you a simple \
    refreshment. s(What brings ye here to ol' Paola? You be needin a charm? A hex on a lover? Speak sweets!)\n\n\
    An interesting offer to say the least, but not exactly what you're after right now. You clear your throat \
    and lick your lips, then reach into your garments and produce the drawing that <span class='npc'>\
    Jonah Blythe</span> gave you and politely ask her if she recognizes it.\n\n\
    NPC_NAME's mouth twists in a grimace. She hands the picture back and sits down across from you on a fallen \
    log. s(Ye, I know et. I know et well.) she says. s(Ets a devil the white man worship. Dey be callin' et \
    'Mahomet') she stops and makes a sign over her heart, possibly some kind of protective charm.\n\n\
    Who is that? What does it mean? You have a dozen questions pop into your mind. However, all you can get \
    out of NPC_NAME is a single statement - s(I dun noe much, but dis - dat devil fills a man with madness \
    and lust and demands a price in blood. Unfaithful. Dat's what eet likes - unfaithful, disloyal, liars.)\n\n\
    You ponder this information for a moment, it's not much, but you at least have a name and some indication \
    of who might be behind it. It's probably enough to return to <span class='npc'>Bertie</span> with.\
     ",
			journalEntry: "HIDDEN",
			journalComplete: "\
     You asked NPC_NAME if she knew anything about the mysterious symbol. She told you a couple of key \
     pieces of information. Namely the symbol belongs to a cult that worships some type of demonic \
     figure named 'Mahomet' and that you should look for his followers amongst the 'unfaithful'.\
     ",
		},

		PAOLA_BREAST_HEALTH: {
			title: "Healthy milk",
			giver: "Paola",
			pre: [
				{type: "quest", name: "VOODOO_PICKUP", property: "status", value: QuestStatus.Completed},
				{type: Stat.Body, name: BodyProperty.Lactation, value: 6, condition: "gte"},
				{type: Stat.Body, name: BodyPart.Bust, value: 64, condition: "gte"}, // gigantic
			],
			post: [
				{type: "worldState", name: "dayPhase", value: DayPhase.LateNight},
				{type: "bodyEffect", name: "VOODOO_LACTATION", value: true},
				{type: "bodyEffect", name: "VOODOO_BUST_FIRMNESS_FULL", value: true},
				{type: Stat.Core, name: CoreStat.Toxicity, value: 350},
				{type: Stat.Core, name: CoreStat.Toxicity, value: 100, opt: "random"},
			],
			checks: [
				{type: "item", name: "drugs/fairy dust", value: 30, condition: "gte"},
				{type: "item", name: "food/cordial waters", value: 5, condition: "gte"},
				{type: "item", name: "drugs/apColdsfoot", value: 4, condition: "gte"},
				{type: "item", name: "drugs/siren elixir", value: 3, condition: "gte"},
				{type: "item", name: "food/ambrosia", value: 2, condition: "gte"},
				{type: "item", name: "drugs/apSenna", value: 1, condition: "gte"},
				{type: "item", name: "drugs/apSweetgrass", value: 1, condition: "gte"},
				{type: "item", name: "drugs/succubus philtre", value: 1, condition: "gte"},
				{type: "item", name: "drugs/apSulfur", value: 1, condition: "gte"},
				{type: "item", name: "drugs/female mandrake", value: 1, condition: "gte"},
			],
			intro:
				"NPC_NAME inspects your chest as you as you approach. In particular, she is looking at the wet spots made by your lactating nipples. You can guess she doesn't like what she sees and decide to ask her what she is concerned with." +
				"sp(NPC_NAME, is there anything wrong with me?) you question her." +
				"s(Oh, no! No! Do not be worry, PLAYER_NAME, you are in good. But if you feed baby more by that bad milk from your teats, it be dyin'. This milk is dead!)\n\n" +
				"Apparently, the strange substance <span class='npc'>Kipler</span> used to inject you to make your NOUN_Bust lactate, is what NPC_NAME sees as the reason for the \"dead milk\". It is not surprising; who knows what does he put in his syringes.\n\n" +
				"s(NPC_NAME can heal to you, milk and body, wit my power and wisdom), NPC_NAME continues. s(But if you agree, PLAYER_NAME, you must bring here what needs for makin' my task.)\n\n" +
				"tp(Heal?) you think, tp(Healing should be a good thing, even for a Vodoo priestess, right?)\n\n" +
				"Maybe you should try that \"healing\"?",
			middle: "You need to bring the following items to NPC_NAME:",
			finish:
				"NPC_NAME sees you carrying a basked with the ingredients she has requested and smiles.\n\n" +
				"s(Ah-ah, PLAYER_NAME decidin' to take my help! After you be help to me with my mojo, I ken do only best for you! I ken be happy to helpin' you as good as I can be doin', PLAYER_NAME!)\n\n" +
				"You give her everything she asked for, and the strange woman checks every bottle and jar, apparently using their looks and smells to distinguish the substances you brought to her.\n\n" +
				"s(All are good and we can start), NPC_NAME declares after finishing inspecting the drugs and portions. s(You wait small time here, and I make you ready),she says and disappears inside the little cave. In a few minutes she comes back with a basket full of various leafs, but you also notice a knife and a rope. Are all of them required for the hexing? The ritual going to be complex, as it seems.\n\n" +
				"In the meantime NPC_NAME minces some leafs and puts them into one of the bottles with ambrosia. s(Take out all clothes, seat in line and drink this, PLAYER_NAME!), she orders you. tp(In line?), your are thinking while getting naked. You try your best to seat as straight as you can on the bedrock stones. NPC_NAME watches as you quickly emptying the bottle, which instead of its normal empowering effect brings you into a rigour, your ears fill in with a monotonic buzz, your eyesight dims and looses colours. You notice NPC_NAME caught your head when your powerless body falls onto the stones. Lying on the ground now, you realize NPC_NAME takes the rope and binds your hands together beyond your head. What the hell she is going to to with you? You scared to death by the beginning of the \"hexing\" already, but the real things did not even begin!\n\n" +
				"s(Do not afraid PLAYER_NAME, I do dis for you not to pain and try killin' us), she laugh and you hear her voice via the mind fog induced by the drink she gave you. s(Now PLAYER_NAME be less in pain and I begin' mah' job), she adds.\n\n" +
				"You can't turn your head and only notice she appears in your side vision with other jars, more leafs and the knife. Then you see her covering your head with a piece of dirty cloth. tp(She is going to kill me, that's for sure) is the first your thought about that. Yet she says s(Sun burnin' very hot). And then comes the pain. You think she cuts your chest open, but you can't see that due to the cloth on your face. The only thing you can feel now is the pain and even more pain. It hurts all over your chest, and you can't move even a bit, just lying there and feeling to be eaten alive…\n\n" +
				"After an eternity the pain starts to fade off, and you see NPC_NAME removes the cloth from your face, unties your hands, opens your mouth and pours it with a liquid, likely the other bottle of ambrosia. s(All is good, now wait) says NPC_NAME.\n\n" +
				"You are lying still for hours, see the sun settles down, and finally you could find your toes and fingers! Sensitivity seems to be recovering quickly now, and not long after dark you are able to move and slowly get up. Inspecting your chest you find two little scars under your NOUN_Bust, which are the only visible changes to your body. NPC_NAME is sitting near the fireplace and you move there to question here about what has she done to you\n\n" +
				"NPC_NAME meets you with a smile and says s(I happy you be wallckin' now. Your body was so very full with poison! I put all healin' in you, and you be better every day! If you do not want to heal more, come back to me and I remove mah healing)\n\n" +
				"tp(What? Did she just say she put something <i>inside</i> your body? And it will heal you from inside? What the hell did she do?) After a few more rounds of questions you indeed realize she put something, made of the ingredients you brought here, under your skin, and that, as she believes, should \"heal\" you. And once it healed you enough, you can come back here so NPC_NAME could remove the remaining \"medication\" from your body. Which would imply the same pain and the same immobilization. You would ask NPC_NAME to undo what she's done to you immediately, but facing another round of the hell you decide to think about that tomorrow.",
			journalEntry:
				"NPC_NAME on <span class='location-name'>Abamond</span> offered you a help in healing your \"body and milk\", \
				whatever that means, damaged by <span class='npc'>Kipler</span>'s injections. You need to collect required ingredients first.",
			journalComplete: "NPC_NAME did a special 'voodoo' ritual for you, claiming it should heal your body and make it produce healthy milk.",
		},
	});
}
