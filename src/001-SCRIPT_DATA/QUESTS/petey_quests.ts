namespace App.Data {
	Object.append(quests, {
		SMUGGLER_PICKUP: {
			title: "Smuggler's Run: The Pickup",
			giver: "Petey",
			pre: [
				// Flags that are required to trigger quest.
				{type: "quest", name: "ISLA_TAVERN02", property: 'status', value: QuestStatus.Active},
			],
			checks: [{type: "quest", name: "ISLA_TAVERN02", property: 'status', value: QuestStatus.Active}],
			reward: [
				{type: "item", name: "quest/smugglers rum cask", value: 1},
				{type: "money", value: 200},
				{type: "npcStat", name: NpcStat.Mood, value: 10},
			],
			intro: "",
			middle: "",
			finish: "\
        NPC_NAME says, s(Aye PLAYER_NAME, I have that delivery for @@.npc;Bosun@@ right behind the bar. Be sure to \
        tell him I'll have the next delivery on time. Sorry for the hassle, take this gold from me as payment for your time.)"
			,
			// Don't display in journal.
			journalEntry: "HIDDEN",
			journalComplete: "HIDDEN",
		},

		LORD_ROWE_PORNO_DELIVERY: {
			title: "Smuggler's Run: Special Delivery - Lord Rowe",
			giver: "Petey",
			pre: [{type: "styleCategory", name: "PirateSlut", value: 50, condition: "gte"}],
			checks: [{type: "quest", name: "LORD_ROWE_DELIVERY", property: 'status', value: QuestStatus.Completed, altTitle: "Deliver the book to @@.npc;Lord Rowe@@"}],
			onAccept: [
				{type: "quest", name: "LORD_ROWE_DELIVERY", value: "start"},
				{type: "item", name: "quest/lolita book", value: 1},
			],
			reward: [
				{type: "money", value: 500},
				{type: "store", name: "smugglers", value: "steel cutlass", opt: "unlock"},
				{type: "npcStat", name: NpcStat.Mood, value: 10},
			],
			intro: "\
        s(Hey there PLAYER_NAME,) say NPC_NAME, s(You look like the kind of sort who would be up for a little, how shall we say \
        it… 'less than legal' activities?)\n\n\
        You're not sure what he's implying, but after what you've gone through, it couldn't hurt to hear the man out. You simply nod \
        your head for him to continue.\n\n\
        s(You see, I've got this client on Isla Harbor, a man, a very important man, who has what we might call 'peculiar tastes'. \
        Now, there's no harm in having a little bit of a fantasy life, but the law doesn't really see it that way. That's where you \
        and me come in.)\n\n\
        He clears his throat with a drink and then reaches behind the bar to retrieve a book. He hands it to you…\n\n\
        s(It's a simple job, all you need to do is take that book to his Lordship on Isla Harbor. It might be hard to get to him, and \
        under no circumstances are you to tell anyone the real reason why you need to see him, and of course if you get caught with \
        that he'll probably have you thrown in the stocks, but if you manage to somehow deliver that book… Well, let's just say \
        I'll pay you well for your services.)\n\n\
        Ignoring the fact that you've just been asked to deliver contraband to your fiance's father, the job itself does seem simple \
        enough on it's surface."
			,
			middle: "\
        NPC_NAME says s(Hey PLAYER_NAME, did you manage to deliver the goods to @@.npc;Lord Rowe@@ yet? Remember, don't\
        get caught, and if you do, we never met.)",
			finish: "\
        s(Well done, PLAYER_NAME, glad to see you delivered the goods and with no one the wiser. I think you might be just the kinda \
        dirty lass we need around here. You know, one who can get the job done. Tell you what, check the shop from time to time and \
        see what we have for sale. I'll open up the 'special inventory' just for you.)"
			,
			// Don't display in journal.
			journalEntry: "You've been asked to deliver a contraband book to @@.npc;Lord Rowe@@ on @@.location-name;Isla Harbor@@.",
			journalComplete: "You delivered the contraband book to @@.npc;Lord Rowe@@ and while your purse is a fair bit richer, you shudder to think of the things you learned about the man and " +
				"his decided unwholesome interest in his own daughter.",
		},

		FORGED_LETTER: {
			title: "Forgery is a Crime - Letter of Introduction",
			giver: "Petey",
			// Don't display in journal.
			journalEntry: "HIDDEN",
			journalComplete: "HIDDEN",
			pre: [{type: "quest", name: "BELLA_INTRODUCTION", property: 'status', value: QuestStatus.Active}],
			checks: [
				{type: "quest", name: "BELLA_INTRODUCTION", property: 'status', value: QuestStatus.Active},
				{type: "money", value: 500, condition: "gte"},
			],
			reward: [
				{type: "item", name: "quest/letter of introduction", value: 1},
				{type: "money", value: -500},
			],
			intro: "\
        NPC_NAME greets you as you enter his shop. s(Ho there traveller! What can I get for you today? Perhaps one o' \
        our famous ales?)\n\n\
        You approach the bar top - you've heard rumors about what kind of man NPC_NAME is and if they're right, he \
        might be just the man to help you. You briefly explain the situation to him and he strokes his chin, \
        in an apparent act of deep thought.\n\n\
        s(It shouldn't be too hard to fool that dimwit, her tits are bigger than her brain for sure. Tell you \
        what PLAYER_NAME, pay me <span class='item-money'>500 coins</span> and I'll set you up with a propper \
        letter of introduction, well at least propper enough to fool that biddy.)\n\n\
        It's a little pricey, especially just to get access to a place where you can spend even MORE money, but \
        your curiosity is eating away at you… what to do?\
        ",
			middle: "\
        NPC_NAME says, s(Come back when you have the <span class='item-money'>500 coins</span> coins lass and I'll \
        get you that letter.)\
        ",
			finish:
				"\
        NPC_NAME says, s(Ah I see you've managed to scrape together the coin? Just wait here it shant be a moment!)\n\n\
        He excuses himself and ducks into a backroom. It takes about 10 minutes, but eventually he reappears \
        holding a thin piece of vellum parchment.\n\n\
        s(Here, as promised - a proper letter of introduction, seal and everything. Not my best work, but \
        definitely good enough to fool dimwit Darling)\n\n\
        You politely accept the letter from him and go on your way. \
     ",
		},

		// Part of Bertie's Queen's Favor Part 2
		BERTIE_QUEEN_PT2_DELIVERY_A: {
			title: "Chasing the Thieves",
			giver: "Petey",
			pre: [{type: "quest", name: "BERTIE_QUEEN_PT2", property: 'status', value: QuestStatus.Active}],
			checks: [{type: "quest", name: "BERTIE_QUEEN_PT2_DELIVERY_B", property: 'status', value: QuestStatus.Completed, altTitle: "Deliver the parcel to Jonah Blythe"}],
			onAccept: [
				{type: "quest", name: "BERTIE_QUEEN_PT2_DELIVERY_B", value: "start"},
				{type: "item", name: "quest/strange parcel", value: 1},
			],
			reward: [{type: "npcStat", name: NpcStat.Mood, value: 10}],
			intro: "\
        NPC_NAME greets you as you enter his shop. s(Ho there traveller! What can I get for you today? Perhaps one o' \
        our famous ales?)\n\n\
        You approach the bar top - you've heard rumors about what kind of man NPC_NAME is and if they're right, he \
        might be just the man to help you. You briefly explain the situation to him and he strokes his chin, \
        in an apparent act of deep thought.\n\n\
        s(Now, let's say I do know something about what yee're after, I'm not quite so sure I can say it outright, \
        not even under threat of the <span class='npc'>Pirate Queen's</span> wrath. But tell you what, \
        you do a favor for old Petey here and I might be able to sing you a line or two that could help.)\n\n\
        Frankly, this is already better than you had hoped for. A little 'quid pro quo', as long as it doesn't \
        involve your poor abused asshole, isn't out of the question.\n\n\
        NPC_NAME reaches below his bar top and produces a small package. He then hastily writes a note and stuffs \
        it inside before waving the entire thing in your face.\n\n\
        s(Take this to my man on <span class='location-name'>Port Royal</span>, a crooked Constable by the name \
        of <span class='npc'>Jonah Blythe</span>. He'll set you straight… just don't lose that \
        package and we'll be right as rain miss.)\
        ",
			middle: "\
        NPC_NAME says, s(Did you deliver that package yet? What? Well then what are you doing lollygagging around \
        these parts!)\
        ",
			finish:
				"",
			journalEntry: "\
     NPC_NAME has offered to give up some valuable information about the thieves that stole part of \
     <span class='npc'>Bertie's</span> treasure map. All you need to do is to deliver this parcel \
     he gave you to his inside man on <span class='location-name'>Port Royal</span>, a crooked Constable \
     by the name of <span class='npc'>Jonah Blythe</span>.\
     ",
			journalComplete: "\
     You handed over the parcel to <span class='npc'>Jonah Blythe</span> only to learn that NPC_NAME \
     had cleverly made you an accomplice in the theft. Still, he did come through with his words and you \
     did learn some valuable information - <span class='npc'>Blythe</span> was able to reproduce \
     for you a strange symbol that the thieves all wore, an emblem of some sort of arcane or mystical \
     significance. Now, you just need to get someone to tell you what it all means.\
     ",
		},
	});
}
