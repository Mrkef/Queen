export const Materials: {
    brownFur: {
        stroke      : "#663300",
        fill        : "#ac7339",
        // fur parts can be tucked under
        coverConceal: ["this"],
    },

    sheerFabric: {
        stroke: "#000",
        fill  : "rgba(0,0,0,0.8)",
    },
}
