import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class CapBasePart extends ClothingPart {
	constructor(...data: object[]);

	// how high the top of the cap should be
	height: number;
	// [-0.5, 0.5] which side the cap is tilted towards (negative is top tilted towards left)
	sideBias: number;
	// degree of curve for bottom of hat
	curvature: number;
	// [-1, 1] (reasonable values) how much of the forehead should be covered (negative doesn't cover forehead)
	headCoverage: number;
	// how wide to make the hat to adjust for ears and stuff
	sideOffset: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class CapBandPart extends ClothingPart {
	constructor(...data: object[]);

	bandWidth: number;
	bandPattern: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export function calcCap(ex: DrawingExports): {rightBot: DrawPoint, leftBot: DrawPoint, top: DrawPoint};

export class Hat extends Clothing {
	constructor(...data: object[]);
}


export class SimpleCap extends Hat {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BandedCap extends Hat {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
