import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class CoveredButtPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class LongPantsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class MediumPantsPart extends ClothingPart {
	constructor(...data: object[])

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class ShortPantsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class ShortsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Pants extends Clothing {
	constructor(...data: object[]);

	/**
	 * How far to extend the pant legs
	 * 1 goes all the way down to ankles
	 * 0.5 goes to knees
	 * anything else is shorter
	 */
	legCoverage: number;
	/**
	 * Where along waist-hips does the top of the hips sit
	 * Positive values means further towards the waist, 0 is at the
	 * hips,
	 * negative values means along hips to inner knee
	 */
	waistCoverage: number;

	fill(): string;
}


export class LongTightPants extends Pants {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class MediumTightPants extends Pants {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class ShortTightPants extends Pants {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class TightShorts extends Pants {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
