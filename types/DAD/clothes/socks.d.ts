import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class SockPart extends ClothingPart {
	constructor(...data: object[]);

}


export class ShortSockPart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class MediumSockPart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class LongSockPart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class ThighHighPart extends SockPart {
	constructor(...data: object[]);


	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class ThighHighBandPart extends SockPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Sock extends Clothing {
	constructor(...data: object[]);

	length: number;
	thickness: number;
	// stroke: string;
}


export class ShortSocks extends Sock {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class MediumSocks extends Sock {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class LongSocks extends Sock {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class ThighHighs extends Sock {
	constructor(...data: object[]);

	bandWidth: number;
	bandPattern: string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
