import {DrawPoint, Point} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";
import {ShoeSidePart} from "./shoes";

export class HeelPart extends ClothingPart {
	constructor(...data: object[]);
}


export class HeelBaseShine extends HeelPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class HeelBasePart extends HeelPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class CoveredHeelBasePart extends HeelPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class HeelStrapPart extends HeelPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export function calcHeels(ex: DrawingExports): {
	out: DrawPoint,
	outBot: Point,
	bot: Point,
	inBot: Point,
	inTop: DrawPoint,
	tongue: DrawPoint
};

export class HeelSideBaseClosedPart extends ShoeSidePart {
	constructor(...data: object[]);

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export class CoveredHeelSideBasePart extends ShoeSidePart {
	constructor(...data: object[]);

	toeCoverage: number;
	soleWidth: number;
	tongueSplitRatio: number;

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export function calcCoveredHeelBaseSide(ex: DrawingExports, mods: object): {
	vamp: DrawPoint,
	toeTip: DrawPoint,
	soleBot: Point,
	heelBack: Point,
	heelBackTop: DrawPoint,
	tongueStart: DrawPoint,
	tongueSide: DrawPoint,
	tongue: Point,
	counterTip: Point,
};


export class StilettoPart extends ShoeSidePart {
	constructor(...data: object[]);

	// how much along the path (0 to 1) from sole bottom to heelBack to start extending down
	separationAlongArch: number;
	heelTipWidth: number;
	// which way and how much the back of the heel bends (can be negative)
	backHeelDeflection: number;
	// use null to inherit the base's fill
	stilettoFill: string;

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export class PlatformSidePart extends ShoeSidePart {
	constructor(...data: object[]);

	// how high the platform is in mm
	platformHeight: number;
	platformFill: string;

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export class HeelSideSimpleStrapPart extends ShoeSidePart {
	constructor(...data: object[]);

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export class HeelSideWideStrapPart extends ShoeSidePart {
	constructor(...data: object[]);

	strapWidth: number;

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export function calcWideStrapSide(ex: DrawingExports, mods: object): {
	left: DrawPoint, topLeft: Point, topRight: DrawPoint, right: DrawPoint
};

export function calcHeelBaseSide(ignore: any, mods: object): {
	vamp: DrawPoint,
	toeTip: DrawPoint,
	soleBot: Point,
	heelBack: Point,
	counterTip: Point
};

export function calcHeelPlatformSide(ex: DrawingExports, mods: object): {topLeft: Point, left: DrawPoint, bot: DrawPoint, right: DrawPoint};

export function calcStiletto(ex: DrawingExports, mods: object): {archTip: DrawPoint, heelTip: DrawPoint, heelTipBack: Point, stilettoHeelBack: DrawPoint};

export class StrapLockSidePart extends ShoeSidePart {
	constructor(...data: object[]);

	lockSize: number;

	renderShoeSidePart(ex: DrawingExports, ctx: CanvasRenderingContext2D, mods: object): void;
}

export class StrapLockPart extends HeelPart {
	constructor(...data: object[]);

	lockSize: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class Heels extends Clothing {
	fill(): string;

	constructor(...data: object[]);

	thickness: number;
	/**
	 * How high the heel is to lift the player's height in inches
	 */
	shoeHeight: number;
	basePointiness: number;
	/**
	 * How constrained the toes are together
	 */
	shoeTightness: number;
}


export class ClosedToePumps extends Heels {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ClosedToeStrappedPumps extends Heels {
	constructor(...data: object[]);

	strapWidth: number;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class CoveredFancyStilettos extends Heels {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class LockedCoveredFancyStilettos extends CoveredFancyStilettos {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
