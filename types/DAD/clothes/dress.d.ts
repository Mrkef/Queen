//import {setStrokeAndFill} from "../util/draw";
import {DrawPoint, Point} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class DressBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class DressBasePart extends ClothingPart {
	constructor(...data: object[]);

	cleavageOpeness: number;
	cleavageCoverage: number;
	sideLoose: number;
	legCoverage: number;
	legLoose: number;
	curveCleavageX: number;
	curveCleavageY: number;
	bustle: boolean;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export function calcDressCleavage(ex: DrawingExports, bottom: Point): {
	cleavageBot: Point,
	cleavageTop: Point,
	neck: DrawPoint
}

export function calcDressBase(ex: DrawingExports): {
	cleavageBot: Point,
	cleavageTop: Point,
	neck: DrawPoint,
	collarbone: DrawPoint,
	armpit: DrawPoint,
	waist: DrawPoint,
	hip: DrawPoint,

	outerPoints: DrawPoint[],

	shoulder: DrawPoint,
	pit: DrawPoint,

	bottom: DrawPoint
}


export class DetachedSleevePart extends ClothingPart {
	constructor(...data: object[]);

	shoulderCoverage: number;

	armCoverage: number;
	armLoose: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class SuperSleevePart extends ClothingPart {
	constructor(...data: object[]);

	armCoverage: number;
	armLoose: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export function calcSuperSleeve(ex: DrawingExports): {
	outerArmPoints: DrawPoint[],
	innerArmPoints: DrawPoint[]
};


export class LacingPart extends ClothingPart {
	constructor(...data: object[]);

	lacing: boolean;
	crosses: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/* TODO
export class SleeveHemPart extends ClothingPart {
    constructor(...data: object[]);
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
			hemArmHeight: 5,
			hemArmWidth: 5,
			hemColor: "",
        }, ...data);
    }

    renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
		if(this.hemArmHeight==0||this.hemArmWidth==0)return
		Clothes.simpleStrokeFill(ctx, ex, this);
        if(this.hemColor){
			ctx.fillStyle=this.hemColor
		}else{
			ctx.fillStyle=ctx.strokeStyle
		};

		const {
           outerArmPoints,
		   innerArmPoints
        } = calcSuperSleeve.call(this, ex);


		//var temp = extractPoint(outerArmPoints[outerArmPoints.length-1]);
		//hemOut = adjust(temp,5,-5) ;

		var hemOut = adjust(extractPoint(outerArmPoints[outerArmPoints.length-1]),this.hemArmWidth,-this.hemArmHeight);
		var	hemIn = adjust(extractPoint(innerArmPoints[0]),-this.hemArmWidth,-this.hemArmHeight) ;

		 //bottom curve
		hemIn.cp1 = {
			x: 0.5*(hemOut.x+hemIn.x),
			y: hemIn.y-1
		};

		ctx.beginPath();
		drawPoints(ctx,
			outerArmPoints[outerArmPoints.length-1],
			hemOut,
			hemIn,
			extractPoint(innerArmPoints[0])
		);
		ctx.stroke();
		ctx.fill();
	}
}
*/

/**
 * Base Clothing classes
 */
export class Dress extends Clothing {
	constructor(...data: object[]);

	armCoverage: number;
	armLoose: number;
	thickness: number;
}


export class SuperDress extends Dress {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
