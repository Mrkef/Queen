import {
	DrawPoint,
	Point
} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Underwear} from "./underwear";



export function calcSuperBra(ex: DrawingExports): {out: DrawPoint, tip: DrawPoint, top: DrawPoint, bot: DrawPoint, cleavage: DrawPoint, inner: DrawPoint};

export function calcSuperBraStrap(ex: DrawingExports): {
	out: Point,
	outbot: Point,
	bot: Point,
	mid: Point,
	strapTop: DrawPoint,
	breastTop: DrawPoint,
	breastOut: Point
};

export class SuperBraGenitalPart extends ClothingPart {
	constructor(...data: object[]);

	strapBehindBreasts: boolean;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class SuperBraChestPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export function calcPanties(ex: DrawingExports): {
	sideTop: DrawPoint,
	sideBot: DrawPoint,
	top: DrawPoint,
	bot: DrawPoint,
	botOut: DrawPoint
};

export function calcPanties2(ex: DrawingExports): {
	sideTop: DrawPoint,
	sideBot: DrawPoint,
	top: DrawPoint,
	bot: DrawPoint,
	botOut: DrawPoint,
	center: Point
};


export class SuperPantiesPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class ChastityBeltPart extends ClothingPart {
	constructor(...data: object[]);
	waistCoverage: number;
	beltWidth: number;
	beltCurve: number;
	highlight: string;

	coverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class SuperPanties2Part extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/*

*/


export class SuperBra extends Underwear {
	constructor(...data: object[]);

	showStrap: boolean;
	strapWidth: number;
	neckCoverage: number;
	thickness: number;
	botStrapWidth: number;
	topStrapCurveX: number;
	topStrapCurveY: number;
	highlight: string;

    /*
    stroke() {
        return "hsl(346, 50%, 70%)";
    }
    */

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class SuperPanties extends Underwear {
	constructor(...data: object[]);


	waistCoverage: number;
	waistCoverageLower: number;
	genCoverage: number;
	topY: number;
	curveTopX: number;
	curveTopY: number;
	curveBotX: number;
	curveBotY: number;
	bow: boolean;

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BikiniBottom extends Underwear {
	constructor(...data: object[]);

	waistCoverage: number;
	waistCoverageLower: number;
	genCoverage: number;
	topY: number;
	curveTopX: number;
	curveTopY: number;
	curveBotX: number;
	curveBotY: number;
	radius: number;
	thickness: number;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class ChastityBelt extends Underwear {
	constructor(...data: object[]);

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
