import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Jewelry} from "./jewelry";


export class Bodychain1Part extends ClothingPart {
	constructor(...data: object[]);

	cleavageCoverage: number;
	cleavageCoverageBot: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Bodychain2Part extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class Bodychain3Part extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Base Clothing classes
 */
export class Bodychain extends Jewelry {
	constructor(...data: object[]);

	thickness: number;
}
/**/



export class Bodychain1 extends Bodychain {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Bodychain2 extends Bodychain {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Bodychain3 extends Bodychain {
	constructor(...data: object[]);

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

/*
export class Bodychain4 extends Bodychain {
    constructor(...data: object[]);
        super({

        }, ...data);
    }

    get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
*/
