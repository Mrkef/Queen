import {DrawPoint, Point} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Pants} from "./pants";

export function calcSuperPantsTop(ex: DrawingExports): {
	top: Point,
	out: DrawPoint,
	hip: DrawPoint,
	thighOut: DrawPoint
};


export function calcSuperPants(ex: DrawingExports): {
	top: DrawPoint,
	out: DrawPoint,
	hip: DrawPoint,
	thighOut: DrawPoint,
	groin: DrawPoint,
	outerPoints: DrawPoint[],
	innerPoints: DrawPoint[]
};

export function calcSuperPantsZip(ex: DrawingExports): {
	zip: Point,
	top: DrawPoint,
	mid: DrawPoint,
	out: DrawPoint,
	hip: DrawPoint,
	thighOut: DrawPoint,
	groin: DrawPoint,
	outerPoints: DrawPoint[],
	innerPoints: DrawPoint[]
};

export class SuperPantsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
	the same thing as SuperPantsPart, but drawn BELLOW shirts
*/
export class SuperLegginsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
	the same thing as SuperPantsPart, but drawn in MIDRIFT layer
*/
export class SuperSkirtPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class LacedLegginsPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class JeansPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class LoinclothPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/*

*/
export class SuperPants extends Pants {
	constructor(...data: object[]);

	innerLoose: number;
	outerLoose: number;
	legCoverage: number;
	waistCoverage: number;
	opacity: number;
	thickness: number;
	bustle: boolean;
	belt: number;

	fill(): string;
	stroke(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class SuperLeggins extends Pants {
	constructor(...data: object[]);

	innerLoose: number;
	outerLoose: number;
	legCoverage: number;
	waistCoverage: number;
	opacity: number;
	thickness: number;
	bustle: boolean;
	belt: number;

	fill(): string;
	stroke(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class SuperSkirt extends Pants {
	constructor(...data: object[]);
	innerLoose: number;
	outerLoose: number;
	legCoverage: number;
	waistCoverage: number;
	opacity: number;
	thickness: number;
	bustle: boolean;
	belt: number;

	fill(): string;
	stroke(): string;

	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class LacedLeggins extends Pants {
	constructor(...data: object[]);

	zipOpen: number;
	zipDeep: number;
	crosses: number;

	innerLoose: number;
	outerLoose: number;
	legCoverage: number;
	waistCoverage: number;
	opacity: number;
	thickness: number;
	bustle: boolean;

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Jeans extends Pants {
	constructor(...data: object[]);

	innerLoose: number;
	outerLoose: number;
	legCoverage: number;
	waistCoverage: number;
	opacity: number;
	thickness: number;
	bustle: boolean;
	belt: number;

	zipOpen: number;  //TODO
	zipDeep: number;
	highlight: string;

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class Loincloth extends Pants {
	constructor(...data: object[]);

	waistCoverage: number;

	beltWidth: number;
	beltCurve: number;

	thickness: number;

	topCoverage: number;
	legCoverage: number;
	bottomCoverage: number;

	curveX: number;
	curveY: number;

	highlight: string;

	fill(): string;
	stroke(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
