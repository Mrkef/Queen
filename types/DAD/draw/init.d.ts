import {Player} from "../player/player";
import {CustomDrawingFunction, DrawingExports, DrawUserConfig} from "./draw";

export function getBaseColors(avatar: object): object;

// export ex.baseStroke, ex.baseFill, and ex.baseLipColor
export function configureBaseColors(ex: object): void;
export function initCanvas(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D, config: DrawUserConfig, avatar: Player,
	ex: DrawingExports, layer: number, clear: boolean, customDrawingFunction?: CustomDrawingFunction|CustomDrawingFunction[]): void;
