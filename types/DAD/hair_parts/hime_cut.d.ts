import {HairPart} from "./hair_part";
import {DrawPoint} from "drawpoint/dist-esm/point"

export class HimeCutBack extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}


export class HimeCutFront extends HairPart {
	constructor(...data: object[]);

	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}


export class HimeCutSide extends HairPart {
	constructor(...data: object[]);

	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}

export function calcHimeCut(ex: object, hl: number): {left: DrawPoint, right: DrawPoint, top: DrawPoint};
