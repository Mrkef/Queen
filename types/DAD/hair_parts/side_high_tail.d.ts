import {HairPart} from "./hair_part";

export class SideHighTailFront extends HairPart {
	constructor(...data: object[]);

	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}


export class SideHighTailTail extends HairPart {
	constructor(...data: object[]);
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object, ignore: any, extraColors: object): void;
}
