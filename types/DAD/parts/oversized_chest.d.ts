import {shadingMedium, ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {Part, BodyPart} from "./part";
import {
    adjustPoints,
    extractPoint,
    adjust,
    breakPoint,
    reverseDrawPoint,
    splitCurve,
    continueCurve,
    clamp,
    none, DrawPoint
} from "drawpoint/dist-esm";
import {connectEndPoints} from "..";
import {Player} from "../player/player";

declare class OversizedLeftBreastShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
    calcDrawPoints(ex: object): DrawPoint[];
}

declare class OversizedLeftBreastUnderShading extends ShadingPart {
	constructor(...data: object[]);

    calcDrawPoints(ex: object): DrawPoint[];
}

declare class OversizedRightBreastShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
    calcDrawPoints(ex: object): DrawPoint[];
}

declare class OversizedRightBreastUnderShading extends ShadingPart {
	constructor(...data: object[]);

    calcDrawPoints(ex: object): DrawPoint[];
}

export class OversizedChest extends BodyPart {
	constructor(...data: object[]);

    calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}


export class OversizedChestNipples extends OversizedChest {
	constructor(...data: object[]);

	strokeClip(): void;
	stroke(ctx:CanvasRenderingContext2D, ex:object): string;
	getLineWidth(avatar: Player): number;
    calcDrawPoints(ex: object, mods: object): DrawPoint[];
}
