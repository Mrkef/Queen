import {shadingMedium, ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {Part, BodyPart} from "./part";
import {
    adjustPoints,
    extractPoint,
    adjust,
    breakPoint,
    reverseDrawPoint,
    splitCurve,
    continueCurve,
    clamp,
    none,
	DrawPoint
} from "drawpoint/dist-esm";
import {connectEndPoints} from "..";
import {DrawingExports} from "../draw/draw";
import {Player} from "../player/player";

declare class LeftBreastShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
	calcDrawPoints(ex: object): DrawPoint[];
}

declare class LeftBreastUnderShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: object): DrawPoint[];
}

declare class RightBreastShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;

	calcDrawPoints(ex: object): DrawPoint[];
}

declare class RightBreastUnderShading extends ShadingPart {
	constructor(...data:[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Chest extends BodyPart {
	constructor(...data: object[]);
}

export class ChestHuman extends Chest {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}


export class NipplesHuman extends Chest {
	constructor(...data: object[]);

	strokeClip(): string;
	stroke(ignore: any, ex: DrawingExports): string;
	getLineWidth(avatar: Player): number;
    calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
