import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    adjust,
    clamp,
    simpleQuadratic,DrawPoint
} from "drawpoint/dist-esm";
import {Player} from "../player/player"

declare class Testicles extends BodyPart {
	constructor(...data: object[]);
}


export class TesticlesHuman extends Testicles {
	constructor(...data: object[]);

	getLineWidth(avatar: Player): number;
	stroke(): string;
    calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}
