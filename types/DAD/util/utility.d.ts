import {Point} from "drawpoint/dist-esm/point"

declare global {
	interface String {
		capitalizeFirstLetter(): string;
		startsWith(searchString: string, poistion?: number): boolean;
	}

	interface Array<T> {
		last(): T;
		extend(b: T[]): void;
		contains(needle: any): boolean;
	}
}

declare class RGBColor {
	r: number;
	g: number;
	b: number;
	a?: number;
}

declare class HSLColor {
	h: number;
	s: number;
	l: number;
	a?: number;
}

/**
 * Extract numeric RGB values from a HTML compatible string (whitespace ignored)
 * @param rgbString RGB string in the format "rgb(100,220,42)"
 * @returns Either an object holding r,g,b properties, or null if not matched
 */
export function extractRGB(rgbString: string): RGBColor;

/**
 * Extract numeric HSL values from a HTML compatible string (whitespace ignored)
 * @param hslString HSL string in the format "hsl(310,12%,25%)"
 * @returns Either an object holding h,s,l properties, or null if not matched
 */
export function extractHSL(hslString: string): HSLColor;

/**
 * Extract numeric RGB values from a HTML compatible hex string (whitespace ignored)
 * @param hexString Hex string in the format "#ffaabc"
 * @returns Either an object holding r,g,b properties, or null if not matched
 */
export function extractHex(hexString: string): RGBColor;

/**
 * Convert an RGB object to HSL object, which are more intuitive to modify.
 * Adapted from https://github.com/mjackson/
 * @param rgb RGB object holding r,g,b properties (each [0,255])
 * @returns HSL object holding h,s,l properties (h [0,360], s,l [0,100])
 */
export function RGBToHSL(rgb: RGBColor): HSLColor;

/**
 * Adjust an existing color into a new color
 * @param color A color in RGB, hex, or HSL form
 * @param adjustment Object with H, S, L, and optionally A as properties
 */
export function adjustColor(color: string | RGBColor | HSLColor, adjustment: HSLColor): string;

/**
 * Get a normally distributed random number
 * (using the Ziggurat algorithm https://en.wikipedia.org/wiki/Ziggurat_algorithm)
 * @param mean Mean of the underlying normal distribution
 * @param stdev Standard deviation around mean
 * @returns The random number
 */
export function randNormal(mean: number, stdev: number): number;

export function testRandGenerator(n: number): void;

export function averagePoint(p1: Point, p2: Point, bias?: number): Point;

// topological sort from https://github.com/marcelklehr/toposort
export function topologicalSort<T>(nodes: T[], edges: T[]): T[];
export function simpleTopologicalSort<T>(edges: T[]): T[];
